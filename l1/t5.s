.SECT .TEXT
        MOV     BX, DX
        MOV     AX, (x)
        MUL     (y)
        MOV     (res),AX
        MOV     DX, BX

.SECT .DATA
x:     .WORD    2
y:     .WORD    3

.SECT .BSS
res:   .SPACE   2
