.SECT .TEXT
        MOV     CX, (n)
L1:     ADD     AX, (i)
        ADD     BX, AX
        LOOP    L1
        MOV     (res),BX

.SECT .DATA
n:     .WORD    5
i:     .WORD    5

.SECT .BSS
res:   .SPACE   2
