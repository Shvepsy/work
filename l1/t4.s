.SECT .TEXT
        MOV     AX, (x)
        SUB     AX, (y)
        MOV     (res),AX
        MOV     AX, x
        MOV     BX, y
        MOV     CX, res

.SECT .DATA
x:     .WORD    4
x1:     .WORD    7
y:     .WORD    3

.SECT .BSS
res:   .SPACE   2

! We are seen offset by mark nameing
