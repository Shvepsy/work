.SECT .TEXT
        MOV     BX, DX
        MOV     AX, (x)
        DIV     (y)
        ADD     AX, DX
        MOV     (res),AX
        MOV     DX, BX

.SECT .DATA
x:     .WORD    55
y:     .WORD    10

.SECT .BSS
res:   .SPACE   2
