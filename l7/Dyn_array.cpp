#include "Dyn_array.h"

Dyn_array::Dyn_array() : data(nullptr), size(0) 
{}

Dyn_array::Dyn_array(int _size)
{
	if (_size < 0)
		throw "Size is less than zero.";
	this -> size = _size;
	data = new datatype[_size];
}

Dyn_array::~Dyn_array()
{
	delete[] data;
}

void Dyn_array::copy(const datatype* from, datatype *to, int size)
{
	
	for (int i = 0; i < size; i++)
	{
		to[i] = from[i];
	}
}

Dyn_array::Dyn_array(const Dyn_array& d)
{
	size = d.size;
	data = new datatype[size];
	copy(d.data, data, size);
}

Dyn_array& Dyn_array::operator=(const Dyn_array& d)
{
	if (size < d.size)
	{
		delete[] data;
		data = new datatype[d.size];
	}
	size = d.size;
	copy(d.data, data, size);
	return *this;
}

Dyn_array::datatype& Dyn_array::operator[](int index) //TODO
{
	if (index >= size || index <= -1)
		throw "Bad index.";
	return data[index];
}

int Dyn_array::count() const
{
	return size;
}

void Dyn_array::resize(int new_size)
{
	if (new_size < 0)
		throw "New size is less than zero.";
	if (size == new_size) return;
	datatype *tmp = data;
	data = new datatype[new_size];
	if (size != 0)
		copy(tmp, data, new_size);
	delete[] tmp;
	size = new_size;
}

void Dyn_array::append(const datatype& x)
{
	resize(size + 1);
	data[size - 1] = x;
}

void Dyn_array::insert(int index, const datatype& x)
{
	if (index < 0 || index > size)
		throw "Bad index.";
	resize(size + 1);
	for (int i = size - 1; i > index; i--)
		data[i] = data[i - 1];
	data[index] = x;
}

void Dyn_array::remove_at(int index)
{
	if (index < 0 || index > size-1)
		throw "Bad index.";
	for (int i = index; i < size - 1; i++)
		data[i] = data[i + 1];
	resize(size - 1);
}

void Dyn_array::remove(const datatype& x)
{
	int i = 0;
	while (i < size && data[i] != x)
		i++;
	if (i < size)
		remove_at(i);
	else cout << "������� �� ��������� �� ������" << endl;
}

bool Dyn_array::contains(const datatype& x)
{
	int i = 0;
	while (i < size && data[i] != x)
		i++;
	return (i < size);
}

ostream& operator<<(ostream& s, Dyn_array& arr)
{
	for (int i = 0; i < arr.count(); i++)
		s << arr[i] << "  ";
	cout << endl;
	return s;
}