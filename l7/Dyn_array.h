#pragma once
#include <exception>
#include <iostream>

using namespace std;

class Dyn_array
{
public:

	typedef double datatype;
private:
	datatype *data;
	int size;

	void copy(const datatype* from, datatype* to, int size);
public:
	Dyn_array();

	Dyn_array(int _size);

	~Dyn_array();

	Dyn_array(const Dyn_array& d);

	Dyn_array& operator=(const Dyn_array& d);

	datatype& operator[](int index);

	int count() const;

	void resize(int new_size);

	void append(const datatype& x);

	void insert(int index, const datatype& x);

	void remove_at(int index);

	void remove(const datatype& x);

	bool contains(const datatype& x);
	
	friend ostream& operator<<(ostream& s, Dyn_array& arr);
};
