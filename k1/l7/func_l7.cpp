#include "func_l7.h"

using namespace std;

void printStaticMatrix(int Matrix[N][N], int column, int line) {
	for (int j = 0; j < line; j++) {
		for (int i = 0; i < column; i++)
			cout << Matrix[i][j] << " ";
		cout << endl;
	}
	cout << endl;
}

void printDynamicMatrix(int **Matrix, int column, int line ) {
	for (int j = 0; j < line; j++) {
		for (int i = 0; i < column; i++)
			cout << Matrix[i][j] << " ";
		cout << endl;
	}
	cout << endl;
}

bool checkEqStaticMatrix(int Matrix1[N][N], int Matrix2[N][N], int column1, int line1, int column2, int line2 ) {
	if (column1 != column2 && line1 != line2)
		return false;
	for (int i = 0; i < column1; i++)
		for (int j = 0; j < line2; j++) {
			if (Matrix1[i][j] != Matrix2[i][j])
				return false;
		}
	return true;
}

bool checkEqDynamicMatrix(int **Matrix1, int **Matrix2, int column1, int line1, int column2, int line2) {
	if (column1 != column2 && line1 != line2)
		return false;
	for (int i = 0; i < column1; i++)
		for (int j = 0; j < line1; j++) {
			if (Matrix1[i][j] != Matrix2[i][j])
				return false;
		}
	return true;
}

void moveStaticMatCol(int Matrix[N][N], int zeroColNumber, int &column, int line) {
	for (int i = zeroColNumber; i < column-1; i++) {
		for (int j = 0; j < line; j++) {
			Matrix[i][j] = Matrix[i+1][j];
		}
	}
	column -= 1;
}

void deleteStaticZeroCol(int Matrix[N][N], int &column, int line) {
	// строка/столбец
	int i = 0, j = 0;
	bool Flag = true; // Флаг нулевого столбца
	while (i < column) {

		while (Flag && j < line) {
			Flag = Matrix[i][j] == 0;
			j++;
		}
		j = 0;

		if (Flag) {
			moveStaticMatCol(Matrix, i, column, line);
			i = -1;
		}
		Flag = true;
		i++;
	}
}

void moveDynamicMatCol(int **Matrix, int zeroColNumber, int &column, int line) {
	for (int i = zeroColNumber; i < column - 1; i++) {
		for (int j = 0; j < line; j++) {
			Matrix[i][j] = Matrix[i + 1][j];
		}
	}
	column -= 1;
}

int** deleteDynamicZeroCol(int **Matrix, int &column, int line) {
	// строка/столбец
	int i = 0, j = 0;
	bool Flag = true; // Флаг нулевого столбца
	int Kolit = 0;
	while (i < column) {
		while (Flag && j < line) {
			Flag = Matrix[i][j] == 0;
			j++;
		}
		j = 0;
		if (Flag) {
			moveDynamicMatCol(Matrix, i, column, line);
			Kolit++;
			i = -1;
		}
		Flag = true;
		i++;
	}

	int **HelpArray = new int*[column];
	for (int i = 0; i < column; i++)
		HelpArray[i] = new int[line];
	for (int i = 0; i < column; i++)
		for (int j = 0; j < line; j++)
			HelpArray[i][j] = Matrix[i][j];

	delete[] Matrix;
	return HelpArray;
}

void insertStaticVecStr(int Matrix[N][N], int &column, int &line, int VecStr[N][N], int Nomcolumn) {
	if (column == Nomcolumn) {
		for (int j = 0; j < line; j++)
			Matrix[column][j] = VecStr[0][j];
		column++;
	}
	else {
		for (int i = column-1; i > Nomcolumn - 1; i--)
			for (int j = 0; j < line; j++) {
				Matrix[i + 1][j] = Matrix[i][j];
			}
		column++;
		for (int j = 0; j < line; j++)
			Matrix[Nomcolumn][j] = VecStr[0][j];
	}
}

int** insertDynamicVecStr(int **Matrix, int &column, int line, int **VecStr, int Nomcolumn) {
	int **HelpArray = new int*[column + 1];
	for (int i = 0; i < column + 1; i++)
		HelpArray[i] = new int[line];
	for (int i = 0; i < column; i++)
		for (int j = 0; j < line; j++)
			HelpArray[i][j] = Matrix[i][j];

	if (column == Nomcolumn) {
		for (int j = 0; j < line; j++)
			HelpArray[column][j] = VecStr[0][j];
		column++;
	}
	else {
		for (int i = column-1; i > Nomcolumn - 1; i--)
			for (int j = 0; j < line; j++) {
				HelpArray[i + 1][j] = HelpArray[i][j];
			}
		column++;
		for (int j = 0; j < line; j++)
			HelpArray[Nomcolumn][j] = VecStr[0][j];
	}
	delete[] Matrix;
	return HelpArray;
}


void transposeStaticMatrix(int Matrix[N][N], int &column, int &line) {
	int MAX;
	if (column > line)
		MAX = column;
	else
		MAX = line;

	int HelpEl;
	for (int i = 0; i < MAX -1; i++)
		for (int j = 1; j < MAX - i; j++) {
			HelpEl = Matrix[i + j][i];
			Matrix[i + j][i] = Matrix[i][i + j];
			Matrix[i][i + j] = HelpEl;
		}

	HelpEl = column;
	column = line;
	line = HelpEl;

}

int** transposeDynamicMatrix(int **Matrix, int &column, int &line) {
	int** HelpArray = new int*[line];
	for (int i = 0; i < line; i++)
		HelpArray[i] = new int[column];
	int HelpEl;
	for (int i = 0; i < column; i++) {
		for (int j = 0; j < line; j++) {

			HelpArray[j][i] = Matrix[i][j];
		}
	}
	HelpEl = column;
	column = line;
	line = HelpEl;
	delete[] Matrix;

	return HelpArray;
}


void multiplyStaticMatrix(int Matrix1[N][N], int column1, int line1, int Matrix2[N][N], int column2, int line2, int MatrixC[N][N]) {
	for (int i = 0; i < line1; i++)
		for (int j = 0; j < column2; j++) {
			for (int k = 0; k < column1; k++) {
				MatrixC[i][j] += Matrix2[i][k] * Matrix1[k][j];
			}
		}
}

void multiplyDynamicMatrix(int **Matrix1, int column1, int line1, int **Matrix2, int column2, int line2, int **MatrixC) {
	for (int i = 0; i < line1; i++)
		for (int j = 0; j < column2; j++) {
			for (int k = 0; k < column1; k++) {
				MatrixC[i][j] += Matrix2[i][k] * Matrix1[k][j];
			}
		}
}
