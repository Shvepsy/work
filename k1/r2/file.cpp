#include <iostream>
#include <fstream>
#include <string>

using namespace std;

void strToFile(const string &name, const string &text) {
	ofstream out(name);
	out << text;
	out.close();
}

void printFile(const string &name) {
	ifstream in(name);
	cout << "[" << name << "] : ";
	char ch;
	while (in.read(&ch, sizeof(char))) {
		cout << ch;
	}
	cout << endl;
	in.close();
}

void encrypt(const string &fname, const string &crypted, const string &password) {
	ifstream in(fname);
	ofstream out(crypted);
	char ch;
	size_t index = 0;
	while (in >> ch) {
		ch = ch ^ password[index];
		index = (index + 1) % password.size();
		out.write(&ch, sizeof(char));
	}
	in.close();
	out.close();
}


void decrypt(const string &crypted, const string &fname, const string &password) {
	ifstream in(crypted);
	ofstream out(fname);
	size_t index = 0;
	char ch;
	while (in.read(&ch, sizeof(char))) {
		ch = ch ^ password[index];
		index = (index + 1) % password.size();
		out << ch;
	}
	in.close();
	out.close();
}

//  Д.з. Доделать перевод в hex и назад!
void fileAsHex(const string &fname, const string &hexfname) {
	ifstream in(fname);
	ofstream out(hexfname);
	const char* const hex = "0123456789ABCDEF";
	char ch;
	while (in.read(&ch, sizeof(char))) {
		out << hex[ch >> 4] << hex[ch & 15] << ' ';
	}
	in.close();
	out.close();
}

void unHex(const string &hexfname, const string &fname ) {
	ifstream in(hexfname);
	ofstream out(fname);
	const char* const hex = "0123456789ABCDEF";
	string st;
	getline (in, st, (char) in.eof());
	int len = st.length();
	for (int i = 0; i < len; i++) {
		if ((int)st[i] == 32 ) st.erase(i,1);
	};
	// char a = '0';
	// cout << (int)'0' << ' ' << (int)'9' << ' ' << (int)'A' << ' ' << (int)'F' << endl ;
	// cout << dec << st;
	int k = 0;
	for (int i = 0; i < len; i++ ) {
		for (int j = 0; j < 16; j++ ) {
			if (st[i] == hex[j] && i % 2 == 0) { k = j*16; }
			else if (st[i] == hex[j] && i % 2 == 1) { k = k + j; out << (char)k ;};
		}
	}
	in.close();
	out.close();
}

int main()
{
	strToFile("test.txt", "Example");
	printFile("test.txt");
	cout << "Stored\n";

	encrypt("test.txt", "hidden.dat", "sdfhsdfjsd");
	printFile("hidden.dat");
	cout << "Crypted\n";

	fileAsHex("hidden.dat", "hex.dat");
	printFile("hex.dat");
	cout << "Hexed\n";

	remove("test.txt");
	remove("hidden.dat");
	cout << "Cleared\n";

	unHex("hex.dat","hidden.dat");
	printFile("hidden.dat");
	cout << "Unhexed!\n";

	decrypt( "hidden.dat", "test.txt", "sdfhsdfjsd");
	printFile("test.txt");
	cout << "Decrypted\n";
}
