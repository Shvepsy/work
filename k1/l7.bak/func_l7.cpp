#include "func_l7.h"

void printStaticMatrix(int Matrix[N][N], int сolumn, int line) {
	for (int j = 0; j < line; j++) {
		for (int i = 0; i < сolumn; i++)
			cout << Matrix[i][j] << " ";
		cout << endl;
	}
	cout << endl;
}

void printDynamicMatrix(int **Matrix, int сolumn, int line ) {
	for (int j = 0; j < line; j++) {
		for (int i = 0; i < сolumn; i++)
			cout << Matrix[i][j] << " ";
		cout << endl;
	}
	cout << endl;
}

bool checkEqStaticMatrix(int Matrix1[N][N], int Matrix2[N][N], int сolumn1, int line1, int сolumn2, int line2 ) {
	if (сolumn1 != сolumn2 && line1 != line2)
		return false;
	for (int i = 0; i < сolumn1; i++)
		for (int j = 0; j < line2; j++) {
			if (Matrix1[i][j] != Matrix2[i][j])
				return false;
		}
	return true;
}

bool checkEqDynamicMatrix(int **Matrix1, int **Matrix2, int сolumn1, int line1, int сolumn2, int line2) {
	if (сolumn1 != сolumn2 && line1 != line2)
		return false;
	for (int i = 0; i < сolumn1; i++)
		for (int j = 0; j < line1; j++) {
			if (Matrix1[i][j] != Matrix2[i][j])
				return false;
		}
	return true;
}

void moveStaticMatCol(int Matrix[N][N], int zeroColNumber, int &сolumn, int line) {
	for (int i = zeroColNumber; i < сolumn-1; i++) {
		for (int j = 0; j < line; j++) {
			Matrix[i][j] = Matrix[i+1][j];
		}
	}
	сolumn -= 1;
}

void deleteStaticZeroCol(int Matrix[N][N], int &сolumn, int line) {
	// строка/столбец
	int i = 0, j = 0;
	bool Flag = true; // Флаг нулевого столбца
	while (i < сolumn) {

		while (Flag && j < line) {
			Flag = Matrix[i][j] == 0;
			j++;
		}
		j = 0;

		if (Flag) {
			moveStaticMatCol(Matrix, i, сolumn, line);
			i = -1;
		}
		Flag = true;
		i++;
	}
}

void moveDynamicMatCol(int **Matrix, int zeroColNumber, int &сolumn, int line) {
	for (int i = zeroColNumber; i < сolumn - 1; i++) {
		for (int j = 0; j < line; j++) {
			Matrix[i][j] = Matrix[i + 1][j];
		}
	}
	сolumn -= 1;
}

int** deleteDynamicZeroCol(int **Matrix, int &сolumn, int line) {
	// строка/столбец
	int i = 0, j = 0;
	bool Flag = true; // Флаг нулевого столбца
	int Kolit = 0;
	while (i < сolumn) {
		while (Flag && j < line) {
			Flag = Matrix[i][j] == 0;
			j++;
		}
		j = 0;
		if (Flag) {
			moveDynamicMatCol(Matrix, i, сolumn, line);
			Kolit++;
			i = -1;
		}
		Flag = true;
		i++;
	}

	int **HelpArray = new int*[сolumn];
	for (int i = 0; i < сolumn; i++)
		HelpArray[i] = new int[line];
	for (int i = 0; i < сolumn; i++)
		for (int j = 0; j < line; j++)
			HelpArray[i][j] = Matrix[i][j];

	delete[] Matrix;
	return HelpArray;
}

void insertStaticVecStr(int Matrix[N][N], int &сolumn, int &line, int VecStr[N][N], int Nomсolumn) {
	if (сolumn == Nomсolumn) {
		for (int j = 0; j < line; j++)
			Matrix[сolumn][j] = VecStr[0][j];
		сolumn++;
	}
	else {
		for (int i = сolumn-1; i > Nomсolumn - 1; i--)
			for (int j = 0; j < line; j++) {
				Matrix[i + 1][j] = Matrix[i][j];
			}
		сolumn++;
		for (int j = 0; j < line; j++)
			Matrix[Nomсolumn][j] = VecStr[0][j];
	}
}

int** insertDynamicVecStr(int **Matrix, int &сolumn, int line, int **VecStr, int Nomсolumn) {
	int **HelpArray = new int*[сolumn + 1];
	for (int i = 0; i < сolumn + 1; i++)
		HelpArray[i] = new int[line];
	for (int i = 0; i < сolumn; i++)
		for (int j = 0; j < line; j++)
			HelpArray[i][j] = Matrix[i][j];

	if (сolumn == Nomсolumn) {
		for (int j = 0; j < line; j++)
			HelpArray[сolumn][j] = VecStr[0][j];
		сolumn++;
	}
	else {
		for (int i = сolumn-1; i > Nomсolumn - 1; i--)
			for (int j = 0; j < line; j++) {
				HelpArray[i + 1][j] = HelpArray[i][j];
			}
		сolumn++;
		for (int j = 0; j < line; j++)
			HelpArray[Nomсolumn][j] = VecStr[0][j];
	}
	delete[] Matrix;
	return HelpArray;
}


void transposeStaticMatrix(int Matrix[N][N], int &сolumn, int &line) {
	int MAX;
	if (сolumn > line)
		MAX = сolumn;
	else
		MAX = line;

	int HelpEl;
	for (int i = 0; i < MAX -1; i++)
		for (int j = 1; j < MAX - i; j++) {
			HelpEl = Matrix[i + j][i];
			Matrix[i + j][i] = Matrix[i][i + j];
			Matrix[i][i + j] = HelpEl;
		}

	HelpEl = сolumn;
	сolumn = line;
	line = HelpEl;

}

int** transposeDynamicMatrix(int **Matrix, int &сolumn, int &line) {
	int** HelpArray = new int*[line];
	for (int i = 0; i < line; i++)
		HelpArray[i] = new int[сolumn];
	int HelpEl;
	for (int i = 0; i < сolumn; i++) {
		for (int j = 0; j < line; j++) {

			HelpArray[j][i] = Matrix[i][j];
		}
	}
	HelpEl = сolumn;
	сolumn = line;
	line = HelpEl;
	delete[] Matrix;

	return HelpArray;
}


void multiplyStaticMatrix(int Matrix1[N][N], int сolumn1, int line1, int Matrix2[N][N], int сolumn2, int line2, int MatrixC[N][N]) {
	for (int i = 0; i < line1; i++)
		for (int j = 0; j < сolumn2; j++) {
			for (int k = 0; k < сolumn1; k++) {
				MatrixC[i][j] += Matrix2[i][k] * Matrix1[k][j];
			}
		}
}

void multiplyDynamicMatrix(int **Matrix1, int сolumn1, int line1, int **Matrix2, int сolumn2, int line2, int **MatrixC) {
	for (int i = 0; i < line1; i++)
		for (int j = 0; j < сolumn2; j++) {
			for (int k = 0; k < сolumn1; k++) {
				MatrixC[i][j] += Matrix2[i][k] * Matrix1[k][j];
			}
		}
}
