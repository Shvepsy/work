#pragma once
#include <iostream>

const int N = 5;
void printStaticMatrix(int Matrix[N][N], int сolumn, int line);
void printDynamicMatrix(int **Matrix, int сolumn, int line);
bool checkEqStaticMatrix(int Matrix1[N][N], int Matrix2[N][N], int сolumn1, int line1, int сolumn2, int line2);
bool checkEqDynamicMatrix(int **Matrix1, int **Matrix2, int сolumn1, int line1, int сolumn2, int line2);
void moveStaticMatCol(int Matrix[N][N], int zeroColNumber, int &сolumn, int line);
void deleteStaticZeroCol(int Matrix[N][N], int &сolumn, int line);
void moveDynamicMatCol(int **Matrix, int zeroColNumber, int &сolumn, int line);
int** deleteDynamicZeroCol(int **Matrix, int &сolumn, int line);
void insertStaticVecStr(int Matrix[N][N], int &сolumn, int &line, int VecStr[N][N], int Nomсolumn);
int** insertDynamicVecStr(int **Matrix, int &сolumn, int line, int **VecStr, int Nomсolumn);
void transposeStaticMatrix(int Matrix[N][N], int &сolumn, int &line);
int** transposeDynamicMatrix(int **Matrix, int &сolumn, int &line);
void multiplyStaticMatrix(int Matrix1[N][N], int сolumn1, int line1, int Matrix2[N][N], int сolumn2, int line2, int MatrixC[N][N]);
void multiplyDynamicMatrix(int **Matrix1, int сolumn1, int line1, int **Matrix2, int сolumn2, int line2, int **MatrixC);
