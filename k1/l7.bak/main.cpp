#include "func_l7.h"

int main() {

	cout << "Q0:" << endl;

	int m01 = 4, n01 = 3;
	cout << "Staric arrays:" << endl << endl;

	cout << "Matrix 1" << endl;
	int StatArray01[N][N];
	for (int i = 0; i < m01; i++)
		for (int j = 0; j < n01; j++)
			StatArray01[i][j] = 1;

	printStaticMatrix(StatArray01, m01, n01);

	cout << "Matrix 2" << endl;
	int StatArray02[N][N];
	for (int i = 0; i < m01; i++)
		for (int j = 0; j < n01; j++)
			StatArray02[i][j] = 1;
	StatArray02[1][1] = 5;

	printStaticMatrix(StatArray02, m01, n01);

	cout << "M1 == M2 ? " << checkEqStaticMatrix(StatArray01, StatArray02, m01, n01, m01, n01) << endl << endl;

	cout << "Dynamic arrays:" << endl << endl;

	cout << "Matrix 1" << endl;
	int **DinArray01 = new int*[m01];
	for (int i = 0; i < m01; i++) {
		DinArray01[i] = new int[n01];
	}

	for (int i = 0; i < m01; i++)
		for (int j = 0; j < n01; j++)
			DinArray01[i][j] = 1;

	printDynamicMatrix(DinArray01, m01, n01);

	cout << "Matrix 2" << endl;
	int **DinArray02 = new int*[m01];
	for (int i = 0; i < m01; i++) {
		DinArray02[i] = new int[n01];
	}

	for (int i = 0; i < m01; i++)
		for (int j = 0; j < n01; j++)
			DinArray02[i][j] = 1;
	printDynamicMatrix(DinArray02, m01, n01);

	cout << "M1 == M2 ? " << checkEqDynamicMatrix(DinArray01, DinArray02, m01, n01, m01, n01) << endl;

	for (int i = 0; i < m01; i++) {
		delete[] DinArray01[i];
		delete[] DinArray02[i];
	}
	delete[] DinArray01;
	delete[] DinArray02;


	cout << endl << endl << "Q2:" << endl;
	cout << "Staric arrays:" << endl << endl;

	cout << "Source matrix" << endl;
	for (int i = 0; i < m01; i++)
		for (int j = 0; j < n01; j++)
			StatArray01[i][j] = i*j+1;

	for (int j = 0; j < n01; j++) StatArray01[1][j] = 0; // Заполняем стобец нулями

	printStaticMatrix(StatArray01, m01, n01);
	deleteStaticZeroCol(StatArray01, m01, n01);
	cout << "Matrix without zero column" << endl;
	printStaticMatrix(StatArray01, m01, n01);

	cout << "Dynamic arrays:" << endl << endl;

	int m1 = 4, n1 = 3;
	cout << "Source matrix" << endl;
	int **DinArray1 = new int*[m1];
	for (int i = 0; i < m1; i++) {
		DinArray1[i] = new int[n1];
	}

	for (int i = 0; i < m1; i++)
		for (int j = 0; j < n1; j++)
			DinArray1[i][j] = i*j + 1;
	for (int j = 0; j < m1; j++) DinArray1[2][j] = 0;  // Заполняем стобец нулями

	printDynamicMatrix(DinArray1, m1, n1);

	DinArray1 = deleteDynamicZeroCol(DinArray1, m1, n1);

	cout << "Matrix without zero strings" << endl;
	printDynamicMatrix(DinArray1, m1, n1);

	for (int i = 0; i < m1; i++)
		delete[] DinArray1[i];
	delete[] DinArray1;

	cout << endl << "Q2:" << endl;
	cout << "Staric arrays:" << endl << endl;
	int m21 = 4, n21 = 3;

	cout << "Matrix 1" << endl;
	int StatArray2[N][N];
	for (int i = 0; i < m21; i++)
		for (int j = 0; j < n21; j++)
			StatArray2[i][j] = i*j + 1;

	printStaticMatrix(StatArray2, m21, n21);
	int VSto = 1, VStr = 3;
	int VecStatStr[N][N] {9, 9, 9};
	cout << "String vector" << endl;
	printStaticMatrix(VecStatStr, 1, 3);
	insertStaticVecStr(StatArray2, m21, n21, VecStatStr, 3);

	cout << "Matrix with string vector" << endl;
	printStaticMatrix(StatArray2, m21, n21);

	cout << "Dynamic arrays:" << endl << endl;

	int m22 = 4, n22 = 3;
	int **DinArray2 = new int*[m22];
	for (int i = 0; i < m22; i++) {
		DinArray2[i] = new int[n22];
	}

	for (int i = 0; i < m22; i++)
		for (int j = 0; j < n22; j++)
			DinArray2[i][j] = i*j + 1;

	cout << "Matrix 1" << endl;
	printDynamicMatrix(DinArray2, m22, n22);

	int **VecDinStr = new int*[N];
	VecDinStr[0] = new int[N];

	VecDinStr[0][0] = 9;
	VecDinStr[0][1] = 9; // Вектор строка
	VecDinStr[0][2] = 9;

	cout << "String vector" << endl;
	printDynamicMatrix(VecDinStr, 1, 3);

	DinArray2 = insertDynamicVecStr(DinArray2, m22, n22, VecDinStr, 2);

	cout << "Matrix with string vector" << endl;

	printDynamicMatrix(DinArray2, m22, n22);

	for (int i = 0; i < m22; i++)
		delete[] DinArray2[i];
	delete[] DinArray2;


	cout << endl << "Q3:" << endl;

	cout << "Dynamic arrays:" << endl << endl;

	int m31 = 4, n31 = 3;
	int StatArray3[N][N];
	int num = 1;
	for (int i = 0; i < m31; i++)
		for (int j = 0; j < n31; j++) {
			StatArray3[i][j] = num;
			num++;
		}
	cout << "Source matrix" << endl;

	printStaticMatrix(StatArray3, m31, n31);

	transposeStaticMatrix(StatArray3, m31, n31);

	cout << "Transposed matrix" << endl;
	printStaticMatrix(StatArray3, m31, n31);

	cout << "Dynamic arrays:" << endl << endl;

	int m32 = 4, n32 = 3;
	int** DinArray3 = new int*[m32];
	for (int i = 0; i < m32; i++)
		DinArray3[i] = new int[n32];

	num = 1;
	for (int i = 0; i < m32; i++)
		for (int j = 0; j < n32; j++) {
			DinArray3[i][j] = num;
			num++;
		}

	cout << "Source matrix" << endl;
	printDynamicMatrix(DinArray3, m32, n32);

	cout << "Transposed matrix" << endl;

	DinArray3 = transposeDynamicMatrix(DinArray3, m32, n32); // Утечка памяти

	printDynamicMatrix(DinArray3, m32, n32);

	for (int i = 0; i < m32; i++)
		delete[] DinArray3[i];
	delete[] DinArray3;



	cout << endl << "Q4:" << endl;

	cout << "Dynamic arrays:" << endl << endl;

	int m41 = 4, n41 = 3;
	int StatArray41[N][N];
	num = 1;
	for (int i = 0; i < m41; i++)
		for (int j = 0; j < n41; j++) {
			StatArray41[i][j] = num;
			num++;
		}

	cout << "Matrix 1" << endl;

	printStaticMatrix(StatArray41, m41, n41);

	int m42 = 3, n42 = 4;
	int StatArray42[N][N];
	num = 1;
	for (int i = 0; i < m42; i++)
		for (int j = 0; j < n42; j++) {
			StatArray42[i][j] = num;
			num++;
		}

	cout << "Matrix 2" << endl;

	printStaticMatrix(StatArray42, m42, n42);

	int m43 = m42, n43 = m42;
	int StatArray43[N][N];

	for (int i = 0; i < m43; i++)
		for (int j = 0; j < n43; j++) {
			StatArray43[i][j] = 0;
		}

	cout << "Multiplication matrix" << endl << endl;

	multiplyStaticMatrix(StatArray41, m41, n41, StatArray42, m42, n42, StatArray43);

	printStaticMatrix(StatArray43, m42, n43);


	cout << "Dynamic arrays:" << endl << endl;

	int m44 = 4, n44 = 3;
	cout << "Matrix 1" << endl;
	int **DinArray41 = new int*[m44];
	for (int i = 0; i < m44; i++) {
		DinArray41[i] = new int[n44];
	}

	num = 1;
	for (int i = 0; i < m44; i++)
		for (int j = 0; j < n44; j++){
			DinArray41[i][j] = num;
			num++;
		}

	printDynamicMatrix(DinArray41, m44, n44);

	int m45 = n44, n45 = m44;

	int **DinArray42 = new int*[m45];
	for (int i = 0; i < m45; i++) {
		DinArray42[i] = new int[n45];
	}
	num = 1;
	for (int i = 0; i < m45; i++)
		for (int j = 0; j < n45; j++) {
			DinArray42[i][j] = num;
			num++;
		}

	cout << "Matrix 2" << endl;
	printDynamicMatrix(DinArray42, m45, n45);

	int m46 = n44, n46 = n44;

	int **DinArray43 = new int*[m46];
	for (int i = 0; i < m46; i++) {
		DinArray43[i] = new int[n46];
	}

	for (int i = 0; i < m46; i++)
		for (int j = 0; j < n46; j++)
			DinArray43[i][j] = 0;

	multiplyDynamicMatrix(DinArray41, m44, n44, DinArray42, m45, n45, DinArray43);

	printDynamicMatrix(DinArray43, m46, n46);

	for (i = 0; i < m44; i++)
		delete[] DinArray41[i];
	delete[] DinArray43;

	for (i = 0; i < m45; i++)
		delete[] DinArray42[i];
	delete[] DinArray43;

	for (i = 0; i < m46; i++)
		delete[] DinArray43[i];
	delete[] DinArray43;

	return 0;
}
