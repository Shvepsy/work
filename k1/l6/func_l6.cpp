#include "func_l6.h"
#include <cstring>
#include <cmath>

void printArray(int *arr, int n) {

	for (int i = 0; i < n; i++)
		cout << arr[i] << " ";
	cout << endl;
}

void printArray(double *arr, int n) {

	for (int i = 0; i < n; i++)
		cout << arr[i] << " ";
	cout << endl;
}

int* DupBetw2Zeros(int *Array, int &ArLength) {

	// Проверка нулей
	int i = 0;
	int ZeroPos1 = -1, ZeroPos2 = -1;

	while (i < ArLength) {
		if (Array[i] == 0) {
			if (((ZeroPos1 == -1) && (ZeroPos1 != -1)) || (ZeroPos1 != -1)) {
				ZeroPos2 = i;
			}
			else {
				ZeroPos1 = i;
			}
		}
		i++;
	}
	if (ZeroPos1 == -1) {
		throw "Array without 2 zeros";
	}


	int hArLength = ArLength + ZeroPos2 - ZeroPos1 - 1;
	int *hArr = new int[hArLength];

	memcpy(hArr, Array, sizeof(int)*hArLength);

	delete[] Array;

	int NewLen = ArLength;
	for (int i = ZeroPos1 + 1; i < ZeroPos2 * 2 - ZeroPos1 - 2; i += 2) {
		for (int j = NewLen - 1; j > i; j--) {
			hArr[j + 1] = hArr[j];
		}
		hArr[i + 1] = hArr[i];
		NewLen++;
	}

	ArLength = hArLength;

	return hArr;
}

double* DelLessAr(double *Array, int &ArLength) {
	// Cреднее арифмитическое
	double SumEl = 0;
	int i = 0;
	while (i < ArLength) {
		SumEl += Array[i];
		i++;
	}
	double SrAr = SumEl / i;

	int NewLen = ArLength;
	i = 0;
	while (i < ArLength) {
		if (Array[i] < SrAr) {
			for (int j = i; j < NewLen-1; j++) {
				Array[j] = Array[j+1];
			}
			NewLen--;
		}
		else {
			i++;
		}
		if (Array[NewLen] < SrAr) {
			NewLen--;
		}

	}

	double *hArr = new double[NewLen];
	memcpy(hArr, Array, sizeof(double)*NewLen);
	delete[] Array;

	ArLength = NewLen;

	return hArr;
}

int* DupBetwZero(int* Array, int &ArLength) {

	// Проверка наличия нулей
	int i = 0;
	int ZeroPos1 = -1, ZeroPos2 = -1;

	while (i < ArLength) {
		if (Array[i] == 0) {
			if (((ZeroPos1 == -1) && (ZeroPos1 != -1)) || (ZeroPos1 != -1)) {
				ZeroPos2 = i;
			}
			else {
				ZeroPos1 = i;
			}
		}
		i++;
	}
	if (ZeroPos1 == -1) {
		throw "Array without 2 zeros";
	}

	int* hArr = new int[ArLength*2];

	memcpy(hArr, Array, sizeof(int)*ArLength*2);
	delete[] Array;

	int NewLen = ArLength;

	ZeroPos1 = -1; ZeroPos2 = -1;
	i = 0;
	while (i < ArLength * 2) {

		if (hArr[i] == 0) {
			if (((ZeroPos1 == -1) && (ZeroPos1 != -1)) || (ZeroPos1 != -1)) {
				ZeroPos2 = i;
			}
			else {
				ZeroPos1 = i;
			}
		}
		i++;

		if (ZeroPos2 != -1) {
			// Копируем элементы между ZeroPos1 и ZeroPos2
			for (int i = ZeroPos1 + 1; i < ZeroPos2 * 2 - ZeroPos1 - 2; i += 2) {
				for (int j = NewLen - 1; j > i; j--) {
					hArr[j + 1] = hArr[j];
				}
				hArr[i + 1] = hArr[i];
				NewLen++;
			}
			ZeroPos1 = -1; i = ZeroPos2;  ZeroPos2 = -1;
		}
	}

	int* nArr = new int[NewLen];
	memcpy(nArr, hArr, sizeof(int)*NewLen);
	delete[] hArr;

	ArLength = NewLen;
	return nArr;
}

int* DeleteA2FromA1(int *Arr1, int *Arr2, int &Arr1Length, int Arr2Length) {

	for (int i = 0; i < Arr2Length; i++) {
		for (int j = 0; j < Arr1Length; j++) {
			if (Arr2[i] == Arr1[j]) {
				for (int h = j; h < Arr1Length-1; h++) {
					Arr1[h] = Arr1[h + 1];
				}
				Arr1Length--;
			}
		}
	}

	if (Arr1Length == 0) {
		cout << "First array included in second";
		return 0;
	}
	else {
		int* hArr = new int[Arr1Length];
		memcpy(hArr, Arr1, sizeof(int)*Arr1Length);
		delete[] Arr1;
		return hArr;
	}
}

double* InsertElDif(double* Arr, int &ArrLength) {

	int hArrLength = ArrLength + (ArrLength - 1);
	int NewLen = ArrLength;
	double* hArr = new double[hArrLength];
	memcpy(hArr, Arr, sizeof(double)*hArrLength);
	double Dif;
	for (int i = 0; i < hArrLength-1; i+=2) {
		Dif = abs(hArr[i] - hArr[i + 1]);
		for (int j = NewLen - 1; j > i; j--) {
			hArr[j + 1] = hArr[j];
		}
		NewLen++;
		hArr[i+1] = Dif;
	 }
	ArrLength = NewLen;
	return hArr;
}

void Merge(int *A, int A1Len, int *B, int A2Len, int *C) {
	int *NewArr = new int[A1Len+A2Len];
	int a = 0, b = 0;
	while (a + b < A1Len + A2Len) {
		if ( (b >= A2Len) || ( a<A1Len && A[a] <= B[b] )) {
			NewArr[a + b] = A[a];
			a++;
		}
		else {
			NewArr[a + b] = B[b];
			b++;
		}
	}
	memcpy(C, NewArr, sizeof(int)*(A1Len + A2Len));
}

void MergeSort(int *A, int n) {
	if (n == 1) return;
	if (n == 2) {
		if (A[0] > A[1]) {
			int t = A[0];
			A[0] = A[1];
			A[1] = t;
		}
		return;
	}
	MergeSort(A, n / 2);
	MergeSort(A + n / 2, n - n / 2);
	int *B = new int[n];
	Merge(A, n / 2, A + n / 2, n - n / 2, B);

	for (int i = 0; i<n; i++)
		A[i] = B[i];

	delete[] B;
}
