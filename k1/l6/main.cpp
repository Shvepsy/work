// здесь основной файл

#include "func_l6.h"
#include "test_l6.h"

int main() {

	cout << "Q0:" << endl << endl;

	int ArrLen = 11;
	int *Array_Q0 = new int[ArrLen] {1, 4, 0, 5, 28, 0, 3, 5, 0, 12, 8};

	cout << "Source array: " << endl;
	printArray(Array_Q0, ArrLen);

	try {
		Array_Q0 = DupBetw2Zeros(Array_Q0, ArrLen);
		cout << "Edited array: " << endl;
		printArray(Array_Q0, ArrLen);
	}
	catch (char(*c)) {
		cout << c << endl;
	}
	delete[] Array_Q0;

	cout << endl << "Q1:" << endl << endl;

	ArrLen = 5;
	double *Array_Q1 = new double[ArrLen] {1, 5, 3, 6, 4};

	cout << "Source array: " << endl;
	printArray(Array_Q1, ArrLen);

	Array_Q1 = DelLessAr(Array_Q1, ArrLen);
	cout << "Edited array: " << endl;
	printArray(Array_Q1, ArrLen);

	delete[] Array_Q1;


	cout << endl << "Q2:" << endl << endl;

	ArrLen = 9;
	int *Array_Q2 = new int[ArrLen] {1, 0, 2, 0, 5, 0, 3, 0, 6};

	cout << "Source array: " << endl;

	printArray(Array_Q2, ArrLen);
	Array_Q2 = DupBetwZero(Array_Q2, ArrLen);

	cout << "Edited array: " << endl;
	printArray(Array_Q2, ArrLen);

	delete[] Array_Q2;


	cout << endl << "Q3:" << endl << endl;

	ArrLen = 4;
	int *Array_Q3 = new int[ArrLen] {1, 4, 5, 7};

	int Arr2Len = 6;
	int *Array_Q3_2 = new int[Arr2Len] {2, 3, 5, 9, 5, 1};
	cout << "Source first array: " << endl;
	printArray(Array_Q3, ArrLen);
	cout << "Source second array: " << endl;
	printArray(Array_Q3_2, Arr2Len);

	Array_Q3 = DeleteA2FromA1(Array_Q3, Array_Q3_2, ArrLen, Arr2Len);

	delete[] Array_Q3_2;

	cout << "First array without elements from secondary: " << endl;
	printArray(Array_Q3, ArrLen);

	delete[] Array_Q3;


	cout << endl << "Q4:" << endl << endl;

	ArrLen = 4;
	double *Array_Q4 = new double[ArrLen] {2, 4, 5, 35};

	cout << "Source array: " << endl;
	printArray(Array_Q4, ArrLen);

	Array_Q4 = InsertElDif(Array_Q4, ArrLen);

	cout << "Edited array: " << endl;
	printArray(Array_Q4, ArrLen);

	delete[] Array_Q4;

	cout << endl << "Q5:" << endl << endl;
	ArrLen = 6;
	int *Array_Q5 = new int[ArrLen] {4, 25, 5, 10, 15, 1};

	cout << "Source array: " << endl;
	printArray(Array_Q5, ArrLen);

	cout << "Edited array: " << endl;
	MergeSort(Array_Q5, ArrLen);
	printArray(Array_Q5, ArrLen);
	delete[] Array_Q5;

	cout << endl;
	test_all();
	cout << endl;

	return 0;
}
