#pragma once

#include <iostream>

using namespace std;

// Печать массива
void printArray(int *arr, int n);
void printArray(double *arr, int n);

// Дублирование элементов между первым и последним нулями
int* DupBetw2Zeros(int *Array, int &ArLength);

// Удаление элементов, меньших среднего арифметического
double* DelLessAr(double *Array, int &ArLength);

// Дублирование элементов между всеми нулями
int* DupBetwZero(int* Array, int &ArLength);

// Удаление вхождений второго массива в первом
int* DeleteA2FromA1(int *Arr1, int *Arr2, int &Arr1Length, int Arr2Length);

// Вставка разности между элементами
double* InsertElDif(double* Arr, int &ArrLength);

// Слияние массивов
void Merge(int *A, int A1Len, int *B, int A2Len, int *C);

// Сортировка слиянием
void MergeSort(int *A, int n);
