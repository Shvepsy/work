#include <cassert>
#include "func_l6.h"

bool test_DupBetw2Zeros() {
	int Len = 4;
	int *Arr = new int[Len] {0, 2, 4, 0};
	Arr = DupBetw2Zeros(Arr, Len);
	assert((Arr[0] == 0) && (Arr[1] == 2) && (Arr[2] == 2) && (Arr[3] == 4) && (Arr[4] == 4) && (Arr[5] == 0));
	delete[] Arr;
	// Если 0 один, то вылетает исключение

	cout << "Test DupBetw2Zeros = OK" << endl;

	return true;
}

bool test_DelLessAr() {
	int Len = 5;
	double *Arr = new double[Len] {5, 2, 3, 6, 4}; // Среднее арифиметическое = 4
	Arr = DelLessAr(Arr, Len);
	assert((Arr[0] == 5) && (Arr[1] == 6));

	delete[] Arr;

	cout << "Test DelLessAr = OK" << endl;

	return true;
}

bool test_DupBetwZero() {
	int Len = 6;
	int *Arr = new int[Len] {0, 1, 2, 0, 4, 0};
	Arr = DupBetwZero(Arr, Len);
	assert( (Arr[0] == 0) && (Arr[1] == 1) && (Arr[2] == 1) && (Arr[3] == 2) && (Arr[4] == 2) && (Arr[5] == 0)
		&& (Arr[6] == 4) && (Arr[7] == 4) && (Arr[8] == 0) );

	delete[] Arr;

	cout << "Test DupBetwZero = OK" << endl;

	return true;
}

bool test_DeleteA2FromA1() {
	int Len1 = 3;
	int *Arr1 = new int[Len1] {0, 1, 2};
	int Len2 = 3;
	int *Arr2 = new int[Len2] {0, 3, 2};
	Arr1 = DeleteA2FromA1(Arr1, Arr2, Len1, Len2);

	assert( Arr1[0] == 1 );

	delete[] Arr1;
	delete[] Arr2;

	/*
	Len1 = 3;
	int *Arr3 = new int[Len1] {0, 1, 2};
	Len2 = 3;
	int *Arr4 = new int[Len2] {0, 1, 2};

	Arr3 = DeleteA2FromA1(Arr3, Arr4, Len1, Len2);
	printArray(Arr3, Len1);
	assert(Arr3 == 0);

	delete[] Arr3;
	delete[] Arr4;
	*/

	cout << "Test DeleteA2FromA1 = OK" << endl;

	return true;
}

bool test_InsertElDif() {
	int Len = 4;
	double *Arr = new double[Len] {0, 1, 4, 10};
	Arr = InsertElDif(Arr, Len);
	assert( (Arr[0] == 0) && (Arr[1] == 1) && (Arr[2] == 1) && (Arr[3] == 3) && (Arr[4] == 4) && (Arr[5] == 6)
		&& (Arr[6] == 10) );

	cout << "Test InsertElDif = OK" << endl;

	return true;
}

bool test_MergeSort() {
	int Len = 4;
	int *Arr = new int[Len] {10, 1, 16, 10};
	MergeSort(Arr, Len);
	assert((Arr[0] == 1) && (Arr[1] == 10) && (Arr[2] == 10) && (Arr[3] == 16));

	cout << "Test MergeSort = OK" << endl;

	return true;
}

bool test_all() {
	return test_DupBetw2Zeros() && test_DelLessAr() && test_DupBetwZero() && test_DeleteA2FromA1()
		&& test_InsertElDif() && test_MergeSort();
}
