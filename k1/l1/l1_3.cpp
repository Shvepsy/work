#include <iostream>
#include <cmath>

using namespace std;
int main() {
	int n;
	cout << "N = ";
	cin >> n;
	if (n > 0) {
		float s = 0;
		for (int i = 1; i <= n; i++) {
			s = s + (float)(pow(i,(n-i+1)));
			cout << i << " " << (i^(n-i+1)) << "\t" << s << endl;
		}
		cout << "S = " << s << endl;
	}
	else {
		cout << n << "N <= 0" << endl;
	}
	return 0;
}
