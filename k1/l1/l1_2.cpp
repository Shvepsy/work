#include <iostream>

using namespace std;
int main() {
	int n,m;
	cout << "Enter <n> <m>" << endl;
	cin >> n >> m;
	if ( ((n >= 6) && (n <= 14)) && ((m >= 1) && (m <= 4)) ) {
		switch (n) {
			case 11: {cout << "valet "; break;}
			case 12: {cout << "dame "; break;}
			case 13: {cout << "king "; break;}
			case 14: {cout << "ace "; break;}
			default: {cout << n << " " ; break;}
		}
		switch (m) {
			case 1: {cout << "pics" << endl; break;}
			case 2: {cout << "cross" << endl; break;}
			case 3: {cout << "rhombus" << endl; break;}
			case 4: {cout << "heart" << endl; break;}
			default: {break;}
		}
	}
	else {
		cout << "m or n out of range" << endl;
	}
	return 0;
}
