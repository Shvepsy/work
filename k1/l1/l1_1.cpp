#include <iostream>

using namespace std;
int main() {
	int x1,y1,x2,y2;
	cout << "Enter <x1> <y1> <x2> <y2>" << endl;
	cin >> x1 >> y1 >> x2 >> y2;
	if ((x1+y1) % 2) == ((x2+y2) % 2)) {
		cout << "One color" << endl;
	}
	else {
		cout << "Different colors" << endl;
	}
	return 0;
}
