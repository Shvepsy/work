#include <iostream>

using namespace std;
int main() {
	int y;
	cout << "Enter year : ";
	cin >> y;
	if (((y % 4) == 0) && (!(y % 100) == 0 || (y % 400) == 0)) {
		cout << "366 days" << endl;
	}
	else {
		cout << "365 days" << endl;
	}
	return 0;
}
