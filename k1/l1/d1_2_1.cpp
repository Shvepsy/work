#include <iostream>

using namespace std;
int main() {
	int d,m;
	cout << "Enter <day> <month>" << endl;
	cin >> d >> m ;
	if (((d >= 1) && (d <= 31)) && ((m >= 1) && (m <= 12))) {
		switch (m) {
			case 1: {cout << ((d < 20)?"Capricorn":"Aquarius") << endl; break;}
			case 2: {cout << ((d < 19)?"Aquarius":"Pisces") << endl; break;}
			case 3: {cout << ((d < 21)?"Pisces":"Aries") << endl; break;}
			case 4: {cout << ((d < 20)?"Aries":"Taurus") << endl; break;}
			case 5: {cout << ((d < 21)?"Taurus":"Gemini") << endl; break;}
			case 6: {cout << ((d < 22)?"Gemini":"Cancer") << endl; break;}
			case 7: {cout << ((d < 23)?"Cancer":"Leo") << endl; break;}
			case 8: {cout << ((d < 23)?"Leo":"Virgo") << endl; break;}
			case 9: {cout << ((d < 23)?"Virgo":"Libra") << endl; break;}
			case 10: {cout << ((d < 23)?"Libra":"Scorpio") << endl; break;}
			case 11: {cout << ((d < 23)?"Scorpio":"Sagittarius") << endl; break;}
			case 12: {cout << ((d < 22)?"Sagittarius":"Capricorn") << endl; break;}
			default: { break;}
		}
	}
	else {
		cout << "day or month out of range" << endl;
	}
	return 0;
}
