#include <cmath>
#include <iostream>

#define YI 0.5772157
#define EPS 1E-6
using namespace std;

bool arr_sum(int *arr, int c, int d) {
  bool b = false;
  for (int i = 0; i < c ; i++ ) {
    int in = arr[i] / 10 ;
    int s = arr[i] % 10 ;
    while (in != 0 ) {
      s += in % 10 ;
      in = in / 10 ;
      cout << s << " " << in << endl;
    }
    if (s == d) {
      cout << arr[i] << endl;
      b = true ;
    }
  }
  return b;
}

int factorial(int n) {
  if (n < 2) {
    return 1;
  }
  else {
    return n*factorial(n-1);
  }
}

double ci(double x) {
  int i = 1;
  double s = YI + log(x);
  double e = (-1)*pow(x,i*2)/(i*2*factorial(i*2)) ;
  while ( s-abs(e) > EPS ) {
    s += e;
    i++;
    cout << s << endl;
    e = pow(-1,i)*pow(x,i*2)/(i*2*factorial(i*2)) ;
  }
  return s ;
}

double sum(int n, double m, double d) {
  if (n == 0) return 0 ;
  return m + sum(n-1,m+d,d);
}
