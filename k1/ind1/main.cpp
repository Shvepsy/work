#include <cmath>
#include <iostream>
#include "func.h"
#include "test.h"
using namespace std;

int main() {
  int item = 0;
  do {
    cout << "Please select required item:" << endl;
    cout << "1. Eq sum and count of elements" << endl;
    cout << "2. Integral cos" << endl;
    cout << "3. Recursion sum" << endl;
    cout << "4. Test all" << endl << "> ";
    cin >> item;
  } while (item < 1 | item > 4);
  int n,k;
  double m,d,x;
  switch (item) {
    case 1: {
      cout << "Please enter count of elements: ";
      cin >> k;
      int a[k];
      for (int i = 0; i < k; i++ ) {
        cout << i+1 << " element: ";
        cin >> a[i];
      }
      cout << "Please enter n: ";
      cin >> n;
      arr_sum(a,k,n);
      break;
    }

    case 2: {
      cout << "Please enter x = ";
      cin >> x;
      cout << ci(x);
      break;
    }
    case 3: {
      cout << "Please enter n: ";
      cin >> n;
      cout << "Please enter m: ";
      cin >> m;
      cout << "Please enter d: ";
      cin >> d;
      cout << sum(n,m,d) << endl;
      break;
    }
    case 4: {
      if (test_all()) {
        cout << "Test: OK" << endl;
      }
      else {
        cout << "Test: ERR" << endl;
      }
      break;
    }
  };

  return 0;
}
