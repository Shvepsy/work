#pragma once

void PrintArray(int *Arr, int ArrLength);
void PrintArray(double *Arr, int ArrLength);
void Merge(int *Arr1, int A1Len, int *Arr2, int A2Len, int *C);
void MergeSort(int *Arr, int ArrLength);
void BubleSort(int* Arr, int ArrLength);
void InsertSort(int *Arr, int ArrLength);
double result(double *Arr, int ArrLength, double x);
void RecShiftR(int *Arr, int ArrLength, int i);
int BinSearch(int *Arr, int s, int r, int l);
int CheckNumberVar(int *ArrVar, int ArrLength, int nEl, int *ArrSeq, double S, int &Pods);
