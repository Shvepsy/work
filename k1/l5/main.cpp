#include <cstdlib>
#include <ctime>
#include <iostream>
#include "func_l5.h"

using namespace std;

int main() {
	srand(time(0));
	const int LEN = 40000;

	cout << "Q1:" << endl;

	int testArr[LEN];
	for (int i = 0; i < LEN; i++)
		testArr[i] = rand() % 15;

	cout << "MergeSort time check" << endl;

	unsigned int start_time = clock();
	MergeSort(testArr, LEN);
	unsigned int end_time = clock();
	cout << "MergeSort (unsorted array) time: " << (end_time - start_time) << endl;

	start_time = clock();
	MergeSort(testArr, LEN);
	end_time = clock();
	cout << "MergeSort (sorted array) time: " << (end_time - start_time) << endl;

	for (int i = 0; i < LEN; i++)
		testArr[i] = rand() % 15;

	cout << "BubleSort time check" << endl;
	start_time = clock();
	BubleSort(testArr, LEN);
	end_time = clock();
	cout << "BubleSort (unsorted array) time: " << (end_time - start_time) << endl;
	start_time = clock();
	BubleSort(testArr, LEN);
	end_time = clock();
	cout << "BubleSort (sorted array) time: " << (end_time - start_time) << endl;

	for (int i = 0; i < LEN; i++)
		testArr[i] = rand() % 15;

	cout << "InsertSort time check" << endl;
	start_time = clock();
	InsertSort(testArr, LEN);
	end_time = clock();
	cout << "InsertSort (unsorted array) time: " << (end_time - start_time) << endl;
	start_time = clock();
	InsertSort(testArr, LEN);
	end_time = clock();
	cout << "InsertSort (unsorted array) time: " << (end_time - start_time) << endl;

	cout << "Q2:" << endl;

	cout << "Recurring shift" << endl;
	const int N = 6;
	int Ar[N] = { 8, 2, 121, 51, 4, 54 };
	PrintArray(Ar, N);

	RecShiftR(Ar, N, 0);
	cout << "After shift:" << endl;
	PrintArray(Ar, N);


	cout << "Q3:" << endl;

	double Ar3[N];
	for (int i = 0; i < N; i++) {
			Ar3[i] = int( ( (double)rand() / ( (RAND_MAX / 10) ) * 100) ) /100.0 ; // random R
	}
	cout << "Gorner scheme" << endl;
	PrintArray(Ar3, N);
	double x;
	cout << "Please enter x:" << endl;
	cin >> x;
	// x = 1;
	cout << "Polynomial value in " << x << " = " << result(Ar3, N-1, x) << endl;


	cout << "Q4:" << endl;

	int BSarr[N];
	for (int i = 0; i < N; i++) {
		BSarr[i] = i+1;
	}
	cout << "Binary search:" << endl;
	PrintArray(BSarr, N);
	InsertSort(BSarr, N);
	start_time = clock();
	cout << "Result = " << BinSearch(BSarr, 3, N, 0) << endl;
	end_time = clock();
	cout << "Binary search time: " << (end_time - start_time) << endl;

	cout << "Q5:" << endl;

	cout << "Sign replace:" << endl;
	const int N1 = 4;
	int Seq[N1] = { 4, 2, 6, 3 };
	PrintArray(Seq, N1);

	int S = 3;
	cout << "Count of signs: " << S << endl;

	int Sgn[N1 - 1];
	int vars = 0;
	CheckNumberVar(Sgn, N1 - 1, 0, Seq, S, vars);
	cout << "Finded " << vars << " variants." << endl;

	return 0;
}
