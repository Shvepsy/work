#include <cstring>
#include <iostream>
#include <algorithm>
using namespace std;

void PrintArray(int *Arr, int ArrLength) {
	for (int i = 0; i < ArrLength; i++) {
		cout << Arr[i] << " ";
	}
	cout << endl;
}

void PrintArray(double *Arr, int ArrLength) {
	for (int i = 0; i < ArrLength; i++) {
		cout << Arr[i] << " ";
	}
	cout << endl;
}

int CheckNumberVar( int *ArrVar, int ArrLength, int nEl, int *ArrSeq, double S, int &Pods) {
	for (int i = nEl; i < 1 + nEl; i++) {
		for (int j = 1; j < 5; j++) {
			ArrVar[i] = j;
			if (nEl != ArrLength-1)
				// Recurre filling
				CheckNumberVar(ArrVar, ArrLength, nEl + 1, ArrSeq, S, Pods);
			else {
				double Sum = ArrSeq[0];
				for (int g = 0; g < ArrLength; g++) {
					switch (ArrVar[g]) {
					case 1: Sum += ArrSeq[g + 1]; break;
					case 2: Sum -= ArrSeq[g + 1]; break;
					case 3: Sum *= ArrSeq[g + 1]; break;
					case 4: Sum /= ArrSeq[g + 1]; break;
					}
				}
				// Инкримент при верной подстановке
				if (Sum == S) {
					Pods++;
				}
			}
		}
	}
	return Pods;
}

void Merge(int *Arr1, int A1Len, int *Arr2, int A2Len, int *C) {

	int *NewArr = new int[A1Len + A2Len];
	int a = 0, b = 0;
	while (a + b < A1Len + A2Len) {
		if ((b >= A2Len) || (a<A1Len && Arr1[a] <= Arr2[b])) {
			NewArr[a + b] = Arr1[a];
			a++;
		}
		else {
			NewArr[a + b] = Arr2[b];
			b++;
		}
	}
	memcpy(C, NewArr, sizeof(int)*(A1Len + A2Len));
}

void MergeSort(int *Arr, int ArrLength) {

	if (ArrLength == 1) return; // Сортировка не нужна

	if (ArrLength == 2) {  // Обмен значениями
		if (Arr[0] > Arr[1]) {
			int t = Arr[0];
			Arr[0] = Arr[1];
			Arr[1] = t;
		}
		return;
	}

	MergeSort(Arr, ArrLength / 2);
	MergeSort(Arr + ArrLength / 2, ArrLength - ArrLength / 2);
	Merge(Arr, ArrLength / 2, Arr + ArrLength / 2, ArrLength - ArrLength / 2, Arr); // Соединим уже отсортированные половины
}

void BubleSort(int* Arr, int ArrLength) {
	bool flag = true;
	int hEl;
	while (flag) {
		flag = false;
		for (int i = 0; i < ArrLength-1; i++)
			if (Arr[i] > Arr[i + 1]) {
				hEl = Arr[i];
				Arr[i] = Arr[i + 1];
				Arr[i + 1] = hEl;
				flag = true; // была перестановка
			}
	}
}

void InsertSort(int *Arr, int ArrLength) {
	for (int i = 0; i < ArrLength; i++) {
		int hEl = Arr[i];
		int j = i;
		while (j > 0 && Arr[j - 1] > hEl) {
			Arr[j] = Arr[j - 1];
				j = j - 1;
		}
		Arr[j] = hEl;
	}
}

double result(double *Arr, int ArrLength, double x) {

	if (ArrLength > 1) {
		return (Arr[ArrLength] + result(Arr, ArrLength - 1, x))*x;
	}
	return Arr[1] * x + Arr[0];
}

void RecShiftR(int *Arr, int ArrLength, int i) {
	if (i < ArrLength-1) {
		if (i == 0) {
			swap(Arr[ArrLength - 1], Arr[0]);
		}
		if ( (i > 0) && (i < ArrLength - 1) ) {
			swap(Arr[ArrLength - 1], Arr[i]);
		}
		RecShiftR(Arr, ArrLength, i + 1);
	}
}

int BinSearch(int *Arr, int s, int r, int l){
	int m = (l + r) / 2;
	if (Arr[m] == s) return m;
	if (m == 0 || m == r) return -1;
	if (Arr[m] < s)
		BinSearch(Arr, s, r, m);
	else
		BinSearch(Arr, s, m, l);
}
