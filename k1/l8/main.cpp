#include <iostream>
#include "func_l8.h"

using namespace std;

int main() {

	cout << "Q0:" << endl;
	char s01[] = "example";
	char s02[] = "anyexample";
	if (checkConpare(s01, s02))
		cout << "Strings are equal" << endl;
	else
		cout << "Strings not equal" << endl;

	cout << "Q1:" << endl;
	char* s11 = "example";
	char symbol = 'a';
	cout << "First position \"" << symbol << "\" in \"" << s11 << "\" : " << firstPostiton(s11, symbol) << endl;

	cout << "Q2:" << endl;
	char s21[] = "example";
	deleteSymbol(s21, 'e');
	cout << "String without symbol " << s21 << endl;

	cout << "Q3:" << endl;
	char s31[30] = "FirstLine";
	char s32[] = "SecondLine";
	concatString(s31, 'e', s32);
	cout << s31 << endl;

	cout << "Q4:" << endl;
	char s41[] = " any example  ";
	cout << countWords(s41) << endl;

	cout << "Q5:" << endl;
	char s61[20] = "one two free four";
	cout << s61 << endl;
	replaceInString(s61, "one", "five");
	cout << s61 << endl;

	  return 0;

}
