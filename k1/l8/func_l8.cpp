#include <string.h>
#include <cstring>
#include <iostream>

using namespace std;

int stringLength(char * String) {
	int i = 0;
	while (String[i] != '\0')
		i++;
	return i;
}

bool checkConpare(char *String1, char *String2) {
	bool flag = false;
	int i = 0;
	if (stringLength(String1) == stringLength(String2))
		while (i < stringLength(String1) && !flag) {
			if (String1[i] == String2[i])
				flag = true;
			else {
				flag = false;
			}
		}
	return flag;
}

int firstPostiton(char *String, char symb) {
	for (int i = 0; String[i] != '\0'; i++)
		if (String[i] == symb)
			return i+1;
	return NULL-1;
}

void* deleteSymbol(char *String1, int symb) {
	int i = 0;
	while (String1[i] != 0) {
		if (String1[i] == symb)
			for (int j = i; String1[j] != 0; j++)
				String1[j] = String1[j + 1];
		else i++;
	}
	return String1;
}

void concatString(char *String1, char ch, char *String2) {

	int NomFirstEn = firstPostiton(String1, ch) - 1;

	int Len1 = stringLength(String1);
	int Len2 = stringLength(String2);

	cout << String1 << endl;

	for (int i = Len1 + Len2; i > NomFirstEn; i--) {
		String1[i] = String1[i - Len2];
	}
	cout << String1 << endl;
	for (int i = NomFirstEn+1, j = 0; i < stringLength(String2), j < stringLength(String2); j++, i++)
		String1[i] = String2[j];

}

int countWords(char *String1) {
	bool flag = false;
	int Sum = 0;
	for (int i = 0; String1[i] != '\0'; i++)
		if (!flag && String1[i] != ' ') {
			Sum++;
			flag = true;
		}
		else if (flag && String1[i] == ' ')
			flag = false;
	return Sum;
}


void replaceInString(char *String1, char *word1, char *word2) {
	char *String = new char[strlen(String1)];
	char *ch = strstr(String1, word1);
	size_t n = strlen(String1) - strlen(ch);
	strncpy(String, String1, n);
	strcpy(String + n, word2);
	strcpy(String + n + strlen(word2), String1 + n + strlen(word1));
	String[strlen(String1) - strlen(word1) + strlen(word2)] = 0;
	strcpy(String1, String);
}
