// Work_with_String.cpp: îïðåäåëÿåò òî÷êó âõîäà äëÿ êîíñîëüíîãî ïðèëîæåíèÿ.
//


/*
1. Â ñòðîêå ðàçâåðíóòü ñëîâà (èíâåðòèðîâàòü), ñîõðàíÿÿ èõ âçàèìíûé ïîðÿäîê
2. Ëþáîé ôàéë âûâîäèòü â 16-ì âèäå. Êðàñèâî!

*/

#include <iostream>
#include <string>

using namespace std;

size_t toDecimal(char ch) {
	if (ch >= '0' && ch <= '9')
		return ch - '0';
	if (ch >= 'a' && ch <= 'z') ch = ch + 'A' - 'a';
	return ch - 'A' + 10;
}

char toDigit(size_t digit) {
	if (digit<10)
		return digit + '0';
	return 'A' + digit - 10;
}

string DecToAny(size_t decN, size_t base) {
	string result;
	while (decN > 0) {
		result = toDigit(decN % base) + result;
		decN /= base;
	}
	return result;
}

size_t AnyToDec(string N, size_t base) {
	size_t result = 0;
	size_t mult = 1;
	for (int i = N.size() - 1; i >= 0; --i) {
		result += mult*toDecimal(N[i]);
		mult *= base;
	}
	return result;
}

string AnyToAny(string N, size_t base1, size_t base2) {
	return DecToAny(AnyToDec(N, base1), base2 );
}

string StringRevert(string N) {
	string result;

	int len=N.length();
	int last = len;
	for (int i=len; i>=0; i--)
		if (N[i]==' ' or i==0 )
		{
			 for (int j=i; j<=last; j++) {
					//  cout << N[j];
					cout << i << ' ' << j << endl;
					 result = result + N[j]  ;
				 }
				// 	cout << ' ';
				result = result + ' ';
			 	last=--i;
		}
		cout << result << endl;
		// for (int i=0; i<=last; i++)
		// result = result + N[i]  ;
		// cout << N[i];

	//
	// cout << l << "\n\n";
	// for (size_t i = len - 1; i >= 0; --i) {
	// 	if (N[i] == ' ') {
	// 		for (size_t j = i + 1; j <= l; j++  ) {
	// 			cout << i << ' ' << j << endl;
	// 			result = result + N[j]  ;
	// 		}
	// 		cout << "_"<< endl;
	// 		result = result + ' ';
	// 		l = --l;
	// 	}
	// 	cout << result << endl;
	// }
	return result;
}

int main()
{
	cout << " 7 - " << toDecimal('7') << endl;
	cout << " D - " << toDecimal('D') << endl;
	cout << " H - " << toDecimal('H') << endl;
	cout << " d - " << toDecimal('d') << endl;
	cout << " h - " << toDecimal('h') << endl;
	cout << "FAA08D - " << AnyToDec("FAA08D", 16) << endl;
	cout << "FAA08D - " << DecToAny(AnyToDec("FAA08D", 16),16 ) << endl;
	cout << "F123 - " << AnyToAny("F123",16,2) << endl;
	cout << "\"abba fu bar\" - " << StringRevert("abba fu bar") << endl;
	return 0;
}
