/* func_l3.h
 * Описание функций к Лабораторной работе №3.
 */
<<<<<<< HEAD
// #pragma once
#include <cassert>
#include <cmath>
#include <iostream>
using namespace std;
=======
#pragma once
#include <cassert>
>>>>>>> b2c4f4b834d6376ffbcd669ac2f72cc025106ea7

/* Глобальная константа : точность вычислений,
 * будет доступна во всех файлах, подключающий данный заголовок.
 */
const double precision = 1E-16;

/* Определение пользовательского типа uint,
 * как неотрицательного целого числа.
 * (разрядность int зависит от разрядности системы)
 */
typedef unsigned int uint;

// void swap_ptr (T *a, T *b)
// Шаблон функции обмена значениями двух переменных по указателю
// Параметры:
//      a, b - указатели на переменные.
// Результат:
//      параметры a и b  меняются значениям.
<<<<<<< HEAD
template <typename T>
void swap_ptr(T *a, T *b)
{
    // assert(a != nullptr);
    // assert(b != nullptr);
    T tmp = *a;
=======
// template <typename T>
void swap_ptr(uint *a, uint *b)
{
    assert(a != nullptr);
    assert(b != nullptr);
    uint tmp = *a;
>>>>>>> b2c4f4b834d6376ffbcd669ac2f72cc025106ea7
    *a = *b;
    *b = tmp;
}

// double exp_of(const double x, const double eps = precision);
// Вычисление значения экспоненты с заданной точностью.
// Параметры:
//      x - значение аргумента функции,
//      eps - точность вычислений (по умолчанию = precision).
// Результат:
//      занчение e(x) с точностью eps.
<<<<<<< HEAD
inline double exp_of(const double x, const double eps = precision)
{
  double s = 1;
  double r = 1;
  for (int i = 1; abs(r) >= eps; i++) {
    r = r*x / i;
    s += r;
  }
  return s;
=======
double exp_of(const double x, const double eps = precision)
{
  double e,s;
  int i = 0;
  s = pow((1+x/i),i);
  while (s > eps) {
    e += s;
    i++;
    s = pow((1+x/i),i);
  }
  return e;
>>>>>>> b2c4f4b834d6376ffbcd669ac2f72cc025106ea7
}

// uint sum_prime_factors(const uint k);
// Вычисление суммы простых делителей целого числа с помощью рекурсии.
// Параметры:
//      k - неотрицательное целое число.
// Результат:
//      сумма простых делителей числа k.
<<<<<<< HEAD
inline uint sum_prime_factors(const uint k) {
	if (k > 1) {
		for (uint i = 2; i <= k; i++) {
			if (k % i == 0) {
				return i + sum_prime_factors(k / i);
			}
		}
	}
	else
		return 0;
}

=======
uint sum_prime_factors(const uint k)
{
  uint s = 0;
  if (k == 1) {return 1;}
  bool f = true;
  for (int i = 2; i <= sqrt(k); ++i) {
    if (k % i == 0) {f = false}
  }
  if (f) {
    s = k + sum_prime_factors(k-1);
  }
  else {
    s = sum_prime_factors(k-1);
  }
  return s;
}
>>>>>>> b2c4f4b834d6376ffbcd669ac2f72cc025106ea7
// s = e + s-1     if k - prime -> s = k+ sum-1 else s = sum-1
// void tab(double(* const f)(double), const double a, const double b, const double h);
// Вывод всех значений функции на отрезке [a;b] с шагом h
// Параметры:
//      f - указатель на целевую функцию,
//      a, b - границы отрезка,
//      h - шаг табуляции (по умолчанию = precision).
// Результат:
//      вывод значений функции в виде: x | f(x)
//      для x = {a, a+h, a+2h, ... b}
//      на стандартный вывод.
void tab( double(* const f)(double), const double a, const double b, const double h = precision)
{
<<<<<<< HEAD
  for (double x = a; x < b; x += h) {
      std::cout << x << "|" << f(x) << std::endl;
=======
  for (double x = a; x < b; a += h) {
      cout << x << "|" << *f(x) << endl;
>>>>>>> b2c4f4b834d6376ffbcd669ac2f72cc025106ea7
  }
}

// double root(double(*const f)(double), const double a, const double b, const double eps)
// Поиск корня функции на отрезке [a;b] с точностью eps методом деления пополам.
// Параметры:
//      f - указатель на целевую функцию,
//      a, b - границы отрезка (f(a)*f(b) < 0),
//      eps - точность поиска (по умолчанию = precision).
// Результат:
//      значение корня функции на отрезке.
<<<<<<< HEAD
inline double root(double(*const f)(double), const double a, const double b, const double eps = precision)
{
	double l = a;
	double r = b;
	double m = l + (r - l) / 2;
	while ( (abs((*f)(r-l)) > eps)&&((*f)(m)>eps) ) {
		if ((*f)(m) == 0) {
			return m;
		}
		if (f(l) * (*f)(m) < 0) {
			r = m;
		}
		else {
			l = m;
		}
		m = l + (r - l) / 2;
	};
  return m;
=======
double root(double(*const f)(double), const double a, const double b, const double eps = precision)
{
  double res = -1;
  for (double x = a; x < b; a += h) {
      if (*f(x) == 0) {res = x}
  }
  return res;
>>>>>>> b2c4f4b834d6376ffbcd669ac2f72cc025106ea7
}
