#include <iostream>
#include "rstring.h"

using namespace std;

int main()
{

	char *res = nullptr;
	cout << endl;

	const int MaxChars = 100;
	char str[MaxChars];
	char sub[MaxChars];
	char rep[MaxChars];
	char symbol;

	cout << "char*" << endl << endl;

	cout << "Q1: substing swap" << endl << endl;
	cout << "Please enter source string:" << endl;
	cin.getline(str, MaxChars);
	cout << "Please enter separator char:" << endl;
	cin >> symbol;
	cin.sync();
	try
	{
		res = swapSubstrings(str, symbol);
		if (res)
			cout << "Result: " << endl << res << endl << endl;
		else
			cout << "Source string haven't char" << endl << endl;

	}
	catch (char *c)
	{
		cout << c << endl << endl;
	}
	delete[] res;

	cout << "Q2: delete in string" << endl << endl;
	cout << "Please enter source string:" << endl;
	cin.getline(str, MaxChars);
	cout << "Please enter pattern for delete:" << endl;
	cin.getline(sub, MaxChars);

	try
	{
		res = deleteSubstrings(str, sub);
		if (res)
			cout << "Result: " << endl << res << endl << endl;
		else
			cout << "Source string can't find" << endl << endl;

	}
	catch (char *c)
	{
		cout << c << endl << endl;
	}
	delete[] res;

	cout << endl << "Strings" << endl << endl;

	string s_str, s_sub, s_rep;

	cout << "Q3:" << endl << endl;
	cout << "Please enter source string:" << endl;
	getline(cin, s_str);
	cout << "Please enter replace string:" << endl;
	getline(cin, s_sub);
	cout << "Please enter new string:" << endl;
	getline(cin, s_rep);
	try
	{
		replace(s_str, s_sub, s_rep);
		cout << endl << "Result: " << endl << s_str << endl << endl;
	}
	catch (char *c)
	{
		cout << c << endl << endl;
	}

	cout << "Q4: delete char in string" << endl << endl;
	cout << "Please enter source string:" << endl;
	getline(cin, s_str);
	cout << "Please enter deleting char:" << endl;
	cin >> symbol;
	cin.sync();


	try
	{
		deleteSymbols(s_str, symbol);
		cout << endl << "Result: " << endl << s_str << endl << endl;
	}
	catch (char *c)
	{
		cout << c << endl << endl;
	}

	cout << "Q5: replace word" << endl << endl;
	cout << "Please enter source string:" << endl;
	getline(cin, s_str);
	cout << "Please enter pattern word:" << endl;
	getline(cin, s_rep);

	try
	{
		replaceWords(s_str, s_rep);
		cout << endl << "Result: " << endl << s_str << endl << endl;
	}
	catch (char *c)
	{
		cout << c << endl << endl;
	}

	cout << "Q6: Word in strig calculate" << endl << endl;
	cout << "Please enter source string:" << endl;
	getline(cin, s_str);
	string * words = nullptr;
	size_t n = 0;
	size_t srclen = s_str.length();
	int * words_num = new int[srclen];

	try
	{
		words = calculateWord(s_str, n);    // strin -> arr of words
		int sum = 1;
		for (size_t i = 0; i < n; i++)
		{
			for (size_t j = i + 1; j < n; j++)
			{
				if (words[i] == words[j])
				{
					for (size_t k = j; k < n - 1; k++)   // Удаление повтора слова
						words[k] = words[k + 1];
					n--;
					sum++;
				}
			}
			words_num[i] = sum;    // Количество слов
			sum = 1;
		}

		cout << endl << "Result: " << endl;
		for (size_t i = 0; i < n; i++)
			cout << words[i] << ": " << words_num[i] << ' ';
		cout << endl;
	}
	catch (char *c)
	{
		cout << c << endl << endl;
	}

	delete[] words_num;
	delete[] words;
	return 0;
}
