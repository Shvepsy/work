#include <cassert>
#include <cstring>
#include <cmath>
#include "rstring.h"

using namespace std;
char * replace(const char *source, const char *from, const char* to)
{
	assert(source);
	assert(from);
	assert(to);

	if (*from == 0)
		throw "Null string.";

	char *result = nullptr;
	size_t lenFrom = strlen(from);
	size_t lenTo = strlen(to);
	const char *next = strstr(source, from);

	if (next == nullptr) return nullptr;

	result = new char[(next - source) + lenTo + 1];
	strncpy(result, source, next - source);
	result[next - source] = 0;
	strcat(result, to);
	source = next + lenFrom;

	while (next = strstr(source, from))
	{
		char *tmp = new char[strlen(result) + (next - source) + lenTo + 1];
		strcpy(tmp, result);
		strcat(strncat(tmp, source, next - source), to);
		source = next + lenFrom;
		delete[] result;
		result = tmp;
	}

	char *tmp = new char[strlen(result) + strlen(source) + 1];
	strcpy(tmp, result);
	strcat(tmp, source);
	delete[]result;
	result = tmp;

	return result;
}

char * swapSubstrings(char *source, const char symbol)
{
	assert(source);

	if (*source == 0)
		throw "Null string";

	char * result = nullptr;
	char * begin = source;
	char * symbol_pos = strchr(source, symbol);
	if (symbol_pos == nullptr)
		return nullptr;
	size_t sublen = symbol_pos - begin;       // Левая части длинна
	result = new char[strlen(source) + 1];
	symbol_pos++;
	result = strcpy(result, symbol_pos);
	result = strncat(result, --symbol_pos, 1);     //left + symbol + right
	result = strncat(result, begin, sublen);
	return result;
}

char * deleteSubstrings(const char * source, char * delete_str)
{
	assert(source);

	if (*delete_str == 0)
		throw "Null string";

	char *result = nullptr;
	size_t lenDel = strlen(delete_str);
	const char *next = strstr(source, delete_str);      // Подстрока для удаления

	if (next == nullptr) return nullptr;

	result = new char[strlen(source) + 1];
	strncpy(result, source, next - source);   // Копирование перед
	result[next - source] = 0;
	source = next + lenDel;

	while (next = strstr(source, delete_str))
	{
		result = strncat(result, source, next - source);
		source = next + lenDel;
	}
	if (source != 0)
		result = strcat(result, source);
	return result;
}

void replace(string& source, const string &from, const string &to)
{
	if (from.length() == 0)
		throw "Null string";

	size_t next = source.find(from, 0);
	while (next != string::npos)
	{
		source.replace(next, from.length(), to);
		next = source.find(from, next + to.length());
	}
}

void deleteSymbols(string &source, char symbol)
{
	if (source.length() == 0)
		throw "Null string";

	size_t next = source.find(symbol);
	while (next != string::npos)            // Первый и остальные символы пока попадают
	{
		deleteSymbol(source, next);
		next = source.find(symbol);
	}
}

void deleteSymbol(string &source, size_t pos)
{
	size_t len = source.length();
	for (size_t i = pos; i < source.length() - 1; i++)
		source[i] = source[i + 1];					// Сдвигаем и удаляем
	source.pop_back();
}

void replaceWords(string &source, string replacement)
{
	if (source.length() == 0)
		throw "Null string";

	size_t replen = replacement.length();
	size_t srclen = source.length();
	size_t count = 0;
	size_t i = 1;
	int diff = 0;      // Разница в длинне слов
	bool even = false;
	while (source[i] != '\0')
	{
		if (source[i] == ' ' && source[i - 1] != ' ') // коенц слова
		{
			if (even == false)     // симафор
				even = true;
			else
			{
				diff = replen - count;
				srclen = srclen + diff;
				source.erase(i - count, count);
				source.insert(i - count, replacement);
				even = false;
				i += diff;
			}
			count = 0;
		}

		if (source[i] != ' ')
			count++;
		i++;
	}
	if (i == srclen && even == true)    // удаление
	{
		source.erase(i - count, count);
		source.insert(i - count, replacement);
	}
}

string * calculateWord(string source, size_t &n)
{
	if (source.length() == 0)
		throw "Null string";

	size_t count = 0;
	size_t i = 1;
	size_t j = 0;
	size_t srclen = source.length();
	string * words = new string[source.length()];
	while (source[i] != '\0')
	{
		if (source[i] == ' ' && source[i - 1] != ' ')
		{
			string temp = source.substr(i - count, count);
			words[j] = temp;                             //Добавление подстроки в массив
			j++;
			n++;
			count = 0;
		}

		if (i == 1 && source[i] != ' ' && source[i - 1] != ' ')  // сдвиг если перед нет пробелов
			count++;
		if (source[i] != ' ')   //Увеличение счетчика букв
			count++;
		i++;
	}

	if (i == srclen && source[i] != ' ')
	{
		string temp = source.substr(i - count, count);
		words[j] = temp; 
		n++;
	}
	return words;
}
