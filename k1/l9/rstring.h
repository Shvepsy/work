#pragma once
#include <string>
using namespace std;

char * replace(const char *source, const char *from, const char* to);
char * swapSubstrings(char *source, const char symbol);
char * deleteSubstrings(const char * source, char * delete_str);
char * invertWords(char * str);
void replace(string& source, const string &from, const string &to);
void deleteSymbols(string &source, char symbol);
void deleteSymbol(string &source, size_t pos);
void replaceWords(string &source, string replacement);
string * calculateWord(string source, size_t &n);
