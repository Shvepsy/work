/* main_l2.cpp
 * Основная программа к Лабораторной работе №2.
 * Вычисляет высоты треугольника по координатам вершин.
*/
#include <iostream>
#include "func_l2.h"
#include "test_l2.h"

using namespace std;

int main()
{
    test_distance();
    // Оставшиеся тесты необходимо реализовать самостоятельно
    // test_full_func_l2();

    system("pause");
    return 0;
}
