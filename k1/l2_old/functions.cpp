#include <iostream>
#include <cmath>

using namespace std;

double sqr ( double x ) {
  return x*x;
}

double distance ( double x1, double y1, double x2, double y2 ) {
  return sqrt(sqr(x1-x2)+sqr(y1-y2));
}

void sort ( double &a, double &b, double &c, bool asc = 0) {
  double t;
  if (asc) {
    if (a > c) {t = a; a = c; c = t; }
    else if (a > b) {t = a; a = b; b = t; }
    else if (b > c) {t = b; b = c; c = t; }
  }
  else {
    if (c > a) {t = a; a = c; c = t; }
    else if (b > a) {t = a; a = b; b = t; }
    else if (c > b) {t = b; b = c; c = t; }
  }
}

enum triangleType {
    tRight = 0,     // прямоугольный
    tAcute,         // остроугольный
    tObtuse         // тупоугольный
};

triangleType checkTriangleType ( double xa, double ya, double xb, double yb, double xc, double yc ) {
  triangleType tt;
  double ab,ac,bc;
  ab = distance (xa,ya,xb,yb);
  ac = distance (xa,ya,xc,yc);
  bc = distance (xb,yb,xc,yc);
  sort (ab,ac,bc,0);
  double sq = sqrt(sqr(ac)+sqr(bc));
  if ( ab == sq ) { tt = tRight; }
  else if ( ab > sq ) { tt = tObtuse; }
  else if ( ab < sq ) { tt = tAcute; }
  return tt;
}

double distanceTo ( double from_x, double from_y, double to_x1, double to_y1, double to_x2, double to_y2 ) {
  double len = (abs(to_y2-to_y1)*from_x - abs(to_x2-to_x1)*from_y + to_x2*to_y1 - to_x1*to_y2 )/(distance(to_x1,to_y1,to_x2,to_y1)) ;
  return len;
}


int main() {
  double x1,y1,x2,y2,x3,y3;
  cout << "Enter (x1) (y1) (x2) (y2) (x3) (y3)" << endl;
  cin >> x1 >> y1 >> x2 >> y2 >> x3 >> y3 ;
  // sort(x1,y1,x2,1);
  // cout << x1 << y1 << x2 << endl;
  triangleType triangle = checkTriangleType(x1,y1,x2,y2,x3,y3);
  if (triangle == tRight ) {cout << "Right traiangle" << endl;}
  else if (triangle == tAcute ) {cout << "Acute traiangle" << endl;}
  else {cout << "Obtuse traiangle" << endl;}
  cout << distanceTo(x1,y1,x2,y2,x3,y3) << endl;
  return 0;
}
