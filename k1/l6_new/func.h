#include <iostream>
#include <stdlib.h>
#include <cstring>
#include <cstdlib>
#include <ctime>

using namespace std;

template <typename T>
struct arr {
  T *data;
  size_t size;
  arr() { data = nullptr; size = 0; };
  arr(const arr <T> & m) {
    data = new T[m.size];
    size = m.size;
    memcpy(data, m.data, sizeof(m.data) * m.size);
  };
  ~arr() { if (size > 0) delete[] data; };
};

template <typename T>
void readArray(arr <T> &  m) {
  if (checkArray(m)) {
    clearArray(m);
  };
  std::cout << "Type arr size: ";
  std::cin >> m.size ;
  // std::cout << "m.size=" << m.size; //
  m.data = new T[m.size];
  std::cout << "Type array elements values: " << std::endl ;
  for ( size_t i = 0; i < m.size; ++i)
    std::cin >> m.data[i];
    // std::cout << i << " elem=" << m.data[i] << "m.size=" << m.size; } //
};

template <typename T>
void printArray(const arr <T> &  m) {
  for ( size_t i = 0; i < m.size; ++i )
    std::cout << m.data[i] << " ";
};

template <typename T>
void initArray(arr <T> &  m) {
  m.data = nullptr;
  m.size = 0;
};

template <typename T>
void clearArray(arr <T> &  m) {
  delete[] m.data;
  m.size = 0;
};

template <typename T>
void createArray(arr <T> &  m, size_t N) {
  if (checkArray(m)) {
    clearArray(m);
    initArray(m);
  }
  m.size = N;
  srand (time(NULL));
  m.data = new T[m.size];
  for ( size_t i = 0; i < m.size; ++i)
    m.data[i] = rand() % 100;
};

template <typename T>
bool checkArray(const arr <T> &  m) {
  if (m.size < 0 or m.data != nullptr) {
    return true;
  }
  else {
    return false;
  };
};

template <typename T>
void sortArray(arr <T> &  m) {
  for ( size_t i = m.size - 1 ; i > 0; i-- ) {
    for ( size_t j = 0; j < i; ++j) {
      if ( m.data[j] > m.data[i] ) {
        T t = m.data[j];
        m.data[j] = m.data[i];
        m.data[i] = t;
      };
    };
  };
};

template <typename T>
void extendArray(arr <T> &  m, size_t N) {
  if (N != m.size) {
    T *newarr = new T[N];
    if (N > m.size) {
      for ( size_t i = 0 ; i < m.size; ++i )
        newarr[i] = m.data[i];
      for ( size_t i = m.size ; i < N; ++i )
        newarr[i] = 0 ;
    } else if (N < m.size) {
      for ( size_t i = 0 ; i < N; ++i )
        newarr[i] = m.data[i];
    };
    delete[] m.data;
    m.size = N;
    m.data = newarr;
    newarr = nullptr;
    m.size = N;
  }
};

template <typename T>
void findZeros(const arr<T> & m, size_t &first, size_t &last) {
	first = -1;
	last = -1;
	for (int i = 0; i < m.size; i++)
		if (m.data[i] == 0) {
			if (first == -1) first = i;
			last = i;
		}

	if (first == -1)
		cout << " Array without 2 zeros" << endl;
	else
		cout << " Zero position " << first << " and " << last << endl;
};

template <typename T>
void duplElems(arr<T> & m) {
	size_t f = 0, l = 0;
	findZeros(m, f, l);
	if (f == l) cout << "Only one zero";
	if (f + 1 == l) return;

	T* b = new T[m.size + l - f ];
  for (size_t i = 0; i <= f; i++)
  b[i] = m.data[i];

  size_t j = f + 1;
  for (size_t i = j; i < l ; i++) {
    b[j++] = m.data[i];
    b[j++] = m.data[i];
  }

  for (size_t i = l; i < m.size; i++, j++)
    b[j] = m.data[i];

  m.size += l - f - 1 ;
  delete[] m.data;
  m.data = b;
}


template <typename T>
void delLessAverage(arr<T> & m) {
	T Average = m.data[0];
	for (int i = 1; i < m.size; i++)
		Average += m.data[i];
	Average /= m.size;
	for (int i = 0; i < m.size; i++) {
		if (m.data[i] <= Average) {
			for (int j = i; j < m.size - 1; j++) {
				m.data[j] = m.data[j + 1];
			}
			m.size = m.size - 1;
			i = i - 1;
		}
	}
}

template <typename T>
void insertDiff(arr<T> & m) {
	size_t N = m.size * 2 - 1;
	T *c = new T[N];
	for (int i = 0; i < m.size - 1; i++) {
		c[2*i] = m.data[i];
		c[2*i + 1] = m.data[i + 1] - m.data[i];
	}
	c[N-1] = m.data[m.size-1];
	m.size = N;
	delete[] m.data;
	m.data = c;
}

template <typename T>
void deleteA2FromA1(arr<T> & m, const arr<T> & b) {
	size_t j = 0;
	bool flag = false;
	for (size_t i = 0; i < m.size;) {
		for (j = 0; j < b.size; j++) {
			if (m.data[i] == b.data[j]) {
				flag = true;
				for (size_t c = i + 1; c < m.size; ++c)
					m.data[c - 1] = m.data[c];
				m.data[m.size-- - 1] = 0;
			}
		}
		if (flag == false)
			i++;
		flag = false;
	}
}
