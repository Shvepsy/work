#include <iostream>
#include <cstdlib>
#include <ctime>
#include "func.h"

using namespace std;

int main() {
	size_t d1,d2;
	arr<double> testarr ;
  initArray(testarr);
  // readArray(testarr);

	cout << "\n[ Init arrays ]\n\n";
  cout << "Created array: ";
  createArray(testarr,6);
  printArray(testarr);

	arr<double> testarr2(testarr);
	cout << endl << "Zeroed array: ";
	testarr2.data[1] = 0;
	testarr2.data[4] = 0;
	printArray(testarr2);

	cout << endl << "\n[ Check zero position ]\n";
	cout << endl << "First array: ";
	findZeros(testarr, d1, d2);
	cout << "Second array: ";
	findZeros(testarr2, d1, d2);

	cout << endl << "\n[ Duplicate from first array ]\n\n";
	cout << "Before: ";
	printArray(testarr2);
	cout << "\nAfter: ";
	duplElems(testarr2);
	printArray(testarr2);

	cout << endl << "\n[ Delete insertion from first array  ]\n\n";
	cout << "From:";
	printArray(testarr2);
	cout << "\nTo: ";
	printArray(testarr);
	cout << "\nAfter: ";
	deleteA2FromA1(testarr,testarr2);
	printArray(testarr);

	cout << endl << "\n[ Delete less average from second array  ]\n\n";
	cout << "Before: ";
	printArray(testarr2);
	cout << "\nAfter: ";
	delLessAverage(testarr2);
	printArray(testarr2);

	cout << endl << "\n[ Insert neighbor diff  ]\n\n";
	cout << "Before: ";
	printArray(testarr2);
	cout << "\nAfter: ";
	insertDiff(testarr2);
	printArray(testarr2);

	cout << endl << "\n[ Sort ]\n\n";
	cout << "Before: ";
	printArray(testarr2);
	cout << "\nAfter: ";
	sortArray(testarr2);
	printArray(testarr2);

	cout << endl << endl;
}
