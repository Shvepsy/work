#include "array.h"
#include "test.h"
#include "func.h"
#include <cassert>
#include <cmath>
#include <iostream>
using namespace std;

int main()
{
    test_full();

    int K = 1;
    int* a = new int[5];
    int n = 5; int m;
    while (K != 0) {
    cout << "Please select required item:" << endl;
    cout << "1. Create array" << endl;
    cout << "2. Printe array" << endl;
    cout << "3. Delete all Before Zero" << endl;
    cout << "4. Delete Zeros Columns " << endl;
    cout << "5. CheckSort " << endl;
    cout << "0. Exit " << endl;
    cin >> K;
    switch (K)
        {
        case 1:
            cout << "Please enter n: ";
            cin >> n; cout << endl;
            createArray(n);
            break;
        case 2:
            printArray(a, n);
            break;
        case 3:
            a = delBeforeZero(a, n);
            break;
        case 4:
            cout << "Please enter m: ";
            cin >> m; cout << endl;
            a = delZeroColumn(a,n, m);
            break;
        case 5:
            cout << checkSort(a, n);
            break;
        default:
            cout << "Try again";
        }
    }
}
