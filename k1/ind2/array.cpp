#include <cassert>
#include <cmath>
#include <iostream>

using namespace std;

int* createArray(int n)
{
    int* a = new int[n];
    for (int j = 0; j < n; j++) {
        cout << "Enter a [" << j << "]";
        cin >> a[j]; cout << endl;
    }
    return a;
}

void copyArray(int *&a, int *b, int n)
{
    for (int j = 0; j < n; j++) a[j] = b[j];
}

bool eqArray(int *a, int *b, int n)
{
    int j = 0;
    while (j < n && a[j] == b[j]) j++;
    return j == n;
}

void printArray(int *a, int n)
{
    for (int j = 0; j < n; j++)
        cout << a[j] << " ";
}
