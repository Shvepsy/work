#pragma once

int* delBeforeZero(int *&a, int &n);
int* delZeroColumn(int *&a, int &n, int m);
bool checkSort(int *a, int n);
int countOthersAndNegative(int *a, int i, int &p);
void test_full();
