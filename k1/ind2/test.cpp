#include <cassert>
#include <cmath>
#include <iostream>
#include "array.h"
#include "func.h"

using namespace std;

void test_delBeforeZero()
{
    int n = 5;

    int* a = new int[5]; a[0] = { 1 }; a[1] = { 4 }; a[2] = { 0 }; a[3] = { 8 }; a[4] = { 9 };
    int b[3] = { 0, 8, 9 };

    a = delBeforeZero(a, n);

    assert(eqArray(a, b, n));

    int* a1 = new int[5]; a1[0] = { 1 }; a1[1] = { 4 }; a1[2] = { 2 }; a1[3] = { 8 }; a1[4] = { 9 };
    int b1[5] = { 1, 4, 2, 8, 9 };

    a1 = delBeforeZero(a1, n);
    printArray(a1, n);
    assert(eqArray(a1, b1, n));

    delete a;  delete a1;

#ifdef _DEBUG
    cerr << "delBeforeZero test: OK" << endl;
#endif /* _DEBUG */

};


void test_delZeroColumn()
{
    int n = 5;

    int* a = new int[5]; a[0] = { 1 }; a[1] = { 4 }; a[2] = { 0 }; a[3] = { 8 }; a[4] = { 9 };
    int b[7] = { 1, 4, 0, 7, 7, 8, 9 };

    a = delZeroColumn(a, n, 7);

    assert(eqArray(a, b, n));
    n = 5;
    int* a1 = new int[5]; a1[0] = { 1 }; a1[1] = { 4 }; a1[2] = { 2 }; a1[3] = { 8 }; a1[4] = { 9 };
    int b1[5] = { 1, 4, 2, 8, 9 };

    a1 = delZeroColumn(a1, n, 7);

    assert(eqArray(a1, b1, n));

    delete a;  delete a1;

#ifdef _DEBUG
    cerr << "delZeroColumn test: OK" << endl;
#endif /* _DEBUG */
};

void test_checkSort()
{
    int n = 5;

    int* a = new int[5]; a[0] = { 1 }; a[1] = { 4 }; a[2] = { 0 }; a[3] = { 8 }; a[4] = { 5 };

    assert(checkSort(a, n));

    int* a1 = new int[5]; a1[0] = { 4 }; a1[1] = { 2 }; a1[2] = { 1 }; a1[3] = { 1 }; a1[4] = { 1 };

    assert(0 == checkSort(a1, n));

    delete a;  delete a1;

#ifdef _DEBUG
    cerr << "checkSort test: OK" << endl;
#endif /* _DEBUG */
};

void test_countOthersAndNegative()
{
    int n = 5; int p = 0;

    int* a = new int[5]; a[0] = { 1 }; a[1] = { -4 }; a[2] = { 0 }; a[3] = { -8 }; a[4] = { 5 };

    assert(2 == countOthersAndNegative(a, n , p) && p == 2);

    delete a;

#ifdef _DEBUG
    cerr << "countOthersAndNegative test: OK" << endl;
#endif /* _DEBUG */
};


void test_full()
{
    test_delBeforeZero();
    test_checkSort();
    test_countOthersAndNegative();
};
