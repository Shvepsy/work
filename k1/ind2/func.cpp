#include <cassert>
#include <cmath>
#include <iostream>

using namespace std;

int* delBeforeZero(int *&a, int &n) // Удаляет до первого нулевого
{
    int new_n = 0;
    while (new_n < n && a[new_n] != 0) new_n++;
    new_n = n - new_n;
    int* b = new int[new_n];
    for (int i = (n - new_n); i < n; i++) b[i - new_n + 1] = a[i];
    delete a;
    n = new_n;
    return b;
}

int* delZeroColumn(int *&a, int &n, int m)
{
    int k = 0;
    while (k < n && a[k] != 0) k++;
    k++;
    if (k == n + 1 ) { cout << "Error (a[i] !=0 for any i)";  return a;}
    int* b = new int[n + 2];
    for (int i = 0; i < n; i++) b[i + (i / k) * 2] = a[i];
    b[k] = m; b[k + 1] = m;
    delete a;
    n += 2;
    return b;
}

bool checkSort(int *a, int n) // Проверка убывания
{
    int k = 1;
    for (int i = 1; i < n; i++) if (a[i - 1] >= a[i]) k++;
    return (k != n);
}

int countOthersAndNegative(int *a, int i, int &p) // Отношение обратынх к пол-м
{
    if (i > 0) {
        i--;
        if (a[i] == 0) return countOthersAndNegative(a, i, p);
        else
            if (a[i] > 0) { p++; return countOthersAndNegative(a, i, p);}
            else return countOthersAndNegative(a, i, p) + 1;
    }
    else return 0;
}
