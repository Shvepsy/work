#pragma once

int* delBeforeZero(int *&a, int &n); // Удаляет до первого нулевого
int* delZeroColumn(int *&a, int &n, int m);
bool checkSort(int *a, int n); // Проверка убывания
int countOthersAndNegative(int *a, int i, int &p); // Отношение обратынх к пол-м
