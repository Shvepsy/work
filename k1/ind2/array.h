#pragma once

const double precision = 1e-12;

int* createArray(int n);
void copyArray(int *&a, int *b, int n);
bool eqArray(int *a, int *b, int n);
void printArray(int *a, int n);
