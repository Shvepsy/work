#include <iostream>
#include <stdlib.h>
#include "r1.h"

using namespace std;

int main() {
  arr<double> testarr ;
  initArray(testarr);
  // readArray(testarr);

  cout << "Created array: ";
  createArray(testarr,12);
  {
    arr<double> testarr2;
    testarr2 = testarr;
    printArray(testarr);
  }

  cout << endl << "Sorted array: ";
  sortArray(testarr);
  printArray(testarr);

  cout << endl << "Truncated array: ";
  extendArray(testarr,6);
  printArray(testarr);

  cout << endl << "Extended array: ";
  extendArray(testarr,9);
  printArray(testarr);

  arr<double> testarr2(testarr);

  cout << endl << "Copied array: ";
  printArray(testarr2);

  cout << endl << "Cleared array: ";
  clearArray(testarr);
  printArray(testarr);
  cout << endl;
};
