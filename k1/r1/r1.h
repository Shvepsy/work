#include <stdlib.h>
#include <cstring>
#include <iostream>

// typedef double valuetype;
template <typename T>
struct arr {
  T *data;
  size_t size;
  arr() { data = nullptr; size = 0; };
  arr(const arr <T> & m) {
    size = m.size;
    if(size>0) {
      data = new T[m.size];
      memcpy(data, m.data, sizeof(m.data) * m.size);
    }
    else data = nullptr;
  };

  const arr<T> & operator=(const arr <T> & m) {
    if(&m == this) return *this;
    if(data!=nullptr) delete[] data;
    if(size>0) {
      data = new T[m.size];
      memcpy(data, m.data, sizeof(m.data) * m.size);
    }
    else data = nullptr;
    return *this;
  }

  T & operator[](size_t index) {
      return data[index];
  }

  ~arr() { if (size > 0) delete[] data; };
};

// template <typename T> void readArray(arr <T> &  m);
// template <typename T> void printArray(const arr <T> &  m);
// template <typename T> void initArray(arr <T> &  m);
// template <typename T> void createArray(arr <T> &  m, size_t N);
// template <typename T> bool checkArray(const arr <T> &  m);
// template <typename T> void clearArray(arr <T> &  m);
// template <typename T> void sortArray(arr <T> &  m);
// template <typename T> void extendArray(arr <T> &  m, size_t N);


// #include <stdlib.h>
// #include "r1.h"

// using namespace std;

template <typename T>
void readArray(arr <T> &  m) {
  if (checkArray(m)) {
    clearArray(m);
  };
  std::cout << "Type arr size: ";
  std::cin >> m.size ;
  // std::cout << "m.size=" << m.size; //
  m.data = new T[m.size];
  std::cout << "Type array elements values: " << std::endl ;
  for ( size_t i = 0; i < m.size; ++i)
    std::cin >> m.data[i];
    // std::cout << i << " elem=" << m.data[i] << "m.size=" << m.size; } //
};

template <typename T>
void printArray(const arr <T> &  m) {
  for ( size_t i = 0; i < m.size; ++i )
    std::cout << m.data[i] << " ";
};

template <typename T>
void initArray(arr <T> &  m) {
  m.data = nullptr;
  m.size = 0;
};

template <typename T>
void clearArray(arr <T> &  m) {
  delete[] m.data;
  m.size = 0;
};

template <typename T>
void createArray(arr <T> &  m, size_t N) {
  if (checkArray(m)) {
    clearArray(m);
    initArray(m);
  }
  m.size = N;
  srand (time(NULL));
  m.data = new T[m.size];
  for ( size_t i = 0; i < m.size; ++i)
    m.data[i] = rand() % 100;
};

template <typename T>
bool checkArray(const arr <T> &  m) {
  if (m.size < 0 or m.data != nullptr) {
    return true;
  }
  else {
    return false;
  };
};

template <typename T>
void sortArray(arr <T> &  m) {
  for ( size_t i = m.size - 1 ; i > 0; i-- ) {
    for ( size_t j = 0; j < i; ++j) {
      if ( m.data[j] > m.data[i] ) {
        T t = m.data[j];
        m.data[j] = m.data[i];
        m.data[i] = t;
      };
    };
  };
};

template <typename T>
void extendArray(arr <T> &  m, size_t N) {
  // int diff = (int *)N - (int *)m.size;
  if (N != m.size) {
    T *newarr = new T[N];
    if (N > m.size) {
      for ( size_t i = 0 ; i < m.size; ++i )
        newarr[i] = m.data[i];
      for ( size_t i = m.size ; i < N; ++i )
        newarr[i] = 0 ;
    } else if (N < m.size) {
      for ( size_t i = 0 ; i < N; ++i )
        newarr[i] = m.data[i];
    };
    delete[] m.data;
    m.size = N;
    m.data = newarr;
    // delete[] newarr;
    newarr = nullptr;
    m.size = N;
  }
};
