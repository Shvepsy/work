#include <stdlib.h>
#include <iostream>
#include "r1.h"

// using namespace std;

template <typename T>
void readArray(arr <T> &  m) {
  if (checkArray(m)) {
    clearArray(m);
  };
  std::cout << "Type arr size: ";
  std::cin >> m.size ;
  // std::cout << "m.size=" << m.size; //
  m.data = new T[m.size];
  std::cout << "Type array elements values: " << std::endl ;
  for ( size_t i = 0; i < m.size; ++i)
    std::cin >> m.data[i];
    // std::cout << i << " elem=" << m.data[i] << "m.size=" << m.size; } //
};

template <typename T>
void printArray(const arr <T> &  m) {
  for ( size_t i = 0; i < m.size; ++i )
    std::cout << m.data[i] << " ";
};

template <typename T>
void initArray(arr <T> &  m) {
  m.data = nullptr;
  m.size = 0;
};

template <typename T>
void clearArray(arr <T> &  m) {
  delete[] m.data;
  m.size = 0;
};

template <typename T>
void createArray(arr <T> &  m, size_t N) {
  if (checkArray(m)) {
    clearArray(m);
    initArray(m);
  }
  m.size = N;
  srand (time(NULL));
  m.data = new T[m.size];
  for ( size_t i = 0; i < m.size; ++i)
    m.data[i] = rand() % 100;
};

template <typename T>
bool checkArray(const arr <T> &  m) {
  if (m.size < 0 or m.data != nullptr) {
    return true;
  }
  else {
    return false;
  };
};

template <typename T>
void sortArray(arr <T> &  m) {
  for ( size_t i = m.size - 1 ; i > 0; i-- ) {
    for ( size_t j = 0; j < i; ++j) {
      if ( m.data[j] > m.data[i] ) {
        T t = m.data[j];
        m.data[j] = m.data[i];
        m.data[i] = t;
      };
    };
  };
};

template <typename T>
void extendArray(arr <T> &  m, size_t N) {
  // int diff = (int *)N - (int *)m.size;
  if (N != m.size) {
    T *newarr = new T[N];
    if (N > m.size) {
      for ( size_t i = 0 ; i < m.size; ++i )
        newarr[i] = m.data[i];
      for ( size_t i = m.size ; i < N; ++i )
        newarr[i] = 0 ;
    } else if (N < m.size) {
      for ( size_t i = 0 ; i < N; ++i )
        newarr[i] = m.data[i];
    };
    delete[] m.data;
    m.size = N;
    m.data = newarr;
    // delete[] newarr;
    newarr = nullptr;
    m.size = N;
  }
};

// int main() {
//   arr<double> testarr ;
//   initArray(testarr);
//   // readArray(testarr);
//
//   std::cout << "Created array: ";
//   createArray(testarr,12);
//   printArray(testarr);
//
//   std::cout << std::endl << "Sorted array: ";
//   sortArray(testarr);
//   printArray(testarr);
//
//   std::cout << std::endl << "Truncated array: ";
//   extendArray(testarr,6);
//   printArray(testarr);
//
//   std::cout << std::endl << "Extended array: ";
//   extendArray(testarr,9);
//   printArray(testarr);
//
//   arr<double> testarr2(testarr);
//
//   std::cout << std::endl << "Copied array: ";
//   printArray(testarr2);
//
//   std::cout << std::endl << "Cleared array: ";
//   clearArray(testarr);
//   printArray(testarr);
//   std::cout << std::endl;
// };

// + array lab
