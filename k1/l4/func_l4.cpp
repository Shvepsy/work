#include <cassert>
#include <cmath>
#include "func_l4.h"


void getArray(double *arr, int n) {

	cout << "Please enter " << n << " elements" << endl;
	for (int i = 0; i < n; i++)
		cin >> arr[i];
}

void printArray(double *arr, int n) {

	for (int i = 0; i < n; i++)
		cout << arr[i] << " ";
	cout << endl;
}

int findMin(double *arr, int n) {
	double min = arr[0];
	int nMin = 0;
	int i = 1;
	bool m = 0;
	while (i < n) {
		if (min > arr[i]) {
			min = arr[i];
			nMin = i;
			m = 1;
		}
		i++;
	}

	if (m)
		return nMin;
	else
		return 0;
}

double beforeAfterRatio(double *arr, int ItNum, int ArrLen) { // Отношение левой-правой части от минимума

	if ((ItNum == 0) || (ItNum == ArrLen - 1)) throw "Before or after haven't element, exiting";

	int i = 0;
	double SumB = 0, SumA = 0;

	while (i < ItNum) {
		SumB = SumB + arr[i];
		i++;
	}

	i = ItNum + 1;

	while (i < ArrLen) {
		SumA = SumA + arr[i];
		i++;
	}

	return SumB / SumA;
}

bool CheckAndDel(double *arr, int &ArrLen) {

	// Проверка групп
	bool prov = 0;
	int i = 1;
	while (i < ArrLen) {
		if ((arr[i] == arr[i - 1])) {
			prov = 1;
		}
		i++;
	}
	if (prov == 0) throw "No duplicates, exiting";

	// Удаление со смещением
	i = 1;
	int lenbuff;
	while (i < ArrLen) {
		if ((arr[i] == arr[i - 1])) {

			lenbuff = ArrLen - 1;

			for (int j = i; j < lenbuff; j++) {
				arr[j] = arr[j + 1];
			}
			ArrLen--;
			i = 0;
		}
		else {
			i++;
		}
	}
	return true;

}

bool CheckAndDupl(double *arr, int &ArrLen) {

	// Дублирование
	if (ArrLen * 2 > N) throw "Array oversize, exiting";

	int NewLen = ArrLen;
	for (int i = 0; i < ArrLen * 2; i += 2) {
		for (int j = NewLen - 1; j > i; j--) {
			arr[j + 1] = arr[j];
		}
		arr[i + 1] = arr[i];
		NewLen++;
	}
	ArrLen = NewLen;
	return 0;
}

bool AbsolDiff(double *arr, int ArrLen, double AbDi) {
	bool Prov = 0;
	int i = 0;
	int j;
	while ((i < ArrLen - 1) && (Prov == 0)) {
		j = i + 1;
		while ((j < ArrLen) && (Prov == 0)) {
			if (abs(arr[i] - arr[j]) == AbDi) {
				Prov = 1;
			}
			else {
				j++;
			}
		}
		i++;
	}
	if (Prov) {
		return 1;
	}
	else {
		return 0;
	}
}

bool symCheck(double *arr, int ArrLen, int nom) {
	if (nom == (ArrLen / 2)) {
		return true;
	}
	else {
		if (arr[nom] == arr[ArrLen - 1 - nom]) {
			symCheck(arr, ArrLen, nom + 1);
		}
		else {
			return false;
		}
	}
}
