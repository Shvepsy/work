#include "func_l4.h"
#include <cassert>

int test_findMin() {
	double a[N] = { 4, 1, 6 }; // min = 1
	assert(findMin(a, 2) == 1);

	double b[N] = { 0, 1, 6 }; // min = 0
	assert(findMin(b, 2) == 0);

	double c[N] = { 4, 6, 2, 5 }; // min = 2
	assert(findMin(c, 3) == 2);

	cout << "Test findMin = OK" << endl;

	return true;
}


int test_beforeAfterRatio() {

	// Минимум - 3 элемент
	// Справа есть числа
	double d[N] = {4, 6, 2, 5}; // Результат 2

	assert(beforeAfterRatio(d, findMin(d, 4), 4) == 2);

	// В случаях, где нет минимума, работает исключение

	cout << "Test beforeAfterRatio = OK" << endl;

	return true;
}

int test_CheckAndDel() {

	// Есть массив с повторяющимися подряд элементами
	int Len = 4;
	double d[N] = {4, 3, 4, 4}; // Результат {4, 3, 4}
	CheckAndDel(d, Len);
	assert((d[0] == 4) && (d[1] == 3) && (d[2] == 4));

	// В случаях, где нет повторяющихся, выбрасывается исключение

	cout << "Test CheckAndDel = OK" << endl;

	return true;
}


bool test_CheckAndDupl() {

	int Len = 3;
	double d[N] = {4, 3, 1}; // Результат {4, 4, 3, 3, 1, 1}

	CheckAndDupl(d, Len);

	assert( (d[0] == 4) && (d[1] == 4) && (d[2] == 3) && (d[3] == 3) && (d[4] == 1) && (d[5] == 1) );

	// В случаях, где длина нового массива больше, чем глобальная константа, выбрасывается исключение

	cout << "Test CheckAndDupl = OK" << endl;

	return true;
}

bool test_AbsolDiff() {

	int Len = 4;

	double d[N] = {4, 3, 3, 4};
	assert(AbsolDiff(d, Len, 2) == 0);
	assert(AbsolDiff(d, Len, 1) == 1);

	double c[N] = {1, 2, 3, 4};
	assert(AbsolDiff(c, Len, 2) == 1);
	assert(AbsolDiff(c, Len, 3) == 1);

	cout << "Test AbsolDiff = OK" << endl;

	return true;
}

bool test_symCheck() {

	int Len = 4;
	double d[N] = { 4, 4, 3, 4 };

	assert(symCheck(d, Len, 0) == 0);

	Len = 5;
	double k[N] = { 12, 4, 1, 4, 12 };

	assert(symCheck(k, Len, 0) == 1);

	cout << "Test symCheck = OK" << endl;

	return true;
}


int test_all() {
	return test_findMin() && test_beforeAfterRatio() && test_CheckAndDel() && test_CheckAndDupl() && test_AbsolDiff() && test_symCheck();
}
