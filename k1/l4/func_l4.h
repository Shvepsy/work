#pragma once

#include <iostream>
using namespace std;

// Максимальный размер статического массива
const int N = 40;

// Ввод массива длинной N
void getArray(double *array, int n);

// Вывод массива длины N
void printArray(double *array, int n);

// Проверка соотношения до и после минимального
double beforeAfterRatio(double *arr, int ItNum, int ArrLen);

// Поиск минимального номера
int findMin(double *array, int n);

// Удаление повторяющихся элементов
bool CheckAndDel(double *arr, int &ArrLen);

// Дублирование всех элементов
bool CheckAndDupl(double *arr, int &ArrLen);

// Абсолютная разность
bool AbsolDiff(double *arr, int ArrLen, double AbDi);

// Проверка массива на симметричность
bool symCheck(double *arr, int ArrLen, int nom);
