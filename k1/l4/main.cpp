#include <iostream>

#include "func_l4.h"
#include "test_l4.h"

using namespace std;

int main() {
	int c;

	do {
		cout << "Please select" << endl;
		cout << "1. Before and after ratio" << endl;
		cout << "2. Delete duplicates" << endl;
		cout << "3. Duplicate all elements" << endl;
		cout << "4. Difference between elements" << endl;
		cout << "5. Symetric check" << endl;
		cout << "6. Test" << endl;
		cout << "0. Exit" << endl;
		cout << endl;
		cin >> c;
		cout << endl;
		switch (c) {
		case 1: {
			int elemCount;
			cout << "Please enter array length:" << endl;
			cin >> elemCount;

			double Ar[N];
			getArray(Ar, elemCount);

			try {
				cout << "Before and after ratio = " << beforeAfterRatio(Ar, findMin(Ar, elemCount), elemCount) << endl << endl;
			}
			catch (char(*c)) {
				cout << c << endl;
			}
		}
		break;

		case 2: {
			int elemCount;
			cout << "Please enter array length:" << endl;
			cin >> elemCount;
			double Ar[N];

			getArray(Ar, elemCount);
			try {
				cout << "Source array: " << endl;
				printArray(Ar, elemCount);
				CheckAndDel(Ar, elemCount);
				cout << "Non-duplicated array: " << endl;
				printArray(Ar, elemCount);
			}
			catch (char(*c)) {
				cout << c << endl;
			}
		}
		break;

		case 3: {
			int elemCount;
			cout << "Please enter array length:" << endl;
			cin >> elemCount;
			double Ar[N];

			getArray(Ar, elemCount);
			try {
				CheckAndDupl(Ar, elemCount);
				printArray(Ar, elemCount);
			}
			catch (char(*c)) {
				cout << c << endl;
			}
		}
		break;

		case 4: {
			int elemCount;
			cout << "Please enter array length:" << endl;
			cin >> elemCount;
			double R;
			cout << "Please enter absolutly differens value R:" << endl;
			cin >> R;
			double Ar[N];

			getArray(Ar, elemCount);
			printArray(Ar, elemCount);
			if (AbsolDiff(Ar, elemCount, R)) {
				cout << "Difference exist" << endl;
			}
			else {
				cout << "Difference absent" << endl;
			}
		}
		break;

		case 5: {
			int elemCount;
			cout << "Please enter array length:" << endl;
			cin >> elemCount;
			double Ar[N];

			getArray(Ar, elemCount);
			printArray(Ar, elemCount);
			if (symCheck(Ar, elemCount, 0)) {
				cout << "Symetric" << endl;
			}
			else {
				cout << "Non-symetric" << endl;
			}
		}
		break;

		case 6: {
			test_all();
			cout << endl;
		}
	}

	} while (c != 0);
	return 0;
}
