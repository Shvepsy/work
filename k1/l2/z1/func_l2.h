/* func_l2.h
 * Описание функций к Лабораторной работе №2.
 */

#pragma once
/* Директива pragma с параметром once гарантирует,
 * что данный загоовочный файл будет включен в компилируемый файл
 * не более одного раза.
 */

 #include <iostream>
 #include <cmath>
 using namespace std;
// Определение макроса SQR
#define SQR(x) (x) * (x)
/* После этой строки любое вхождение SQR(_параметр_)
 * будет заменено на (_парметр_) * (_параметр_).
 * Стоит отметить, что скобки в данном случае обязательны,
 * т.к. без них подстановка не работает для выражений:
 *      ( x + y ) * ( x + y ) != x + y * x + y
 */

// Разопределение макроса SQR
#undef SQR
/* После этой строки любое вхождение SQR()
 * не будет обрабатываться препроцессором.
 *
 * Описывать макросы в заголовочных файлах - плохо,
 * поскольку это вносит изменения в ещё не написанный код.
 *
 * Сегодня макросы вообще считаются вредным инструментом,
 * т.к. приводят к ситуации, когда програмист не видит тот код,
 * который будет скомпилирован.
 */


// double sqr ( double x )
// Вычисляет квадрат числа.
double sqr ( double x ){
  return x*x;
}

/* Вместо макросов в С++ принято использовать встраиваемые функции.
 * При реализации необходимо отметить функцию спецификатором inline.
 */

// double distance ( double x1, double y1, double x2, double y2 )
// Вычисляет эвклидово расстояние между двумя точками на плоскости.
// Параметры:
//      x1, y1, x2, y2 – вещественные координаты точек.
// Возвращаемое значение:
//      расстояние между точками  ( x1; y1 ), ( x2; y2 ).
inline double distance(double x1, double y1, double x2, double y2)
{
	return sqrt(sqr(x1-x2)+sqr(y1-y2));
}

// void sort ( double &a, double &b, double &c, bool asc = 0)
// Вспомогательная функция сортировки трех значений.
// Параметры:
//      a, b, c - значения для сортировки,
//      asc - флаг направления сортировки:
//          false - по убыванию (по умолчанию),
//          true - по возростанию.
// Результат:
//      a, b, c отсортированные в соответствии со значением asc.
inline void sort(double &a, double &b, double &c, bool asc = 0) {
  double t;
  if (asc) {
    if (a > c) {t = a; a = c; c = t; }
    else if (a > b) {t = a; a = b; b = t; }
    else if (b > c) {t = b; b = c; c = t; }
  }
  else {
    if (c > a) {t = a; a = c; c = t; }
    else if (b > a) {t = a; a = b; b = t; }
    else if (c > b) {t = b; b = c; c = t; }
  }
}

// enum triangleType
// Набор целочисленных констант, описывающих тип треугольника.
enum triangleType {
    tRight = 0,     // прямоугольный
    tAcute,         // остроугольный
    tObtuse         // тупоугольный
};

/* Перечисление - удобный способ ограничить набор значений,
 * которые может принимать переменная или возвращать функция.
 */

// triangleType checkTriangleType ( double xa, double ya, double xb, double yb, double xc, double yc )
// Определяет тип треугольника, заданного тремя точками.
// Параметры:
//      xa, ya, xb, yb, xc, yc – вещественные координаты точек
//          A ( xa, ya ), B ( xb, yb ), C ( xc, yc ).
// Возвращаемое значение:
//      tRight - прямоугольный,
//      tAcute - остроугольный,
//      tObtuse - тупоугольный.


inline triangleType checkTriangleType ( double xa, double ya, double xb, double yb, double xc, double yc ) {
  triangleType tt;
  double ab,ac,bc;
  ab = distance (xa,ya,xb,yb);
  ac = distance (xa,ya,xc,yc);
  bc = distance (xb,yb,xc,yc);
  sort (ab,ac,bc,0);
  double sq = sqrt(sqr(ac)+sqr(bc));
  if ( ab == sq ) { tt = tRight; }
  else if ( ab > sq ) { tt = tObtuse; }
  else if ( ab < sq ) { tt = tAcute; }
  return tt;
}


// double square ( double xa, double ya, double xb, double yb, double xc, double yc )
// Вычисляет площадь треугольника, заданного тремя точками.
// Параметры:
//      xa, ya, xb, yb, xc, yc – вещественные координаты точек
//          A ( xa, ya ), B ( xb, yb ), C ( xc, yc ).
// Возвращаемое значение:
//      площадь треугольника ABC.

inline double square(double xa, double ya, double xb, double yb, double xc, double yc) {
	double ab = distance(xa, ya, xb, yb);
	double bc = distance(xb, yb, xc, yc);
	double ac = distance(xa, ya, xc, yc);
	double hp = (ab + bc + ac) / 2;
	double sq = sqrt(hp*(hp - ab)*(hp - bc)*(hp - ac));
	return sq;
}


// double distanceTo ( double from_x, double from_y, double to_x1, double to_y1, double to_x2, double to_y2 )
// Вычисляет расстояние от точки до прямой, заданной двумя точками.
// Параметры:
//      from_x, from_y, to_x1, to_y1, to_x2, to_y2 –  вещественные координаты точек
//          F ( from_x; from_y ), T1 ( to_x1; to_y1 ), T2 ( to_x2; to_y2 ).
// Возвращаемое значение:
//      расстояние от точки F ( from_x; from_y ) до прямой, заданной точками T1 ( to_x1; to_y1 ), T2 ( to_x2; to_y2 ).
inline double distanceTo(double from_x, double from_y, double to_x1, double to_y1, double to_x2, double to_y2) {
	double c = to_y2*(to_x2 + to_x1) + to_x1*(to_y1 - to_y2);
	double a = to_y2 - to_y1;
	double b = to_x1 - to_x2;
	double dis = abs(a*from_x + b*from_y + c) / sqrt(sqr(a) + sqr(b));
	return dis;
}
