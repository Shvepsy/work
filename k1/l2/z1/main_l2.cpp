/* main_l2.cpp
 * Основная программа к Лабораторной работе №2.
 * Вычисляет высоты треугольника по координатам вершин.
*/

#include <iostream>
//#include "func_l2.h"
#include "test_l2.h"

using namespace std;

int main()
{
  test_distance();
	test_sort();
	test_checkTriangleType();
	test_square();
	test_distanceTo();
	cout << endl;
	test_full_func_l2();
  return 0;
}
