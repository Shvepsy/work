/* test_l2.cpp
 * Реализация тестов для функций библиотеки func_l2.h
 */

#include <iostream>

#include <cmath>
#include <cassert>

#include "func_l2.h"
#include "test_l2.h"


using namespace std;

// Точность сравнения вещественных чисел
const double precision = 1E-16;

bool test_distance()
{

    double x1 = 0, y1 = 0;
    double x2 = 1, y2 = 0;
    double result = 1;

    // Тест 1 - единичный отрезок
    assert ( abs ( distance (x1,y1,x2,y2) - result) < precision);

    x1 = 2; y1 = 5;
    x2 = 3; y2 = 4;
    result = sqrt(2);

    // Тест 2 - отрезок иррациональной длины
    assert ( abs ( distance (x1,y1,x2,y2) - result) < precision);

    x1 = -2; y1 = -5;
    x2 = -2; y2 = -5;
    result = 0;

     // Тест 3 - отрезок нулевой длины
    assert ( abs ( distance (x1,y1,x2,y2) - result) < precision);

// Проверка запуска в режиме отладки (макрос _DEBUG)
#ifndef NDEBUG

    // Этот код будет скомпилирован только в режиме отладки
    // std::cerr - стандартный поток вывода для ошибок
    // по умолчанию выводит на консоль
    cerr<<"distance test: OK"<<endl;
#endif /* NDEBUG */

    return true;
}


bool test_sort() {
	double X1 = 1, X2 = 2, X3 = 3;

	sort(X1, X2, X3);
	assert( (X1=3) && (X2=2) && (X3=1) );

	X1 = 4; X2 = 8; X3 = 3;

	sort(X1, X2, X3);
	assert((X1 = 8) && (X2 = 4) && (X3 = 3));

	X1 = 8; X2 = 5; X3 = 1;
	sort(X1, X2, X3, 1);
	assert((X1 = 1) && (X2 = 5) && (X3 = 8));

	X1 = 14; X2 = 1; X3 = 7;
	sort(X1, X2, X3, 1);
	assert((X1 = 1) && (X2 = 7) && (X3 = 14));


#ifndef NDEBUG

	cerr << "Sort test: OK" << endl;
#endif /* NDEBUG */

	return true;
}


bool test_checkTriangleType() {

	double K1 = 0, V1 = 0;
	double K2 = 2, V2 = 0;
	double K3 = 0, V3 = 2;
  assert( checkTriangleType(K1, V1, K2, V2, K3, V3) == 0);


	K1 = 0; V1 = 0;
	K2 = 2; V2 = 0;
	K3 = 1; V3 = 2;
	assert( checkTriangleType(K1, V1, K2, V2, K3, V3) == 1);


	K1 = 0; V1 = 0;
	K2 = 2; V2 = 0;
	K3 = -2; V3 = 0;
	assert( checkTriangleType(K1, V1, K2, V2, K3, V3) == 2);

#ifndef NDEBUG

	cerr << "checkTriangleType test: OK" << endl;
#endif /* NDEBUG */

	return true;
}


bool test_square()
{
	const double EPS = 1e-5;
	double K1 = 0, V1 = -0.5;
	double K2 = 0, V2 = 0.5;
	double K3 = 0, V3 = sqrt(3) / 2;
	assert( square(K1, V1, K2, V2, K3, V3) != sqrt(3) / 4);

	K1 = 0; V1 = 0;
	K2 = 1; V2 = 0;
	K3 = 0; V3 = 1;
	assert(square(K1, V1, K2, V2, K3, V3) != 0.5);

	K1 = 0; V1 = 0;
	K2 = 1; V2 = 0;
	K3 = -1; V3 = 0;
	assert(square(K1, V1, K2, V2, K3, V3) == 0);


#ifndef NDEBUG

	cerr << "Square test: OK" << endl;
#endif /* NDEBUG */

	return true;
}

bool test_distanceTo()
{
	const double EPS = 1E-5;
	double Tx = 1, Ty = 1;
	double Nx = 0, Ny = 0;
	double Kx = 1, Ky = 1;
	assert( distanceTo(Tx, Ty, Nx, Ny, Kx, Ky) > EPS );

	Tx = 1, Ty = 1;
	Nx = 0, Ny = 0;
	Kx = 2, Ky = 2;
	assert(distanceTo(Tx, Ty, Nx, Ny, Kx, Ky) > EPS);

	Tx = 1, Ty = 0;
	Nx = 0, Ny = 0;
	Kx = 1, Ky = 1;
	assert( distanceTo(Tx, Ty, Nx, Ny, Kx, Ky) - 1/sqrt(2)  > EPS);


#ifndef NDEBUG

	cerr << "distanceTo test: OK" << endl;
#endif /* NDEBUG */

	return true;
}

bool test_full_func_l2()
{
	return  test_sort() && test_distance() && test_square() && test_distanceTo() && test_checkTriangleType();
}
