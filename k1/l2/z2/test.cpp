#include "func_h.h"
#include <cmath>
#include <cassert>
#include <iostream>
using namespace std;

bool test_gcd()
{
	assert(gcd(5, 5) == 5);

	assert(gcd(17, 8) == 1);

	cout << "gcd test = OK" << endl;
    return true;
}

bool test_prime()
{
	assert(prime(2) == 1);

	assert(prime(4) == 0);

	assert(prime(18) == 0);

	cout << "Primeness test = OK" << endl;
	return true;
}

bool test_byte()
{
	assert(byte(15, 3) == 1);
	assert(byte(16, 2) == 0);

	cout << "Byte test = OK" << endl;
	return true;
}

bool test_all()
{
    return test_byte() && test_gcd() && test_prime();
}
