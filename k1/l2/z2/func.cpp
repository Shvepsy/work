#include <cmath>
#include "math.h"

using  namespace std;

int gcd(int a, int b)
{
	int q, r;
	q = a / b;
	r = a - b * q;
	while (r != 0) {
		a = b;
		b = r;
		q = a / b;
		r = a - b * q;
	};
	return b;
}
bool prime( int a)
{
    int i;
    if (a == 2)
        return 1;
    if (a == 0 || a == 1 || a % 2 == 0)
        return 0;
    for (i = 3; (i*i <= a) && (a % i != 0); i += 2);
    return i*i > a;
}

int byte(int a, int n)
{
	return (a >> (n - 1)) & 1;
}

