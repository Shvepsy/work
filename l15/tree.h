#pragma once
#include "node.h"
#include <queue>
#include <iostream>

template<typename T> class tree {
private:
	void push(node<T> *nd, T val);
	node<T>* find(node<T> *nd, T val);
	void count(node<T> *nd, int &c);
	void push(node<T> *source);
	node<T>* find_empty_place(node<T>* nd);
protected:
	node<T>* _root;
	void remove_tree(node<T> *nd);
public:
	tree();
	tree(tree<T> &tr);
	virtual void push(T val);
	virtual void remove(T val);
	virtual node<T>* find(T val);
	int count();
	int height();
	int width();
	void dfs(node<T> *nd, int nodeHeight, int *numberVertexesOnLevels);
	void print(size_t space);
	void print_tree(node<T> *Ptr, size_t space);
	~tree();
	virtual void out_tree(node<T> *nd, std::ostream& os);
	virtual void operator+=(T val);
	virtual void operator-=(T val);
	virtual bool operator>(tree<T> &val);
	virtual bool operator<(tree<T> &val);
	virtual tree<T>& operator=(tree<T> &val);

	friend std::ostream& operator<<(std::ostream& os, tree<T>& tr) {
		os << endl;
		if (tr._root == nullptr)
			os << "tree is empty";
		else
			tr.print(0);
		os << std::endl;
		return os;
	}
};

template<typename T> tree<T>::tree() {
	_root = nullptr;
}

template<typename T> tree<T>::tree(tree<T> &tr) {
	_root = nullptr;
	push(tr._root);
}

template<typename T> void tree<T>::push(T val) {
	if (_root == nullptr)
		_root = new node<T>(val);
	else
		push(_root, val);
}

template<typename T> void tree<T>::push(node<T> *nd, T val) {

	node<T>* nd_with_empt = find_empty_place(nd);
	if (nd_with_empt->_left == nullptr) {
		nd_with_empt->_left = new node<T>(val);
		nd_with_empt->_left->_parent = nd_with_empt;
	}
	else if (nd_with_empt->_right == nullptr) {
		nd_with_empt->_right = new node<T>(val);
		nd_with_empt->_right->_parent = nd_with_empt;
	}
}

template<typename T> void tree<T>::remove(T val) {
	queue<node<T>*> looking_nodes;
	looking_nodes.push(_root);
	node<T>* curr_nd = nullptr;
	bool start_moving = false;
	do {
		curr_nd = looking_nodes.front();
		looking_nodes.pop();
		if (curr_nd->_left != nullptr)
			looking_nodes.push(curr_nd->_left);
		if (curr_nd->_right != nullptr)
			looking_nodes.push(curr_nd->_right);
		if (!start_moving && curr_nd->_value == val)
			start_moving = true;

		if (start_moving && !looking_nodes.empty())
			curr_nd->_value = looking_nodes.front()->_value;

	} while (!looking_nodes.empty());

	if (curr_nd == curr_nd->_parent->_left) {
		node<T>* par = curr_nd->_parent;
		delete par->_left;
		par->_left = nullptr;
	}
	else if (curr_nd == curr_nd->_parent->_right) {
		node<T>* par = curr_nd->_parent;
		delete par->_right;
		par->_right = nullptr;
	}
}

template<typename T> void tree<T>::remove_tree(node<T> *nd) {
	if (nd != nullptr) {
		if (nd->_left != nullptr) {
			remove_tree(nd->_left);
			delete nd->_left;
			nd->_left = nullptr;
		}

		if (nd->_right != nullptr) {
			remove_tree(nd->_right);
			delete nd->_right;
			nd->_right = nullptr;
		}
	}
}

template<typename T> node<T>* tree<T>::find(T val) {
	if (_root == nullptr)
		return nullptr;
	return find(_root, val);
}

template<typename T> node<T>* tree<T>::find(node<T> *nd, T val) {
	if (val == nd->_value)
		return nd;

	queue<node<T>*> looking_nodes;
	looking_nodes.push(nd);
	node<T>* curr_nd = nullptr;
	do {

		node<T>* curr_nd = looking_nodes.front();
		looking_nodes.pop();
		if (curr_nd->_value == val)
			return curr_nd;

		if (curr_nd->_left != nullptr)
			looking_nodes.push(curr_nd->_left);
		if (curr_nd->_right != nullptr)
			looking_nodes.push(curr_nd->_right);

	} while (!looking_nodes.empty());
	return nullptr;
}

template<typename T> node<T>* tree<T>::find_empty_place(node<T>* nd) {
	queue<node<T>*> looking_nodes;
	looking_nodes.push(nd);
	node<T>* curr_nd = nullptr;
	do {
		node<T>* curr_nd = looking_nodes.front();
		looking_nodes.pop();
		if (curr_nd->_left == nullptr || curr_nd->_right == nullptr)
			return curr_nd;
		looking_nodes.push(curr_nd->_left);
		looking_nodes.push(curr_nd->_right);
	} while (!looking_nodes.empty());
	return curr_nd;
}

template<typename T> int tree<T>::height() {
	node<T>* curr = _root;
	int result = 1;
	while (curr->_left != nullptr) {
		result++;
		curr = curr->_left;
	}
	return result;
}

template<typename T>  void tree<T>::dfs(node<T> *nd, int nodeHeight, int *numberVertexesOnLevels) {
	numberVertexesOnLevels[nodeHeight] += 1;
	if (nd->_left != NULL)
		dfs(nd->_left, nodeHeight + 1, numberVertexesOnLevels);
	if (nd->_right != NULL)
		dfs(nd->_right, nodeHeight + 1, numberVertexesOnLevels);
}

template<typename T> void tree<T>::print(size_t space)
{
	print_tree(_root, space);
}

template<typename T> void tree<T>::print_tree(node<T> *root, size_t space)
{
	if (root == nullptr) return;
	print_tree(root->_right, space + 1);
	for (int i = 0; i < space; i++) cout << '\t';
	cout << root->_value << endl;
	print_tree(root->_left, space + 1);
}

template<typename T> int tree<T>::width() {
	int heightr = height();
	int *numberVertexesOnLevels = new int[heightr];
	for (int i = 0; i < heightr; i++)
		numberVertexesOnLevels[i] = 0;
	dfs(_root, 0, numberVertexesOnLevels);
	int max = numberVertexesOnLevels[0];
	for (int i = 1; i < heightr; i++) {
		if (numberVertexesOnLevels[i] > max)
			max = numberVertexesOnLevels[i];
	}
	return max;
}

template<typename T> void tree<T>::out_tree(node<T> *nd, std::ostream& os) {

	queue<node<T>*> looking_nodes;
	looking_nodes.push(nd);
	node<T>* curr_nd = nullptr;
	do {
		node<T>* curr_nd = looking_nodes.front();
		looking_nodes.pop();
		if (curr_nd != nullptr)
			os << curr_nd->_value << " ";

		if (curr_nd->_left != nullptr)
			looking_nodes.push(curr_nd->_left);

		if (curr_nd->_right != nullptr)
			looking_nodes.push(curr_nd->_right);

	} while (!looking_nodes.empty());
}

template<typename T> tree<T>::~tree() {
	if (_root != nullptr)
		remove_tree(_root);
	delete _root;
}

template<typename T> int tree<T>::count() {
	int c = 0;
	count(_root, c);
	return c;
}

template<typename T> void tree<T>::count(node<T> *nd, int &c) {
	if (nd != nullptr) {
		c++;
		count(nd->_left, c);
		count(nd->_right, c);
	}
}

template<typename T> void tree<T>::operator+=(T val) {
	this->push(val);
}

template<typename T> void tree<T>::operator-=(T val) {
	this->remove(val);
}

template<typename T> bool tree<T>::operator>(tree<T> &val) {
	return this->count() > val.count();
}

template<typename T> bool tree<T>::operator<(tree<T> &val) {
	return this->count() < val.count();
}

template<typename T> tree<T>& tree<T>::operator=(tree<T> &val) {
	this->remove_tree(_root);
	push(_root);
	return *this;
}

template<typename T> void tree<T>::push(node<T> *source) {
	if (source != nullptr) {
		queue<node<T>*> looking_nodes;
		looking_nodes.push(source);
		node<T>* curr_nd = nullptr;
		do {
			node<T>* curr_nd = looking_nodes.front();
			looking_nodes.pop();

			if (curr_nd != nullptr)
				push(curr_nd->_value);
			if (curr_nd->_left != nullptr)
				looking_nodes.push(curr_nd->_left);
			if (curr_nd->_right != nullptr)
				looking_nodes.push(curr_nd->_right);
		} while (!looking_nodes.empty());
	}
}
