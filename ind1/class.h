#include <iostream>
#include <string>

using namespace std;

const int N = 10000;

class product {
 private:
  std::string name_;
  double price_;
  int qt_;

 public:
  product() : name_(), price_(), qt_(){};
  product(std::string name, double price, int quantity)
      : name_(name), price_(price), qt_(quantity){};
  product(const product& obj) {
    name_ = obj.name_;
    price_ = obj.price_;
    qt_ = obj.qt_;
  };
  ~product(){};

  void set(std::string name, double price, int quantity);
  void set_name(std::string name);
  void set_price(double price);
  void set_qt(int n);

  std::string name();
  double price();
  int qt();

  void operator=(const product& b);
  bool operator!=(const product& b);
  friend ostream& operator<<(ostream& os, const product& b);
  friend istream& operator>>(istream& in, product& b);

  void print();
};

class storage {
 private:
  int n_;
  product a_[N];
  std::string name_;

 public:
  storage() {}

  storage(std::string name, product a[], int n) : name_(name), n_(n) {
    for (int i = 0; i < n_; i++) a_[i] = a[i];
  }
  ~storage() {}

  product get_product_number(int n);
  int get_number();

  void print();
  void set_number(int n);
  void set_product_number(int n, product& b);
  void set_product_arr(int n, product* a);
  void set_product_name_number(int n, product& b);
  void set_product_price_number(int n, product& b);
  void set_product_qt_number(int n, product& b);

  std::string find_name_max();
  bool is_there(std::string name);

  void operator=(const storage& b);
  bool operator==(const storage& b);
  friend ostream& operator<<(ostream& os, const storage& b);
  friend istream& operator>>(istream& in, storage& b);
};
