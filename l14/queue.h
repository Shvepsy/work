#pragma once
#include <iostream>
#include <stdexcept>

template <typename Datatype>
class abstract_queue {
public:
	virtual ~abstract_queue() {};
	virtual bool is_empty() const = 0;
	virtual void push(const Datatype &x) = 0;
	virtual void pop() = 0;
	virtual Datatype top() = 0;
};

template <typename Datatype>
class queue: public abstract_queue <Datatype>
{
private:
	struct node {
		Datatype data;
		node *prev, *next;
	};
	node *first, *last;

	void copy_queue(const node *from_first, const node *from_last);
	void delete_queue();

public :
	queue() : first(nullptr), last(nullptr) {}
	queue(const queue &l);
	~queue();

	bool is_empty() const
	{
		return first == nullptr && last == nullptr;
	}

	void push(const Datatype &x);
	void pop();
	Datatype top() ;

	queue &operator=(const queue &l);
	friend std::ostream &operator<<(std::ostream &os, queue &s)
	{
		if (s.is_empty())
			throw std::out_of_range("������� ������� ������� �� ������ �������");
		while (s.begin != nullptr)
		{
			os << s.top() << ' ';
			s.pop();
		}
		return os;
	}
	friend class test_queue;
};

template <typename Datatype>
queue <Datatype>:: queue(const queue &l)
{
	copy_queue(l.first, l.last);
}

template <typename Datatype>
void queue <Datatype>::copy_queue(const node *from_first, const node *from_last)
{
	first = nullptr;

	last = nullptr;
	node **to = &first;
	const node *from = from_first;
	while (from != from_last->next) {
		node *prev = *to;
		*to = new node;
		(*to)->prev = prev;
		(*to)->data = from->data;
		to = &(*to)->next;
		from = from->next;
	}
	*to = nullptr;
	last = *to;
}

template <typename Datatype>
queue <Datatype> & queue <Datatype>::operator=(const queue &l)
{
	if (this == &l) {
		return *this;
	}
	delete_queue();
	copy_queue(l.first, l.last);
	return *this;
}

template <typename Datatype>
void queue <Datatype>::delete_queue()
{
	if (first != nullptr)
	{
		while (first != last) {
			node *t = first;
			first = first->next;
			delete t;
		}
		delete last;
		first = nullptr;
		last = nullptr;
	}
}

template <typename Datatype>
queue <Datatype>::~queue()
{
	delete_queue ();
}

template <typename Datatype>
void queue <Datatype>::push(const Datatype &x)
{
	if (last == nullptr) {
		last = new node;
		last->prev = nullptr;
		first = last;
	}
	else {
		last->next = new node;
		last->next->prev = last;
		last = last->next;
	}
	last->data = x;
	last->next = nullptr;
}

template <typename Datatype>
void queue <Datatype>::pop()
{
	if (first == nullptr)
		throw std::out_of_range("������� ������� � �������� ������ �������");
	node *t = first;
	if (first->next == nullptr)
	{
		delete[] first;
		last = first = nullptr;
	}
	else
	{

		first = first->next;
		t->next->prev = nullptr;
		delete[] t;
	}
}

template <typename Datatype>
Datatype queue <Datatype>::top()
{
	if (first == nullptr)
		throw std::out_of_range("������� ������� � �������� ������ �������");
	return first->data;
}

struct shop
{
	double summ_time;
	size_t count;
};
