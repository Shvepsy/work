//
// Динамические структуры данных. Списки
// main.cpp
//

// g++ -std=c++14  tlist_rec.cpp main.cpp && ./a.out
// g++ -std=c++14 test_tlist.cpp tlist_rec.cpp tlist.cpp main.cpp && ./a.out

#include <iostream>
// #include "tlist_rec.h"
#include "tlist.h"
#include "test_tlist.h"

using namespace std;



int main()
{
   setlocale(LC_ALL, "Russian");
    test_tlist_full();

    size_t n;
    cout << "Введите количество элементов списка: ";
    cin >> n;
    cout << "Введите элементы списка: " << endl;
    tlist * list = get_list(n);

    int cnt = nullCount(n,list);
    cout << "Количество нулевых элементов: " << cnt << endl;

		tlist::datatype s;
		cout << "Какое значение вставить в середину?: ";
		cin >> s;
		middleInsert(n,s,list);
		n++;
		print_list(list);

		auto el = s = 0;
		cout << "После какого значения удалять?: ";
		cin >> s;
		cout << "Сколько элементов удалять?: ";
		cin >> el;
		int res = deleteAfter(s,el,list);
		if (res < 0) {
				cout << "Элемент не найден." << endl;
		} else {
			cout << "Удалено." << endl;
			n =- el;
		}
		print_list(list);
    delete_list(list);

		tlist * listf = get_list("testfile");
		// tlist * list = get_list(5);
		print_list(listf);
		// print_list(list);

		tlist * o = nullptr;
		tlist * e = nullptr;
		// cout << "There" << endl;
		listSplit(listf,o,e);
		// print_list(list);
		print_list(o);
		print_list(e);

    return 0;
}
