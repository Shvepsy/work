//
//
//

//
#pragma once
#include <sys/types.h>

// ���� ������:
//     tlist::datatype - ��� ������ ������;
//     data - �������� � ���� ������;
//     next - ��������� �� ��������� �������.
struct tlist
{
    typedef int datatype;
    datatype data;
    tlist *next;
};

tlist *get_list(const size_t length);

tlist *get_list(const char *filename);

void delete_list(tlist *&begin);

void print_list(const tlist *begin);

tlist *find(const tlist *begin, tlist::datatype x);

int nullCount(int num, tlist * &l);
void middleInsert(int num, tlist::datatype e, tlist * &l) ;
int deleteAfter(tlist::datatype symb, int count, tlist * &l) ;
