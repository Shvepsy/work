//
//
//

#define ARRAY_MAXSIZE 20

#include <cassert>
#include <iostream>
#include <fstream>
#include <sys/types.h>
#include "tlist_rec.h"

using namespace std;

tlist *get_list(size_t length)
{
    if (length == 0)
        return nullptr;
    tlist *begin = nullptr;

    tlist::datatype data;
    if (cin >> data) {  // ���� ������ ��� ������

        begin = new tlist;
        begin->data = data;
        begin->next = get_list(length - 1);
    }

    return begin;
}

tlist *get_list(ifstream &fin)
{
    if (!fin.good())
        return nullptr;
    tlist *begin = nullptr;

    tlist::datatype data;
    if (fin >> data) {  // ���� ������ ��� ������

        begin = new tlist;
        begin->data = data;
        begin->next = get_list(fin);
    }

    return begin;
}

tlist *get_list(const char *filename)
{
    assert(filename != nullptr);
    tlist *begin = nullptr;

    ifstream fin(filename);
    if (!fin.is_open())
        throw "���������� ������� ����";

    begin = get_list(fin);

    fin.close();
    return begin;
}

void delete_list(tlist *&begin)
{
    if (begin == nullptr)
        return;
    delete_list(begin->next);
    delete begin;
    begin = nullptr;
}

void print_list(const tlist *begin)
{
    if (begin == nullptr)
        return;
    cout << begin->data << ' ';
    print_list(begin->next);
}

tlist *find(const tlist *begin, tlist::datatype x)
{
    if (begin == nullptr)
        return nullptr;
    if (begin->data == x)
        return const_cast<tlist *>(begin);
    return find(begin->next, x);

}


int nullCount(int num, tlist * &l) {
	tlist *p = find(l, 0);
  if (p == nullptr) {
    return 0;
  }
  // std::cout << "There..";
  p = p->next;
  return 1 + nullCount(num, p);
}

void middleInsert(int num, tlist::datatype e, tlist * &l) {
    if (num > 2) {
      middleInsert(num-2,e,l->next);
    }
    else {
      tlist * mid = new tlist;
      mid->next = l->next;
      mid->data = e;
      l->next = mid;
    }
}

int deleteAfter(tlist::datatype symb, int count, tlist * &l) {
	tlist * pos = find(l,symb);
	if (pos == nullptr) {
		return 0;
	}
  tlist * next = pos->next;
  std::cout << "There";
  if (!(next == nullptr) and count > 0) {
    pos->next = next->next;
    deleteAfter(next->data,count-1,pos);
	}
}
