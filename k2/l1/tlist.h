//
// ������������ ��������� ������. ������
// tlist.h
//
#pragma once
#include <sys/types.h>

// ���� ������:
//     tlist::datatype - ��� ������ ������;
//     data - �������� � ���� ������;
//     next - ��������� �� ��������� �������.
struct tlist
{
    typedef int datatype;
    datatype data;
    tlist *next;
};

// ������� �������� ������.
// ������ �������� � �������.
// ������� ���������:
//     length - ���������� ��������� ������.
// ���������� ��������� �� ������ ������� ������.
tlist *get_list(const size_t length);

// ������� �������� ������.
// ������ �������� �� �����.
// ������� ���������:
//     filename - ��� ����� ������.
// ���������� ��������� �� ������ ������� ������.
// � ������, ���� �� ������ �������� ������ � �����
// ����������� ���������� char*.
tlist *get_list(const char *filename);

// ������� �������� ������.
// ������� ���������:
//     begin - ��������� �� ������ ������.
// �������� �������� ��������� begin ����� nullptr.
void delete_list(tlist *&begin);

// ������� ������ ������ �� �������.
// ������� ���������:
//     begin - ��������� �� ������ ������.
void print_list(const tlist *begin);

// ������� ������ ��������.
// ������� ���������:
//     begin - ��������� �� ������ ������;
//     x - ������� ��������.
// ���������� ��������� �� ������ ������� � �������� ��������� ���
// nullptr, ���� �������� � ����� �������� � ������ ���.
tlist *find(const tlist *begin, const tlist::datatype x);

int nullCount(int num, tlist * &l);
void middleInsert(int num, tlist::datatype e, tlist * &l);
int deleteAfter(tlist::datatype symb, int count, tlist * &l);
void listSplit(tlist * &s,tlist * &odd,tlist * &even);
