//
// ������������ ��������� ������. ������
// tlist.cpp
//
#define ARRAY_MAXSIZE 20

#include <cassert>
#include <iostream>
#include <fstream>
#include <sys/types.h>
#include "tlist.h"

using namespace std;

tlist *get_list(size_t length)
{
    tlist *begin = nullptr;
    tlist *end = nullptr;

    while (length > 0) {
        tlist::datatype data;
        if (cin >> data) {  // ���� ������ ��� ������

            tlist *node = new tlist;
            node->data = data;
            node->next = nullptr;

            if (begin == nullptr)
                begin = node;

            if (end != nullptr)
                end->next = node;
            end = node;
        }
        length--;
    }

    return begin;
}

tlist *get_list(const char *filename)
{
    assert(filename != nullptr);
    tlist *begin = nullptr;
    tlist *end = nullptr;
    // tlist *prev = nullptr;

    ifstream fin(filename);
    if (!fin.is_open())
        throw "���������� ������� ����";

    tlist::datatype data;
    tlist::datatype arr[ARRAY_MAXSIZE];
    int i = 0;
    while (fin >> data) {
      arr[i++] = data;
    }
    // for (int p = 0; p < i; p++) {
    //   std::cout << arr[p] << endl;
    // }
    // cout << "==============" << endl;

    tlist::datatype tmp;
    for (int j = 0; j < i; j++) {
      for (int k = j + 1; k < i; k++) {
        if (arr[j] > arr[k]) {
          tmp = arr[j];
          arr[j] = arr[k];
          arr[k] = tmp;
        }
      }
    }

    // for (int p = 0; p < i; p++) {
    //   std::cout << arr[p] << endl;
    // }

    // fin.seekg(0);
    // while (fin >> data) {
    for (int p = 0; p < i; p++) {
      // std::cout << "there";
        tlist *node = new tlist;
        node->data = arr[p];
        node->next = nullptr;

        if (begin == nullptr)
            begin = node;

        if (end != nullptr)
            end->next = node;
        end = node;
    }

    fin.close();
    return begin;
}

void delete_list(tlist *&begin)
{
    while (begin != nullptr) {
        tlist *t = begin;
        begin = begin->next;
        delete t;
    }
}

void print_list(const tlist *begin)
{
    // std::cout << "there";
    while (begin != nullptr) {
        std::cout << begin->data << ' ';
        begin = begin->next;
    }
    cout << endl;
}

// �������� begin ����������, ����� �������� ��� ������ �� ��������
tlist *find(const tlist *begin, tlist::datatype x)
{
    // ������� ������������� ���������
    // �.�. �� �����, ��� ���������� ������ �� ����������� tlist*
    tlist *t = const_cast<tlist *>(begin);

    while (t != nullptr) {
        if (t->data == x)
            break;
        t = t->next;
    }
    // std::cout << t;
    return t;
}

int nullCount(int num, tlist * &l) {
	tlist *p = find(l, 0);
	int c = 0;
    while (p) {  // p != nullptr
        c++;
        p = find(p->next, 0);
    }
	return c;
}

void middleInsert(int num, tlist::datatype e, tlist * &l) {
		tlist * prev = nullptr;
		tlist * p = l;
		tlist * mid = new tlist;
		int b = num/2;
		int i = 0;
		while (i < b + 1) {
			if (i >= b) {
				prev->next = mid;
				mid->next = p;
				mid->data = e;
			}
			prev = p;
			p = p->next;
			i++;
		}
}

int deleteAfter(tlist::datatype symb, int count, tlist * &l) {
	tlist * pos = find(l,symb);
	if (pos == nullptr) {
		return -1;
	}
	tlist * prev = pos;
	tlist * p = prev->next;
	for (int i = 0; i < count;i++) {
		if (p != nullptr) {
			prev = p;
			p = p->next;
			delete prev;
		}
	}
	pos->next = p;
	return 0;
}

void listSplit(tlist * &s,tlist * &odd,tlist * &even) {
	// int i = 1;
	// tlist * o,e = nullptr;
	tlist * curr = s;
	tlist * ostart = odd;
	tlist * estart = even;
	tlist * oend = nullptr;
	tlist * eend = nullptr;
	// cout << "There" << curr << s << endl;

	while (curr != nullptr) {
			// tlist * next = curr->next;
			// tlist *t = curr;
			if (curr->data%2==0) {
				tlist *node = new tlist;
				node->data = curr->data;
				node->next = nullptr;

				if (ostart == nullptr)
						ostart = node;

				if (oend != nullptr)
						oend->next = node;
				oend = node;
				// odd = curr;
				// odd = odd->next;
			}
			else {
				tlist *node = new tlist;
				node->data = curr->data;
				node->next = nullptr;

				if (estart == nullptr)
						estart = node;

				if (eend != nullptr)
						eend->next = node;
				eend = node;
				// even = curr;
				// even = even->next;
			}
			curr = curr->next;
	}
	odd = ostart;
	even = estart;
	// cout << ostart->data << endl;
}
