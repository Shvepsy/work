#ifndef HEAD_H_INCLUDED
#define HEAD_H_INCLUDED

struct link {
  int data;
  link* next;
  link* prev;
};

void additem_right(link*&, int);
void print(link*);
void print_rev(link*);

void less_swap(link*&);
int len_list(link*);
link* inc(link*, int);
link* dec(link*, int);

int count_of_elem(link*, int);  // Q�1
void double_elem(link*&, int);  // Q�2
void insert_elem(link*&, int, int);  // Q�3
void del_max_min(link*&);  // Q�4
void insertion_sort(link*&);  // Q�5
link* binary_search_list(link*, int);  // Q�6

int count_rec(link*, int);  // Q�1 rec
void double_rec(link*&, int);  // Q�2 rec

#endif  // HEAD_H_INCLUDED
