#include <iostream>
#include "head.h"
using namespace std;

int main()
{
    link* first = NULL;
    additem_right(first, 90);
    additem_right(first, -7);
    additem_right(first, -73);

    additem_right(first, 19);
    additem_right(first, 834);
    additem_right(first, 43);

    additem_right(first, 76);
    additem_right(first, 3);
    additem_right(first, 10);

    additem_right(first, -724);
    additem_right(first, 13);
    additem_right(first, -5);

    cout << count_of_elem(first, 90) << endl;

    double_elem(first, 90);
    print(first);

    insert_elem(first, 90, 10000);
    print(first);
    // cout << "there";


    del_max_min(first);
    print(first);

    insertion_sort(first);
    print(first);

    int cntr = count_rec(first, 90);
    cout << "Elem: 90" << endl;
    cout << "Count: " << cntr << endl;

    double_rec(first, 90);
    print_rev(first);
    insertion_sort(first);
    print(first);

    insert_elem(first, 10000, 90);
    insertion_sort(first);
    print(first);

    link* res = binary_search_list(first, 90);
    cout << "Find elem: " << res -> data << endl;
    cout << "Find location: " << &res << endl;

    return 0;
}
