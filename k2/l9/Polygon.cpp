#include "Polygon.h"
#include <fstream>

Polygon::Polygon(Point* t, int size) {
  N = size;
  top = new Point[size];
  for (int i = 0; i < N; i++) top[i] = t[i];
}

Polygon::Polygon() {
  top = nullptr;
  N = 0;
}

Polygon::~Polygon() { delete[] top; }

Polygon::Polygon(Polygon& pol2) {
  N = pol2.get_size();
  top = new Point[N];
  for (int i = 0; i < N; i++) top[i] = pol2[i];
}

int Polygon::get_size() const { return N; }

void Polygon::set_i_top(int i, Point value) {
  if (i >= 0 && i < N) {
    top[i].reset_x(value.get_x());
    top[i].reset_y(value.get_y());
  }
}

void Polygon::set_size(int size) {
  N = size;
  top = new Point[size];
}

Polygon& Polygon::operator=(Polygon& right) {
  if (this == &right) {
    return *this;
  }
  N = right.get_size();
  top = new Point[N];
  for (int i = 0; i < N; i++) top[i] = right[i];
  return *this;
}

Point& Polygon::operator[](int i) {
  if (i >= 0 && i < N) return top[i];
}

bool Polygon::operator==(Polygon& right) {
  if (N != right.get_size())
    return 0;
  else {
    for (int i = 0; i < N; i++)
      if (top[i] != right[i]) return 0;
    return 1;
  }
}

bool Polygon::operator!=(Polygon& right) {
  if (N == right.get_size())
    return 0;
  else {
    for (int i = 0; i < N; i++)
      if (top[i] == right[i]) return 0;
    return 1;
  }
}

ostream& operator<<(ostream& s, const Polygon& pol1) {
  int N = pol1.get_size();
  s << "Tops: " << endl;
  for (int i = 0; i < N; i++) s << " " << pol1.top[i] << endl;
  return s;
}

istream& operator>>(istream& s, Polygon& pol1) {
  cout << "Enter N: ";
  int N;
  s >> N;
  pol1.set_size(N);

  for (int i = 0; i < N; i++) {
    cout << "Enter " << i + 1 << "st top: " << endl;
    Point tmp;
    s >> tmp;
    pol1.set_i_top(i, tmp);
  }
  return s;
}

Point Polygon::center_of_gravity() {
  double x = 0;
  double y = 0;
  for (int i = 0; i < N; i++) {
    x += top[i].get_x();
    y += top[i].get_y();
  }
  x /= N;
  y /= N;
  return Point(x, y);
}

int Polygon::is_convex() {
  int i, j, k;
  double flag = 0;
  double z;

  if (N < 3) return 0;

  for (i = 0; i < N; i++) {
    j = (i + 1) % N;
    k = (i + 2) % N;

    z = (top[j].get_x() - top[i].get_x()) * (top[k].get_y() - top[j].get_y());
    z -= (top[j].get_y() - top[i].get_y()) * (top[k].get_x() - top[j].get_x());

    if (flag * z < 0) return -1;

    flag = z;
  }
  return 1;
}

double Polygon::p() {
  double perim = 0;
  for (int i = 0; i < N - 1; i++) {
    perim += top[i].distance(top[i + 1]);
  }
  perim += top[N - 1].distance(top[0]);
  return perim;
}

double Polygon::S() {
  double sq = 0;
  for (int i = 0; i < N - 1; i++) {
    sq += (top[i].get_x() + top[i + 1].get_x()) *
          (top[i].get_y() - top[i + 1].get_y());
  }
  sq += (top[N - 1].get_x() + top[0].get_x()) *
        (top[N - 1].get_y() - top[0].get_y());
  sq /= 2;
  return sq;
}

void Polygon::print_to_file() {
  string s;
  ofstream out("result.txt", ios_base::app);

  out << "N ";
  for (int i = 0; i < N; i++) {
    out << "(" << top[i].get_x() << "; " << top[i].get_y() << ") ";
  }
  out << endl;
  out.close();
}
