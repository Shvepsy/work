#pragma once
#include "Polygon.h"

const double PI = 3.141592;

class Right_polygon : public Polygon {
  double side_length;
  Point center;

 public:
  Right_polygon(int, double, Point);
  double get_length();
  Point get_center();

  Point center_of_gravity(); 
  double S();
  double p();
  int is_convex();

  double r();
  double R();

  void print_to_file();
};
