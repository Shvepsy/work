#pragma once
#include "Polygon.h"

class Triangle : public Polygon {
 public:
  Triangle(Point*);
  ~Triangle() {}
  Point get_top0();
  Point get_top1();
  Point get_top2();
  void type_of_triangle();

  double p();  
  double S();

  double r();
  double R();

  void print_to_file();
};
