#pragma once
#include "Polygon.h"

class Rectangle : public Polygon {
  double width;
  double heigth;

 public:
  Rectangle(Point, Point);
  Rectangle(Point, double, double);
  double get_width();
  double get_heigth();

  Point center_of_gravity();  
  double S();
  double p();
  int is_convex();

  double r();
  double R();

  void print_to_file();
};
