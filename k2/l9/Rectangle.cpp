#include "Rectangle.h"
#include <fstream>

Rectangle::Rectangle(Point left_upper_top, Point right_lower_top) {
  N = 4;
  top = new Point[4];
  top[0] = left_upper_top;
  top[1] = Point(right_lower_top.get_x(), left_upper_top.get_y());
  top[2] = right_lower_top;
  top[3] = Point(left_upper_top.get_x(), right_lower_top.get_y());
  width = top[0].distance(top[1]);
  heigth = top[0].distance(top[3]);
}

Rectangle::Rectangle(Point left_upper_top, double w, double h) {
  N = 4;
  top = new Point[4];
  top[0] = left_upper_top;
  top[1] = Point(left_upper_top.get_x() + w, left_upper_top.get_y());
  top[2] = Point(left_upper_top.get_x() + w, left_upper_top.get_y() - h);
  top[3] = Point(left_upper_top.get_x(), left_upper_top.get_y() - h);
  width = w;
  heigth = h;
}

double Rectangle::get_width() { return width; }

double Rectangle::get_heigth() { return heigth; }

Point Rectangle::center_of_gravity() {
  return Point(((top[0].get_x() + top[1].get_x()) / 2),
               ((top[0].get_y() + top[3].get_y()) / 2));
}

double Rectangle::S() { return width * heigth; }

double Rectangle::p() { return 2 * (width + heigth); }

int Rectangle::is_convex() { return 1; }

double Rectangle::r() {
  try {
    if (width != heigth) throw(1);
    return width / 2;
  } catch (...) {
    cout << "Not S. ";
    return -1;
  }
}

double Rectangle::R() {
  try {
    if (width != heigth) throw(1);
    return top[0].distance(top[2]) / 2;
  } catch (...) {
    cout << "Not S. ";
    return -1;
  }
}

void Rectangle::print_to_file() {
  string s;
  ofstream out("result.txt", ios_base::app);

  out << "R ";
  out << "(" << width << "; " << heigth << ")";
  out << endl;
  out.close();
}
