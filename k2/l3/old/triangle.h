//
// ������������ ������ �4. ������. ���� � v�����
// point.h
//
#pragma once

// ����� ����� ��������� ������������ ����������,
// ������ ��� ���������� ���������� �������� ����������.
// const double precision = 1E-10;

// ��������������� ���������� ������ ��������,
// ��� ����� ��������� � ����� ������ � ������� ������.
class test_point;

class triangle
{
private:
    point p1, p2, p3;
public:
    // ����������� � �����������.
    triangle(point p1, point p2, point p3);
    // ����������� �� ���������.
    triangle();

    // ������ ������� � �������� �����
    double get_p1() const;
    double get_p2() const;
    double get_p3() const;

    // ����� ������ ������, �� �������� ��������� ��� �����
    // ������ ����������� � �������� ������ const.
    // ��� ����������, ��� ������ ������ ����� ����������
    // ������������ ��������� ����� ������.

    // ����� ���������� ���������� �� ������ �����.
    // ������� ���������:
    //      target - ������� �����.
    // double distance_to(point target) const;

    string type() const;
    double perimeter() const;
    double square() const;
    double incircle() const;
    double circumcircle() const;
    // ����� ����� ��� �������, ����������� ��������������,
    // ����� ����� ������ � �������� ����� � ������� ������.

    // ����������� �����.
    friend class test_triangle;
    // ������� ������ ����� � �������.
    // extern point get_point();
    friend point get_point();
    // friend void print(point p);
};

// ������� ������ ����� �� �������.
void print(point p);
point get_point();
