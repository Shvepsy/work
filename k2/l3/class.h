// #include <iostream>
// #include <cmath>
// #include <vector>
// #include <fstream>


const double Pi = 3.1415;

template <typename T>
T sqr(T x) {
    return x*x;
}

class point {
private:
    int x_, y_;
public:
    point();

    point(int x, int y);

    point(const point &a);

    void set(int x, int y);

    int x();
    int y();

    void print();

    double distance(point &p);

    ~point();
};

class triangle {
private:
    point p1_;
    point p2_;
    point p3_;
public:

    bool exist();

    triangle(point &a, point &b, point &c);

    triangle();

    void print();

    void set(point &a, point &b, point &c);

    void set_a(int x, int y);
    void set_a(point &tmp);
    void set_b(int x, int y);
    void set_b(point &tmp);
    void set_c(int x, int y);
    void set_c(point &tmp);

    point &p1();
    point &p2();
    point &p3();

    double P();
    double S();
    double r();
    double R();

};
