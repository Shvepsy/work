#include <iostream>
#include <cmath>
#include <vector>
#include <fstream>

const double Pi = 3.1415;

template <typename T>
T sqr(T x) {
    return x*x;
}

class point {
private:
    int x_, y_;
public:
    point() :x_(0), y_(0) {
        //std::cout << "created point(0,0)" << std::endl;
    }

    point(int x, int y) :x_(x), y_(y) {
        //std::cout << "created point(" << x << "," << y << ")" << std::endl;
    }

    point(const point &a) {
        x_ = a.x_;
        y_ = a.y_;
        //std::cout << "created a copy of point(" << x_ << "," << y_ << ")" << std::endl;
    }

    void set(int x, int y) {
        x_ = x;
        y_ = y;
    }

    int x() {
        return x_;
    }
    int y() {
        return y_;
    }

    void print() {
        std::cout << "point(x,y) = (" << x_ << "," << y_ << ")" << std::endl;
    }

    double distance(point &p) {
        int x1 = x_;
        int x2 = p.x_;
        int y1 = y_;
        int y2 = p.y_;

        return(sqrt(sqr(x2 - x1) + sqr(y2 - y1)));
    }

    ~point() {
        //std::cout << "point (" << x_ << "," << y_ << ") deleted" << std::endl;
    }
};

class triangle {
private:
    point p1_;
    point p2_;
    point p3_;
public:

    bool exist() {
        double a = p1_.distance(p2_);
        double b = p2_.distance(p3_);
        double c = p3_.distance(p1_);
        return ((a < b + c) && (b < a + c) && (c < a + b) ? true : false);
    }

    triangle(point &a, point &b, point &c) :p1_(a), p2_(b), p3_(c) {
        if (!exist())
            std::cout << "triangle doesn't exist!" << std::endl;
    }

    triangle() :p1_(0,0), p2_(0,0), p3_(0,0) {}

    void print() {
        std::cout << "======================== TRIANGLE ========================" << std::endl
            << "p1 = (" << p1_.x() << "," << p1_.y() << ")" << std::endl
            << "p2 = (" << p2_.x() << "," << p2_.y() << ")" << std::endl
            << "p3 = (" << p3_.x() << "," << p3_.y() << ")" << std::endl << std::endl
            << "length a = " << p1_.distance(p2_) << std::endl
            << "length b = " << p2_.distance(p3_) << std::endl
            << "length c = " << p3_.distance(p1_) << std::endl << std::endl;

    }

    void set(point &a, point &b, point &c) {
        p1_ = a;
        p2_ = b;
        p3_ = c;
    }

    void set_a(int x, int y) {
        point tmp(x, y);
        p1_ = tmp;
    }
    void set_a(point &tmp) {
        p1_ = tmp;
    }
    void set_b(int x, int y) {
        point tmp(x, y);
        p2_ = tmp;
    }
    void set_b(point &tmp) {
        p2_ = tmp;
    }
    void set_c(int x, int y) {
        point tmp(x, y);
        p3_ = tmp;
    }
    void set_c(point &tmp) {
        p3_ = tmp;
    }

    point &p1() {
        return p1_;
    }
    point &p2() {
        return p2_;
    }
    point &p3() {
        return p3_;
    }

    double P() {
        double a = p1_.distance(p2_);
        double b = p2_.distance(p3_);
        double c = p3_.distance(p1_);
        return (a + b + c);
    }

    double S() {
        double a = p1_.distance(p2_);
        double b = p2_.distance(p3_);
        double c = p3_.distance(p1_);
        double p = (a + b + c) / 2;
        return (sqrt(p*(p - a)*(p - b)*(p - c)));
    }

    double r() {
        double a = p1_.distance(p2_);
        double b = p2_.distance(p3_);
        double c = p3_.distance(p1_);
        return S() / ((a + b + c) / 2);
    }


    double R() {
        double a = p1_.distance(p2_);
        double b = p2_.distance(p3_);
        double c = p3_.distance(p1_);
        return (a*b*c) / (4 * S());
    }

};

class circle {
private:
    point core_;
    double rad_;
public:
    circle(point &a, double radius) :core_(a), rad_(radius) {
        //std::cout << "created circle with core: (" << core_.x() << "," << core_.y() << ") and rad = " << rad_ << std::endl;
    }

    point core() {
        return(core_);
    }

    double radius() {
        return(rad_);
    }

    void set_core(point &a) {
        core_ = a;
    }

    void set_radius(double radius) {
        rad_ = radius;
    }

    void set(point &a, double radius) {
        core_ = a;
        rad_ = radius;
    }

    double S() {
        return(Pi*sqr(rad_));
    }

    double P() {
        return(2 * Pi*rad_);
    }

    bool is_contain(point &a) {
        if (core_.distance(a) > rad_)
            return false;
        else
            return true;
    }
};
