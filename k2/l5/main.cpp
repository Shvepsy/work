#include "class.h"
using namespace std;

int main() {
    setlocale(LC_ALL, "Russian");

    cout.precision(4);

    vector<point> vec_p;
    point a;

    int x, y;

    fstream fin("points.txt");
    if (!fin.is_open()) cout << "doesn't open!" << endl;
    else {
        while (fin >> x >> y) {
            a.set(x, y);
            vec_p.push_back(a);
        }

        for (int i = 0; i < vec_p.size(); i++)
        {
            cout << "Координаты " << i << " точки = (" << vec_p[i].x() << "," << vec_p[i].y() << ")" << endl;
        }

        vector<double> rads;
        double max_rad;

        for (int i = 0; i < vec_p.size(); i++)
        {
            max_rad = 0;
            for (int j = 0; j < vec_p.size(); j++)
            {
                if (vec_p[i].distance(vec_p[j]) > max_rad) max_rad = vec_p[i].distance(vec_p[j]);
            }
            rads.push_back(max_rad);
        }

        cout << endl;

        for (int i = 0; i < rads.size(); i++)
        {
            cout << "Радиус " << i << " = " << rads[i] << endl;
        }

        cout << endl;

        point core;
        circle c(core, 0);

        vector<circle> vec_c;
        for (int i = 0; i < vec_p.size(); i++)
        {
            c.set(vec_p[i], rads[i]);
            vec_c.push_back(c);
        }

        double min = vec_c[0].S();
        cout << "Площадь " << 0 << " = " << min << endl;

        for (int i = 1; i < vec_c.size(); i++)
        {
            cout << "Площадь " << i << " = " << vec_c[i].S() << endl;
            if (vec_c[i].S() < min)
                min = vec_c[i].S();
        }

        cout << endl <<  "Минимальная площадь круга содержащая все точки " << min << endl;

  }
}
