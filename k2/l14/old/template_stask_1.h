#pragma once
#include <iostream>
// ������ � ������� ����������

template <typename T>
class list {
public:

	list()
	{
		first = last = nullptr;
	}
	list(const list &l)
	{
		copy_list(l.first, l.last);
	}
	list &operator=(const list &l)
	{
		delete_list();
		copy_list(l.first, l.last);
		return *this;
	}
	~list()
	{
		delete_list();
	}

	// ���������� ��������� � ������
	size_t size()
	{
		iterator_list  it = begin();
		size_t count = 0;
		while (it != end())
		{
			count++;
			it = ++it;
		}
		return count;
	}

	// �������� ������ �� �������
	bool is_empty() const
	{
		// ������� ������������ � ���� ������
		// ���������� ������������
		return first == nullptr && last == nullptr;
	}

	// ���������� �������� � �����
	void push_back(const T &x)
	{
		if (last == nullptr) {
			last = new node;
			last->prev = nullptr;
			first = last;
		}
		else {
			last->next = new node;
			last->next->prev = last;
			last = last->next;
		}
		last->data = x;
		last->next = nullptr;
	}
	// ���������� ���������� �������
	void pop_back()
	{
		if (first == nullptr)
			throw std::out_of_range("������� ������� � �������� ������� ������");
		node *t = last;
		if (last->prev == nullptr)
		{
			delete[] last;
			last = first = nullptr;
		}//������� 1 �������

		else
		{
			last = last->prev;
			t->prev->next = nullptr;

			delete[] t;
		}
	}
	// ��������� ���������� ��������
	T back() const
	{
		if (is_empty())
			throw std::out_of_range("������� ������� � �������� ������� ������");
		return last->data;
	}

	// ���������� �������� � ������
	void push_front(const T &x)
	{
		if (first == nullptr)
		{
			first = new node;
			first->data = x;
			first->prev = nullptr;
			first->next = nullptr;
		}
		else
		{
			node * t = new node;
			t->data = x;
			t->next = first;
			t->prev = nullptr;
			first = t;
		}
	}
	// ���������� ������� ��������
	void pop_front()
	{
		if (first == nullptr)
			throw std::out_of_range("������� ������� � �������� ������� ������");
		node *t = first;
		if (first->next == nullptr)
		{
			delete[] first;
			last = first = nullptr;
		}//� ������ 1 �������
		else
		{

			first = first->next;
			t->next->prev = nullptr;

			delete[] t;
		}
	}
	// ��������� ������� ��������
	T front() const
	{
		if (first == nullptr)
			throw std::out_of_range("������� ������� � �������� ������� ������");
		return first->data;
	}

	// ����������� �����
	friend class test_list;
private:
	// �������� ���� ������
	struct node {
		T data;
		node *prev, *next;
	};

	// ������ � ����� ������
	node *first, *last;

	// ����� ��� ����������� ��������� �� ������� ������
	void copy_list(const node *from_first, const node *from_last)
	{
		first = nullptr;
		last = nullptr;
		node **to = &first;
		const node *from = from_first;
		while (from != from_last->next) {
			node *prev = *to;
			*to = new node;
			(*to)->prev = prev;
			(*to)->data = from->data;
			to = &(*to)->next;
			from = from->next;
		}
		*to = nullptr;
		last = *to;
	}
	// ����� �������� ������
	void delete_list()
	{
		if (first != nullptr)
		{
			while (first != last) {
				node *t = first;
				first = first->next;
				delete t;
			}
			delete last;
			first = nullptr;
			last = nullptr;
		}
	}

public:
	//
	// ������� ��������
	//

	// ����� ��������� ��������, ��� ���������� ���
	class iterator_list {

		// ��������� �� ���� ������
		node *current;

		// ��������� �� ������
		const list *collection;

		// �������� �����������
		// �������� ������ � ������������� ������� 
		iterator_list(const list *collection, node *current)
		{
			this->collection = collection;
			this->current = current;
		}
	public:
		// �������������
		T &operator*()
		{
			if (current == nullptr)//���� ��������� �� ���� ������ ����
				throw std::out_of_range("������� ������������ ������ ���������");
			return current->data;
		}

		// ��������� (����������)
		iterator_list  &operator++()
		{
			if (current == nullptr)
				throw std::out_of_range("��������� ��������� �� ������ ������");
			current = current->next;
			return *this;//���������� �������� ������� ���������
		}
		// ��������� (�����������)
		iterator_list  operator++(int)
		{
			if (current == nullptr)
				throw std::out_of_range("��������� ��������� �� ������ ������");
			iterator_list  it = *this;
			current = current->next;
			return it;//������� �������,����� �����������
		}
		// ��������� (����������)
		iterator_list  &operator--()
		{
			if (current == nullptr)
				throw std::out_of_range("��������� ��������� �� ������ ������");
			current = current->prev;
			return *this;//���������� �������� ������� ���������
		}
		// ��������� (�����������)
		iterator_list  operator--(int)
		{
			if (current == nullptr)
				throw std::out_of_range("��������� ��������� �� ������ ������");
			iterator_list  it = *this;
			current = current->prev;
			return it;//������� �������,����� �����������
		}

		// ��������� �� ���������
		bool operator==(const iterator_list  &it) const
		{
			return this->collection == it.collection && this->current == it.current;
			//��������,��� ������� ��������� �� ���������� ���������, � ����� �������� �� ���� ����������
		}
		// ��������� �� �����������
		bool operator!=(const iterator_list  &it) const
		{
			return !(*this == it);
		}

		// ��������� �����, 
		// �������� ����� ��������� ���������
		friend class list;
	
	};

	//
	// ������ ��� ������ � ����������
	//

	// ��������� ���������� �� ������ ������
	iterator_list  begin()
	{
		iterator_list  it(this, first);
		return  it;
	}
	// ��������� ��������� �� ������ ������
	iterator_list  end()
	{
		iterator_list  it(this, nullptr);
		return it;
	}

	// ����� �������� � �������� ��������� � ������
	// � ����������� ���������, ������������ �� ����.
	// � ������, ���� ������� � �������� ��������� �� ������,
	// ������������ �������� list::end()
	iterator_list  find(const T &x)
	{
		iterator_list  it = begin();
		while (it != end())
		{
			if (*it == x)
				return it;
			it = ++it;
		}
		return it;
	}

	// ������� ��������, ����� ��������� �� ������� ��������� ��������
	void insert(const iterator_list  &it, const T &x)
	{
		if (it.current == nullptr)
		{
			this->push_back(x);
			return;
		}
		{
			iterator_list  it1 = this->begin();//��������� �������� �� ������ ������� ������
			if (it == it1)
			{
				node* t = new node;
				t->data = x;
				it.current->next->prev = t;
				t->next = it.current->next;
				it1.current->next = t;
				t->prev = it.current;


			}
			if (it1 != it)
			{
				while (it1.current->next->data != it.current->data)//�������� �� ����� ��������� �� ������� � ���������� it
				{
					++it1;
				}
				node* t = new node;
				t->data = x;
				t->next = it.current;
				it.current->prev = t;
				it1.current->next = t;
			}
		}
	}

	// ������� ��������, �� ��������� �� ������� ��������� ��������
	void insert_after(const iterator_list  &it, const T &x)
	{
		if (it.current == nullptr)
		{
			this->push_back(x);
			return;
		}
		{
			iterator_list  it1 = this->begin();//��������� �������� �� ������ ������� ������
			while (it1.current->data != it.current->data)//�������� �� ����� ��������� �� ������� � ���������� it
			{
				++it1;
			}
			it1++;
			node* t = new node;
			t->data = x;
			t->next = it1.current;
			it1.current->prev = t;
			it.current->next = t;
			t->prev = it.current;

		}
	}

	// �������� �������� �� ������� ��������� ��������
	void remove(const iterator_list  &it)
	{
		iterator_list  it1 = begin();//��������� �������� �� ������ ������� ������
		while (it1.current->data != it.current->data)//�������� �� ����� ��������� �� ������� � ���������� it
		{
			++it1;
		}
		node* t = it1.current;
		if (it1.current->next == nullptr)//�������� ���������� ��������
		{

			it1.current->prev->next = nullptr;
			delete[]t;
		}
		else
		{
			if (it1.current->prev != nullptr)
			{
				it1.current->prev->next = it1.current->next;
				it1.current->next->prev = t->prev;
			}
			else

				t->next->prev = nullptr;
		}

		delete[] t;
	}

};

template <typename T>
std::ostream &operator<<(std::ostream &os, list<T> &l)
{
	for (list<T>::iterator_list i = l.begin(); i != l.end(); ++i) {
		os << *i << std::endl;
	}
	return os;
}
