#include "queue.h"
#include <iostream>

using namespace std;
const size_t TIME = 43200;

int main()
{
	setlocale(LC_ALL,"Rus");
	try {
		size_t product, N, max;
		double k;
		cout << "Введите количество касс в магазине" << endl;
		cin >> N;
		shop *cash = new shop [N];//массив по числу касс
		for (int i = 0; i < N; i++)
			cash[i] = { 0,0};
		cout << "Введите время обработки одного товара ( в секундах) " << endl;
		cin >> k;
		cout << "Введите максимально возможно число товаров,которое может взять один покупатель:" << endl;
		cin >> max;
		size_t time = 0;
		double h= max*k / N;
		cout << "Максимальное число покупателей в очереди" << endl;
		cout << TIME / k*N << endl;
		//масимальное число будет,если каждый покупатель будет оплачивать один товар
		queue <size_t> q;
		cout << "TIME = " << TIME << endl;
		while (time < TIME)//пока магазин работает
		{

			//в начале вводим покупателя,так как изначально очередь пустая
			time = time + h;//период поступления в очередь нового покупателя
			cout << "Введите количесвто товара нового покупателя" << endl;
			cin >> product;
			while (product > max)
			{

				cout << "Количество товара данного покупателя превышает масимально допустимое.Введите новое число." << endl;
				cin >> product;
			}
			q.push(product);//добавляем покупателя в очередь
			size_t now_p = q.top();
			q.pop();
			size_t min=0;
			for (int j = 0; j < N; j++)
			{
				if (cash[j].summ_time < cash[min].summ_time)
					min = j;
			}

			cash[min].count++;
			cash[min].summ_time += k*now_p;
		}

		for (int i = 0; i < N; i++)
		{
			cout << "Касса " << i + 1 << " обслужила " << cash[i].count << " покупателей." << endl;
			cout << "Среднее время обслуживания покупателей на "<< i + 1 <<" кассе="<< cash[i].summ_time/cash[i].count << endl;
		}

		delete[] cash;

	}

	catch (out_of_range e) {
		cout << e.what() << endl;
	}
	catch (...)
	{
		cout << "Непредвиденная ошибка" << endl;
	}
	return 0;    
}
