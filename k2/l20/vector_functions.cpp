#include <fstream>
#include <algorithm>
#include <stdexcept>
#include <climits>
#include "vector_functions.h"
using namespace std;

void vector_capacity(vector<int>& v, size_t count)
{
    size_t c = v.capacity();
    cout << "Start" << endl;
    cout << "Capacity: " << c << "\tSize: " << v.size() << endl;
    for (int i = 0; i < count; ++i) {
        v.push_back(rand());
        if (c != v.capacity()) {
            c = v.capacity();
            cout << "Capacity: " << c << "\tSize: " << v.size() << endl;
        }
    }

    cout << "End. Final : " << endl;
    cout << "Capacity: " << c << "\tSize: " << v.size() << endl;

    v.shrink_to_fit();
    if (c != v.capacity()) {
        c = v.capacity();
        cout << "\tAfter capacity trunc : " << endl;
        cout << "Capacity: " << c << "\tSize: " << v.size() << endl;
    }
}

vector<int> max_sum_vector(char *filename)
{
    vector<int> max;
    ifstream fin(filename);
    if (!fin.is_open()) throw("Cant open file!");
    long sum = LONG_MIN;
    vector<int> v;
    while (!fin.eof()) {
        v.clear();
        fin >> v;
        long s = 0;
        for (int i = 0; i < v.size(); i++)
            s += v[i];
        if (sum < s) {
            sum = s;
            max = v;
        }
    }
    return max;
}

void max_min_vector(char *filename)
{
	int max;
	int min;
	int k = 1;
	ifstream fin(filename);
	if (!fin.is_open()) throw("Cant open file!");
	vector<int> v;
	while (!fin.eof())
	{
		v.clear();
		fin >> v;
		if (v.empty()) return;
		max=*max_element(v.begin(), v.end());
		min = *min_element(v.begin(), v.end());
		cout << "St[" << k << "]\tmax: " << max << "\tmin: " << min << endl;
		k++;
	}
}

void vector_uniq(char *filename)
{

	ifstream fin(filename);
	if (!fin.is_open()) throw("Cant open file!");
	vector<int> v;
	while (!fin.eof())
	{
		v.clear();
		fin >> v;
		if (v.empty()) return;
    sort(v.begin(), v.end());
		auto it = unique(v.begin(), v.end());
    v.erase(it, v.end());
    cout << v << endl;
	}
}
