#include "vector_functions.h"

using namespace std;

int main() {
  setlocale(LC_ALL, "Russian");

	vector<int> v;
	size_t cnt = 100;
	vector_capacity(v, cnt);
	cout << endl;
	v.clear();
	v.reserve(cnt);
  vector_capacity(v, cnt);

  vector<int> max = max_sum_vector("vector.txt");
  cout << max << endl;

  cout << "Min-max items" << endl;
  max_min_vector("vector.txt");

  cout << "Uniq items" << endl;
  vector_uniq("vector.txt");

	return 0;
}
