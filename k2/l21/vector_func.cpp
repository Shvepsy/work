#include <algorithm>
#include <iostream>
#include "vector_func.h"
using namespace std;

void test_vector(int N, int M) {
	size_t vec_size;
	cout << "Vector size: ";
	cin >> vec_size;
	vector<int> v(vec_size);

	generate(v.begin(), v.end(), [&N, &M]() { return rand() % (M - N + 1) + N; });
	print_vector("Vector: ", v);

	double avg = 0;
	for_each(v.begin(), v.end(), [&avg, &vec_size](int n) { avg += n / (double)vec_size; });
	cout << "Average value: " << avg << endl;

	cout << "Min module: " << *min_element(v.begin(), v.end(), [](int &op1, int &op2) { return abs(op1) < abs(op2); }) << endl;

	cout << "Count elements less average: " << count_if(v.begin(), v.end(), [&avg](int n) { return n < avg; }) << endl;

	vector<int> buff(v.size());
	remove_copy_if(v.begin(), v.end(), buff.begin(), [&avg](int n) { return n < avg; });
	print_vector("Truncated vector (whithout less average): ", buff);

	std::transform(v.begin(), v.end(), v.begin(), [](double n) { return n*n; });
	print_vector("Square vector: ", v);
	cout << endl;
}

void print_vector(char* prefix, vector<int> &arg) {
	cout << prefix;
	for_each(arg.begin(), arg.end(), [&](int n) { cout << n << " "; });
	cout << endl;
}
