#pragma once
#include <cstdlib>
#include <limits>

void test_matrix(int, int);
int** matrix_alloc(size_t, size_t);
void print_matrix(char*, int**, int, int);
