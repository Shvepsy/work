#include <iostream>
#include <vector>
#include <algorithm>
#include <iomanip>
#include <numeric>
#include <limits>
#include <cstring>
#include "vector_func.h"
#include "matrix_func.h"

using namespace std;

void test_matrix(int N, int M) {
	size_t msize;
	cout << "Input matrix size: ";
	cin >> msize;

	int **matrix = matrix_alloc(msize, msize);
	generate(*matrix, *matrix + msize * msize, [&N, &M]() {
		return rand() % (M - N + 1) + N;
	});
	print_matrix("Maxtrix: ", matrix, msize, msize);

	int **min_row = matrix;
	int min_val = INT32_MAX;
	for_each(matrix, matrix + msize, [&min_val, &min_row, &msize](int *&row) {
		int sum = accumulate(row, row + msize, 0);
		if (sum < min_val) {
			min_val = sum;
			min_row = &row;
		}
	});
	cout << "Row with smallest sum: ";

	for_each(*min_row, *min_row + msize, [](int &el) { cout << el << " ";	});
	cout << endl;

	int **square_matrix = matrix_alloc(msize, msize);
	int c = 0;
	for_each(matrix, matrix + msize, [&](int *&row) {
		int i = &row - matrix;
		for_each(row, row + msize, [&](int &el) {
			int j = &el - row;
			int s = 0;
			int k = 0;
			for_each(row, row + msize, [&](int &el1) {
				s += matrix[i][k] * matrix[k][j];
				k++;
			});
			transform(&s, &s + 1, *square_matrix + c, [&](int &x) { return s; });
			c++;
		});
	});
	print_matrix("Matrix square: ", square_matrix, msize, msize);

	int **end_matrix = remove_if(matrix, matrix + msize, [&msize](int *&row) {
		return count_if(row, row + msize, [](int &el) {return el == 0; }) == msize;
	});
	int rows = end_matrix - matrix;
	print_matrix("Zero value delete. Matrix: ", matrix, rows, msize);

	vector<int> min_abs(rows);
	transform(matrix, matrix + rows, min_abs.begin(), [&min_abs, &msize](int *&row) {
		return *min_element(row, row + msize, [](int &op1, int &op2) { return abs(op1) < abs(op2); });
	});
	print_vector("Min module vector: ", min_abs);
}


int** matrix_alloc(size_t rows, size_t cols) {
	size_t size = rows * cols;
	int *buffer = new int[size];
	int **arr = new int*[rows];
	for(int i = 0; i < rows; i++) arr[i] = buffer + i * cols;
	memset(*arr, 0, size * sizeof(int));
	return arr;
}

void print_matrix(char* prefix, int **arr, int rows, int cols) {
	cout << prefix << endl;
	for_each(arr, arr + rows, [&](int *&row) {
		for_each(row, row + cols, [&](int &el) {  cout << setw(3) << el << " ";	});
		cout << endl;
	});
}
