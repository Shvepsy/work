// ind3_v6.cpp: определяет точку входа для консольного приложения.
//
// #include "stdafx.h"
#include "tree.h"
#include "set.h"
#include "task.h"
#include <fstream>
#include <iostream>
using namespace std;

//Правильная реализация АТД шаблонами дает дополнительные баллы.
int main()
{
	setlocale(LC_ALL, "Rus");

	cout << "Заболняем дерево уникальными жлементами : " << endl;
	int N = 20;
	int A[] = { 333,325,45,88,6,4,1,2,8,7,6,7,6,4,4,4,333,1,0,20 };
	tree<int> my_tree;
	for (int i = 0; i < N; i++) {
		my_tree.push(A[i]);
		cout << my_tree;
	}
	cout << "Удаление элемента 88 : " << endl;
	my_tree.remove(88);
	cout << my_tree;

	cout << "Заполняем множество : " << endl;
	set<int> my_set;
	for (int i = 0; i < N; i++) {
		my_set.push(A[i]);
		cout << my_set;
	}

	cout << "6 в множестве: " << (my_set.isexist(6) ? "Да" : "Нет") << endl;

	cout << "Проверка операции добавления элемента (5,8,7) в множество : " << endl;
	cout <<  my_set << endl;
	my_set += 5;
	my_set += 8;
	my_set += 7;
	cout << my_set;

	cout << "Добавление элемента 6 : " << endl;
	my_set.push(6);
	cout << my_set;

	cout << "Удаление 88 из множества : " << endl;
	my_set -= 88;
	cout << my_set;
	cout << "Удаление 6 из множества : " << endl;
	my_set -= 6;
	cout << my_set;

	cout << "Всего элементов : " << my_set.sum() << endl;
	cout << "Уникальных элементов : " << my_set.count() << endl;

	set<int> my_set2;//создали новое множество
	cout << "Инициализируем set2" << endl;
	my_set2 += 8;
	my_set2 += 5;
	my_set2 += 0;
	my_set2 += 5;
	my_set2 += 6;
	my_set2 += 8;
	my_set2 += 3;
	my_set2 += 5;
	my_set2 += 9;
	cout << "set 1: " << my_set;
	cout << "set 2: " << my_set2;

	cout << "set1 > set 2: " << (my_set > my_set2) << endl;

	set<int> *my_set_union = my_set || my_set2;
	cout << "set1 || set2 = " << *my_set_union << endl;

	cout << "Множество букв из файла и их повторения" << endl;
	cout << *find_symbols("test_idz.txt") << endl;
	return 0;
}
