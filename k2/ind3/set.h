#pragma once
#include "tree.h"
#include "item.h"

template<typename T>
class set :  public tree<item<T>> {//создается дерево, узлы которого -
	// - элементы множества типа Т
private:
	//вывод узла
	virtual void out_tree(node<item<T>> *nd);
	//сумма количеств всех элементов в  узле
	int sum(node<item<T>> *nd);
public:
	bool isexist(T val);//проверяет, есть ли элемент уже в множестве
	//сумма количеств всех элементов
	int sum();
	virtual void push(T val);
	virtual void push(T val, int count);
	virtual void remove(T val);
	set();
	//деструктор, в реализации не нуждается поскольку деструктор реализован в tree
	~set();
	//присваивает множество
	virtual void operator=(set<T> &val);
	//добавляет элемент
	virtual void operator+=(T val);
	//удаляет элемент
	virtual void operator-=(T val);
	//объединение множеств
	 set<T>* operator||(set<T> &val);
	//статичный: кладет элементы почередно из множества в приемник dest
	static void push_to_set(set<T> &dest, node<item<T>>* source);
	//вывод множества
	friend std::ostream& operator<<(std::ostream& os, set<T>& st) {
		if (st._root == nullptr)
			os << "set is empty\n";
		else
			st.out_tree(st._root);
		os << std::endl;
		return os;
	}
};

template<typename T> void set<T>::push(T val) {
	//создается элемент - значение и количетсво повторений 1
	item<T> new_item(val);
	if (this->_root == nullptr) {
		//кладется в корень, если он пуст, вызывается метод базового класса, который создает корень
		tree<item<T>>::push(new_item);
		return;
	}

	//ищется значение во всем дереве
	node<item<T>> *it = this->find(new_item);
	//если найдено - увеличиваем количество повторений в нужном узле
	if (it != nullptr) {
		it->_value._count++;
	}
	//иначе кладем новый элемент во множество
	else {
		tree<item<T>>::push(new_item);
	}
}

template<typename T> void set<T>::push(T val, int count) {
	//создается элемент - значение и количетсво повторений 1
	item<T> new_item(val, count);
	if (this->_root == nullptr) {
		//кладется в корень, если он пуст, вызывается метод базового класса, который создает корень
		tree<item<T>>::push(new_item);
		return;
	}

	//ищется значение во всем дереве
	node<item<T>> *it = this->find(new_item);
	//если найдено - увеличиваем количество повторений в нужном узле
	if (it != nullptr) {
		it->_value._count += count;
	}
	//иначе кладем новый элемент во множество
	else {
		tree<item<T>>::push(new_item);
	}
}

template<typename T> set<T>::set() {}

template<typename T> set<T>::~set() {}

template<typename T> bool set<T>::isexist(T val) {
	if (this->_root == nullptr)
		return false;
	item<T> new_item(val);
	//возращает true если результат поиска элемента не пуст
	return this->find(new_item) != nullptr;
}

template<typename T> void set<T>::remove(T val) {
	item<T> new_it(val);
	tree<item<T>>::remove(new_it);
}

template<typename T> void set<T>::out_tree(node<item<T>> *nd) {
	//вывод множества обходом слева
	//сначала левое поддерево
	if (nd->_left != nullptr)
		out_tree(nd->_left);
	//потом корень: выводится значение и количество повторений значения
	std::cout << nd->_value._val << ":" << nd->_value._count << " ";
	//потом правое поддерево
	if (nd->_right != nullptr)
		out_tree(nd->_right);
}

template<typename T> int set<T>::sum() {
	return sum(this->_root);
}

template<typename T> int set<T>::sum(node<item<T>> *nd) {
	if (nd != nullptr) {
		//считаем количество всех повторений в левом поддереве
		int res_left = sum(nd->_left);
		//потом в правом
		int res_right = sum(nd->_right);
		//потом добавляем количество повторений текущего узла
		int res = nd->_value._count + res_left + res_right;
		return res;
	}
	return 0;
}

template<typename T> void set<T>::operator=(set<T> &val) {
	this->remove_tree(this->_root);
	this->_root = nullptr;
	*this = val;
}

template<typename T> void set<T>::operator+=(T val) {
	this->push(val);
}

template<typename T> void set<T>::operator-=(T val) {
	this->remove(val);
}

template<typename T> set<T>* set<T>::operator||(set<T> &val) {
	set<T> *new_set = new set<T>();
	//положить все дерево - первый операнд
	set<T>::push_to_set(*new_set, this->_root);
	//положить все дерево - второй операнд
	set<T>::push_to_set(*new_set, val._root);
	return new_set;
}


template<typename T> void set<T>::push_to_set(set<T> &dest, node<item<T>>* source) {
	if (source != nullptr) {
		//положить элемент
		dest.push(source->_value._val, source->_value._count);
		set<T>::push_to_set(dest, source->_left);
		set<T>::push_to_set(dest, source->_right);
	}
}
