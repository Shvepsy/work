#pragma once
#include<iostream>
using namespace std;

//���: ������� ���������
template<typename T>
struct item {
public:
	//�������� (����� ��� ������)
	T _val;
	//���������� ����������� �������� �� ���������
	int _count;
	//������� �������
	item();
	//������� �������
	item(T val);
	item(T val, int count);
	//���������� ��������� �������� ��������� � ��������� ���������
	bool operator==(const item<T> op);
	bool operator>(const item<T> op);
	bool operator<(const item<T> op);
	bool operator>(const T op);
	bool operator<(const T op);
	//���������� ��������� �������� ��������� �� �������� ���������
	bool operator==(T op);
	//���������� �������� ������ �������� ��������� � �����
	friend std::ostream& operator<<(std::ostream& os, const item<T>& it) {
		os << it._val;
		return os;
	}
};

template<typename T> bool item<T>::operator==(const item<T> op) {
	//��������� ��������� ��������� �� ���������
	return this->_val == op._val;
}

template<typename T> bool item<T>::operator==(T op) {
	return this->_val == op;
}

template<typename T> bool item<T>::operator>(const item<T> op) {
	return this->_val > op._val;
}

template<typename T> bool item<T>::operator<(const item<T> op) {
	return this->_val < op._val;
}

template<typename T> bool item<T>::operator>(const T op) {
	return this->_val > op;
}

template<typename T> bool item<T>::operator<(const T op) {
	return this->_val < op;
}


template<typename T> item<T>::item(T val) {
	_val = val;
	//�� ��������� �������� �������� � ����� ��������
	_count = 1;
}

template<typename T> item<T>::item(T val, int count) {
	_val = val;
	//�� ��������� �������� �������� � ����� ��������
	_count = count;
}

template<typename T> item<T>::item() {
	_val = 0;
	//�� ��������� �������� �������� � ����� ��������
	_count = 1;
}