#include "Circle.h"

Circle::Circle() : Distance(0, 0), orbit(0) {}
Circle::Circle(double d, int p) : Distance(d, p), orbit(2 * PI * d) {}
Circle::~Circle() {}
double Circle::get_orbit() const { return orbit; }
void Circle::set_dist(const double d) { distance += d; }
void Circle::set_period(const int& p) { period += p; }
void Circle::set_orbit(const double& orb) { orbit += orb; }

Circle& Circle::operator=(Circle& source) {
  distance = source.distance;
  period = source.period;
  orbit = source.orbit;
  return *this;
}

ostream& operator<<(ostream& os, const Circle& c) {
  os << "Distance: " << c.distance << endl;
  os << "Period: " << c.period << " days" << endl;
  os << "Orbit: " << c.orbit << endl;
  return os;
}

istream& operator>>(istream& is, Circle& c) {
  cout << "Distance: ";
  is >> c.distance;
  cout << "Period: ";
  is >> c.period;
  cout << "Orbit: ";
  is >> c.orbit;
  return is;
}

const bool Circle::operator<(const Circle& comparison) const {
  if (distance < comparison.distance && period < comparison.period &&
      orbit < comparison.orbit)
    return 1;
  return 0;
}

const bool Circle::operator>(const Circle& comparison) const {
  if (distance > comparison.distance && period > comparison.period &&
      orbit > comparison.orbit)
    return 1;
  return 0;
}
