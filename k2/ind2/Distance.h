#pragma once
#include <iostream>

using namespace std;

class Distance {
 protected:
  double distance;
  int period;

 public:
  Distance();
  Distance(double, int);
  ~Distance();

  double get_dist() const;
  int get_period() const;
};
