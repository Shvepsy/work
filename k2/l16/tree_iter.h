#pragma once
#include "node.h"
#include <stack>

template<typename T>
class tree_iter {
private:
	node<T> *_curr;
	stack<node<T>*> _tmpstack;
	void remove(node<T> *nd);
	node<T>* find_min_left(node<T> *nd);
public:
	tree_iter();
	tree_iter(node<T> *nd);
	tree_iter(tree_iter<T> &iter);
	~tree_iter();
	void reset();
	node<T>* get_node();
	void set_default(node<T> *nd);
	void set_default_right(node<T> *nd);
	tree_iter<T>* operator++();
	tree_iter<T>* operator++(int);
	tree_iter<T>* operator--();
	tree_iter<T>* operator--(int);
	T& operator*();
	void operator=(node<T> *nd);
	void operator=(tree_iter<T>& arg);
	bool operator==(tree_iter<T>& arg);
	bool operator!=(tree_iter<T>& arg);
	bool is_empty();
};

template<typename T> tree_iter<T>::tree_iter() {
	_curr = nullptr;
}

template<typename T> tree_iter<T>::tree_iter(node<T> *nd) {
	_curr = nullptr;
	set_default(nd);
}

template<typename T> tree_iter<T>::tree_iter(tree_iter<T> &iter) {
	_curr = iter._curr;
}

template<typename T> tree_iter<T>::~tree_iter() {
	_curr = nullptr;
}

template<typename T> void tree_iter<T>::reset() {
	delete _curr;
	_curr = nullptr;
}

template<typename T> node<T>* tree_iter<T>::find_min_left(node<T> *nd) {
	if (nd == nullptr)
		return nullptr;
	node<T>* cr = nd;

	while (cr->_left != nullptr)	{
		_tmpstack.push(cr);
		cr = cr->_left;
	}
	return cr;
}

template<typename T> void tree_iter<T>::remove(node<T> *nd) {
	if (nd != nullptr) {
		if (nd->_left != nullptr) {
			remove_tree(nd->_left);
			delete nd->_left;
			nd->_left = nullptr;
		}
		if (nd->_right != nullptr) {
			remove_tree(nd->_right);
			delete nd->_right;
			nd->_right = nullptr;
		}
	}
}

template<typename T> node<T>* tree_iter<T>::get_node() {
	return _curr;
}

template<typename T> void tree_iter<T>::set_default(node<T> *nd) {
	_curr = nd;

	if (_curr == nullptr)
		return;

	while (!_tmpstack.empty())
		_tmpstack.pop();

	_tmpstack.push(nullptr);

	while (_curr->_left != nullptr) {
		_tmpstack.push(_curr);
		_curr = _curr->_left;
	}
}

template<typename T> void tree_iter<T>::set_default_right(node<T> *nd) {
	_curr = nd;

	if (_curr == nullptr)
		return;

	while (!_tmpstack.empty())
		_tmpstack.pop();

	_tmpstack.push(nullptr);

	while (_curr->_right != nullptr) {
		_tmpstack.push(_curr);
		_curr = _curr->_right;
	}
}


template<typename T> tree_iter<T>* tree_iter<T>::operator++() {
	if (_curr != nullptr) {
		if (_curr->_right != nullptr) {
			_curr = _curr->_right;
			while (_curr->_left != nullptr) {
				_tmpstack.push(_curr);
				_curr = _curr->_left;
			}
		}
		else {
			_curr = _tmpstack.top();
			_tmpstack.pop();
		}
	}

	return this;
}

template<typename T> tree_iter<T>* tree_iter<T>::operator++(int) {
	tree_iter<T> *buff_iter = new tree_iter<T>(*this);
	++(*this);
	return buff_iter;
}

template<typename T> tree_iter<T>* tree_iter<T>::operator--() {
	if (_curr != nullptr) {
		if (_curr->_left != nullptr) {
			_curr = _curr->_left;
			while (_curr->_right != nullptr) {
				_tmpstack.push(_curr);
				_curr = _curr->_right;
			}
		}
		else {
			_curr = _tmpstack.top();
			_tmpstack.pop();
		}
	}

	return this;
}

template<typename T> tree_iter<T>* tree_iter<T>::operator--(int) {
	tree_iter<T> *buff_iter = new tree_iter<T>(*this);
	--(*this);
	return buff_iter;
}

template<typename T> T& tree_iter<T>::operator*() {
	return _curr->_value;
}

template<typename T> void tree_iter<T>::operator=(node<T> *nd) {
	_curr = nd;
}

template<typename T> void tree_iter<T>::operator=(tree_iter<T>& arg) {
	_curr = arg._curr;
}

template<typename T> bool tree_iter<T>::operator==(tree_iter<T>& arg) {
	return _curr == arg._curr;
}

template<typename T> bool tree_iter<T>::operator!=(tree_iter<T>& arg) {
	return _curr != arg._curr;
}

template<typename T> bool tree_iter<T>::is_empty() {
	return _curr == nullptr;
}
