#include "tree.h"

int main()
{
	tree<int> my_tree;
	my_tree.push(6);
	my_tree.push(8);
	my_tree.push(7);
	my_tree.push(6);
	my_tree.push(4);
	my_tree.push(1);
	my_tree.push(2);
	my_tree.push(3);
	cout << "Initial tree" << endl;
	my_tree.print(0);
	cout << "iterator in begin: " << *my_tree.reset_iterator() << endl;
	cout << "with iterator: ";
	my_tree.out();
	cout << "with reverse iterator: ";
	my_tree.out_right();
	my_tree.remove(7);
	cout << "after 7 removing: ";
	my_tree.out();


	if (my_tree.get_iterator().is_empty())
		cout << "iterator after last element (null)\n";
	cout << "input value: ";
	int val;
	cin >> val;
	auto res = my_tree.find_by_iter(val);
	bool found = !res.is_empty();
	cout << (found ? "element found!" : "element not found") << endl;
	my_tree.reset_iterator();
	int max = *my_tree.get_iterator();
	while (!my_tree.get_iterator().is_empty()) {
		int v = *my_tree.get_iterator();
		if (v > max && v < val)
			max = v;
		my_tree.get_iterator()++;
	}

	cout << "max value < " << val << ": " << max << endl;
	my_tree.reset_iterator();
	cout << "last element: " << **my_tree.get_last_element_iterator() << endl;

  return 0;
}
