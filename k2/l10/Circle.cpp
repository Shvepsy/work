#include "Circle.h"

const double PI = 3.141592;

Circle::Circle() {
  center_of_cir = Point(0, 0);
  rad = 0;
}

Circle::Circle(Point c, double r) {
  center_of_cir = c;
  rad = r;
}

/*Methods from abstract class Shape*/

Point Circle::center() const { return center_of_cir; }

double Circle::area() const { return PI * rad * rad; }

double Circle::p() const { return 2 * PI * rad; }

void Circle::print(ostream& os) const {
  os << "Circle." << endl;
  os << "Center of circle: " << center_of_cir << endl;
  os << "Radius of circle: " << rad;
}

/*Methods from abstract class Circled*/

double Circle::radius_of_incircle() const { return rad; }

double Circle::radius_of_circumcircle() const { return rad; }
