#pragma once
#include "Shape.h"
#include "Point_class.h"

class Circle : public Shape {
  Point center_of_cir;
  double rad;

 public:
  Circle();
  Circle(Point, double);

  Point center() const;           // from class Shape
  double area() const;            // from class Shape
  double p() const;       // from class Shape
  void print(ostream& os) const;  // from class Shape

  double radius_of_incircle() const;      // from class Circled
  double radius_of_circumcircle() const;  // from class Circled
};
