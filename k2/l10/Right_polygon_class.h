#pragma once
#include "Polygon_class.h"
#include "Shape.h"

const double PI = 3.141592;

class Right_polygon : public Polygon, public Shape
{
	double side_length;
	Point center_of_pol;
public:
	Right_polygon(int, double, Point);
	double get_length();
	Point get_center();
	Point center_of_gravity();
	int is_convex();

	Point center() const; // from class Shape
	double area() const; // from class Shape
	double perimeter() const; // from class Shape
	void print(ostream& os) const; // from class Shape

	double radius_of_incircle() const; // from class Circled
	double radius_of_circumcircle() const; // from class Circled
};