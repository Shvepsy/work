#pragma once
#include "Point_class.h"
#include "Circled.h"

//����������� ����� � ������
class Shape : public Circled
{
public:
	// ��� ������ �������� ����������� �������
	// ����� ������ ����� ����������� ����������
	virtual ~Shape();

	// ����� ������
	virtual Point center() const = 0;
	// ������� ������
	virtual double area() const = 0;
	// �������� ������
	virtual double perimeter() const = 0;
	// ������� ����� � �������� ����� os
	virtual void print(ostream& os) const = 0;
};

// ����������� �������� ������
std::ostream & operator<<(std::ostream &os, const Shape& s);