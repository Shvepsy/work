#pragma once
#include "Polygon_class.h"
#include "Shape.h"


class Triangle : public Polygon, public Shape
{
public:
	Triangle(Point*);
	~Triangle()
	{}
	Point get_top0();
	Point get_top1();
	Point get_top2();
	void type_of_triangle();

	Point center() const; // from class Shape
	double area() const; // from class Shape
	double perimeter() const; // from class Shape
	void print(ostream& os) const; // from class Shape

	double radius_of_incircle() const; // from class Circled
	double radius_of_circumcircle() const; // from class Circled
};