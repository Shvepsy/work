// lab15.cpp: определяет точку входа для консольного приложения.
//

#include "tree.h"

int main()
{
	setlocale(LC_ALL, "Russian");
	tree<int> my_tree;
	my_tree.push(6);
	my_tree.push(4);
	my_tree.push(1);
	my_tree.push(2);
	my_tree.push(8);
	my_tree.push(7);
	my_tree.push(6);
	my_tree.push(3);
	cout << "Initial tree : " << my_tree;

	cout << "Deep = " << my_tree.height() << ": " << endl;
	// my_tree.print(0);
	cout << "Width = " << my_tree.width() << ": " << endl;
	my_tree.print(0);

	cout << "Delete by value(3)\n";
	my_tree.remove(3);
	cout << "Tree 1: " << my_tree;

	tree<int> my_tree1(my_tree);
	cout << "Tree 2 (copy with copy constructor): " << my_tree1;

	tree<int> my_tree2 = my_tree;
	cout << "Tree 3 (copy with = operator): " << my_tree2;

	cout << "three 1 > three 2: " << (my_tree > my_tree2) << endl;

	return 0;
}
