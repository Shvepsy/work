#include "class.h"

using namespace std;

int main() {
  product pen("pen", 25, 13);
  product a("a", 77, 7);
  product b("b", 45, 234);

  product c(b);
  c.set_name("c");

  product arr[4] = {pen, a, b, c};

  storage str("Склад", arr, 4);

  cout << "Самый дорогой товар = "<< str.find_name_max() << endl;

  cout << "Есть ли pen на складе  = " << (str.is_there("pen") ? "Да" : "Нет") << endl << endl;

  cout << str << endl;

  cin >> str;
  cout << endl << str;
}
