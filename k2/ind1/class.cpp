#include "class.h"

void product::set(std::string name, double price, int quantity) {
  name_ = name;
  price_ = price;
  qt_ = quantity;
}
void product::set_name(std::string name) { name_ = name; }

void product::set_price(double price) { price_ = price; }

void product::set_qt(int n) { qt_ = n; }

std::string product::name() { return name_; }

double product::price() { return price_; }

int product::qt() { return qt_; }

void product::operator=(const product &b) {
  name_ = b.name_;
  price_ = b.price_;
  qt_ = b.qt_;
}

bool product::operator!=(const product &b) {
  if ((name_ != b.name_) || (price_ != b.price_) || (qt_ != b.qt_))
    return true;
  else
    return false;
}

void product::print() {
  std::cout << "product: " << name_ << ", price = " << price_
            << ", quantity = " << qt_ << std::endl;
}

product storage::get_product_number(int n) { return a_[n]; }

int storage::get_number() { return n_; }

void storage::print() {
  for (int i = 0; i < n_; i++) a_[i].print();
}

void storage::set_number(int n) { n_ = n; }

void storage::set_product_number(int n, product &b) { a_[n] = b; }

void storage::set_product_arr(int n, product *a) {
  n_ = n;
  for (int i = 0; i < n_; i++) a_[i] = a[i];
}

void storage::set_product_name_number(int n, product &b) {
  a_[n].set_name(b.name());
}

void storage::set_product_price_number(int n, product &b) {
  a_[n].set_price(b.price());
}

void storage::set_product_qt_number(int n, product &b) { a_[n].set_qt(b.qt()); }

std::string storage::find_name_max() {
  double max = a_[0].price();
  int k = 0;
  for (int i = 1; i < n_; i++)
    if (max < a_[i].price()) {
      max = a_[i].price();
      k = i;
    }
  return a_[k].name();
}

bool storage::is_there(std::string name) {
  for (int i = 0; i < n_; i++)
    if (a_[i].name() == name) return true;
  return false;
}

void storage::operator=(const storage &b) {
  n_ = b.n_;
  for (int i = 0; i < n_; i++) a_[i] = b.a_[i];
}

bool storage::operator==(const storage &b) {
  if (n_ != b.n_)
    return false;
  else {
    for (int i = 0; i < n_; i++) {
      if (a_[i] != b.a_[i]) return false;
    }
  }
  return true;
}

ostream &operator<<(ostream &os, const product &b) {
  os << "name: " << b.name_ << endl
     << "price: " << b.price_ << endl
     << "quantity: " << b.qt_ << endl;
  return os;
}

istream &operator>>(istream &in, product &b) {
  cout << "enter name: ";
  in >> b.name_;
  cout << "enter price: ";
  in >> b.price_;
  cout << "enter quantity: ";
  in >> b.qt_;
  return in;
}

ostream &operator<<(ostream &os, const storage &b) {
  os << "name of storage: " << b.name_ << endl << "products: " << b.n_ << endl;
  for (int i = 0; i < b.n_; i++)
    os << "============ product #" << i + 1 << " ============" << endl
       << b.a_[i];
  return os;
}

istream &operator>>(istream &in, storage &b) {
  cout << "name of storage: ";
  in >> b.name_;
  cout << "products: ";
  in >> b.n_;
  for (int i = 0; i < b.n_; i++) {
    cout << "============ product #" << i + 1 << " ============" << endl;
    in >> b.a_[i];
  }
  return in;
}
