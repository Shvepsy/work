#include "list.h"
#include <fstream>
#include <stdexcept>

using namespace std;

void list::copy_list(const node * from_first, const node * from_last)
{
    first = nullptr;
    last = nullptr;
    node **to = &first;
    const node *from = from_first;
    while (from != from_last->next) {
        node *prev = *to;
        *to = new node;
        (*to)->prev = prev;
        (*to)->data = from->data;
        to = &(*to)->next;
        from = from->next;
    }
    *to = nullptr;
    last = *to;
}

void list::delete_list()
{
    while (first != last) {
        node *t = first;
        first = first->next;
        delete t;
    }
    delete last;
    first = nullptr;
    last = nullptr;
}

list::list()
{
    first = last = nullptr;
}

list::list(const list & L)
{
    copy_list(L.first, L.last);
}

list & list::operator=(const list & L)
{
	if (this == &L) {
		return *this;
	}
    delete_list();
    copy_list(L.first, L.last);
    return *this;
}

list::~list()
{
    delete_list();
}

size_t list::size() const {
	size_t i = 0;
	node *f = first;
	while (f != last) {
		f = f->next;
		++i;
	}
	return i;
}

bool list::is_empty() const
{
	return first == nullptr && last == nullptr;
}

void list::push_back(const datatype & x)
{
    if (last == nullptr) {
        last = new node;
		last->next = nullptr;
        last->prev = nullptr;
        first = last;
    }
    else {
        last->next = new node;
        last->next->prev = last;
        last = last->next;
    }
    last->data = x;
    last->next = nullptr;
}

void list::pop_back()
{
	node *t = last;
	last = last->prev;
	delete t;
	t = nullptr;
}

list::datatype list::back() const
{
    if (is_empty())
        throw std::out_of_range("Empty list");
    return last->data;
}


void list::push_front(const datatype &x) {
	if (first == nullptr) {
		first = new node;
		first->next = nullptr;
		last = first;
	}
	else {
		first->prev = new node;
		first->prev->next = first;
		first = first->prev;
	}
	first->data = x;
	first->prev = nullptr;
}

void list::pop_front() {
	node *t = first;
	first = first->next;
	delete t;
	t = nullptr;
}

list::datatype list::front() const {
	if (is_empty())
		throw std::out_of_range("Empty list");
	return first->data;
}

std::ostream &operator<<(std::ostream &os, list &l)
{
    for (list::iterator i = l.begin(); i != l.end(); ++i) {
		//std::cout << &i << " | " << &(l.end()) << std::endl;
		os << *i << "  ";
    }
    return os;
}

list::datatype &list::iterator::operator*() {
	return current->data;
}


list::iterator &list::iterator::operator++() {
	current = current->next;
	return *this;
}

list::iterator list::iterator::operator++(int) {
	iterator tmp(collection,current);
	++tmp;
	return *this = tmp;
}

bool list::iterator::operator==(const iterator &it) const {
	return (current == it.current);
}

bool list::iterator::operator!=(const iterator &it) const {
	return !(*this == it);
}

list::iterator &list::iterator::operator=(const iterator &l) {
	current = l.current;
	collection = l.collection;
	return *this;
}


list::iterator list::begin() const {
	return iterator(this, first);
}


list::iterator list::end() const{
	return iterator(this, nullptr);
}

list::iterator list::find(const list::datatype &x) const {
	iterator a = begin();
	iterator b = end();
	while (a != b) {
		if (a.current->data == x) return a;
		++a;
	}
	return b;
}

void list::insert(const list::iterator &it, const datatype &x) {
	node *pred = it.current->prev;
	node *nexd = it.current;
	node *Node = new node;

	Node->data = x;
	Node->next = nexd;
	Node->prev = pred;

	pred->next = Node;
	nexd->prev = Node;
}

void list::remove(const list::iterator &it) {
	if (it == begin()) {
		pop_front();
	}
	else if (it.current == last) {
		pop_back();
	}
	else {
		node *pred = it.current->prev;
		node *nexd = it.current->next;
		pred->next = nexd;
		nexd->prev = pred;
		node *t = it.current;
		delete t;
		t = nullptr;
	}
}

void read_from_file(const char *filename) {
	ifstream is(filename);
	list L = list();
	int t;
	while (is >> t) {
		if (L.is_empty())
			L.push_back(t);
		else {
			if (L.last->data <= t)
				L.push_back(t);
			else
				if (L.first->data >= t)
					L.push_front(t);
				else {
					list::iterator it = L.begin();
					while (*it < t)
						++it;
					L.insert(it, t);
				}
		}
	}

	cout << L << endl;
	L.delete_list();
	is.close();
}
