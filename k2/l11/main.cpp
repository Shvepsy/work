#include "Stack.h"
#include "test_d_stack.h"
#include "func.h"
#include <ctime>
#include <iostream>
#include <string>
#include <fstream>
#include <stack>

using namespace std;

int main() {
  setlocale(LC_ALL, "Russian");

  cout << "----------------------------------------Test of "
          "d_Stack------------------------------------------------"
       << endl;
  d_Stack d;
	int t;
  cout << "Init d (d_Stack)" << endl;

  d.push(23);
  d.push(-12);
  cout << "Added 2 elemets to d: " << d << endl;

  t = d.pop();
  cout << "Deleted last element : " << d << endl;

  d_Stack d1 = d;
  cout << "Create d1 based on d: " << endl;

  d1.push(666);
  cout << "Added element to d1: " << d1 << endl;

  cout << "----------------------------------------Test of "
          "L_Stack------------------------------------------------"
       << endl;
  L_Stack L1;
  cout << "Init L1 (L_Stack)" << endl;

  L1.push(65);
  L1.push(-23);
	L1.push(-26);
  cout << "Added 3 elemets to L1 : " << L1 << endl;

  t = L1.pop();
  cout << "Deleted last element : " << t << endl;

  cout << "Current top element : " << L1.top() << endl;

  L1.push(-1001);
  L1.push(-9009);
  cout << "Added 2 elemets to L1: " << endl;

  L_Stack L2(L1);
  cout << "Init L2 based on L1" << endl;

  L_Stack L3 = L2;
  cout << "Init L3 and assign from L1" << endl;
  cout << "L1 = " << L1 << endl;
  cout << "L2 = " << L2 << endl;
  cout << "L3 = " << L3 << endl;

  t = L2.pop();
  t = L2.pop();
  t = L3.pop();
  t = L3.pop();
  t = L3.pop();

  cout << "Deleted 2 elems from l2 and 3 from l3 : " << endl;
  cout << "L1 = " << L1 << endl;
  cout << "L2 = " << L2 << endl;
  cout << "L3 = " << L3 << endl;

  cout << "--------------------------------------------------------------------"
          "-----------------------------------"
       << endl;
  test_d_stack::run();
  cout << "Execute 1000000 operations time." << endl;
  d_Stack s;
  const int MAX_N = 2000;
  int n = MAX_N;

  time_t start = clock();

  while (n > 0) {
    int m = 0;
    while (m < MAX_N) {
      s.push(rand());
      m++;
    }
    while (m > 0) {
      t = s.pop();
      m--;
    }
    n--;
  }

  time_t end = clock();
  double seconds = (end - start) * 1.0 / CLOCKS_PER_SEC;
  cout << "Dynamic array based time : " << seconds << endl;

  L_Stack s1;

  n = MAX_N;

  time_t start1 = clock();

  while (n > 0) {
    int m = 0;
    while (m < MAX_N) {
      s1.push(rand());
      m++;
    }
    while (m > 0) {
      t = s1.pop();
      m--;
    }
    n--;
  }

  time_t end1 = clock();
  double seconds1 = (end1 - start1) * 1.0 / CLOCKS_PER_SEC;
  cout << "List based time : " << seconds1 << endl;

  cout << "Check bracers syntax { , [ , ( " << endl;

  if (check("right_cpp_program.txt"))
    cout << "Right cpp program " << endl;

  if (check("wrong_cpp_program.txt"))
    cout << "Right cpp program " << endl;
  else
    cout << "Wrong program." << endl;

  return 0;
}
