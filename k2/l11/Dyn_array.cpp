//
// ������������ ������ �14. ����������� ���� ������. ����
// Dyn_array.cpp
//
#include "Dyn_array.h"

Dyn_array::Dyn_array() : data(nullptr), size(0) {}

Dyn_array::Dyn_array(size_t size) {
  this->size = size;
  data = new datatype[size];
}

Dyn_array::~Dyn_array() { delete[] data; }

void Dyn_array::copy(const datatype* from, datatype* to, size_t size) {
  for (size_t i = 0; i < size; ++i) {
    to[i] = from[i];
  }
}

Dyn_array::Dyn_array(const Dyn_array& d) {
  size = d.size;
  data = new datatype[size];
  copy(d.data, data, size);
}

Dyn_array& Dyn_array::operator=(const Dyn_array& d) {
  if (size < d.size) {
    delete[] data;
    data = new datatype[d.size];
  }
  size = d.size;
  copy(d.data, data, size);
  return *this;
}

Dyn_array::datatype& Dyn_array::operator[](size_t index) { return data[index]; }

int Dyn_array::count() const { return size; }

void Dyn_array::resize(size_t new_size) {
  if (size == new_size) return;
  datatype* tmp = data;
  data = new datatype[new_size];
  if (size != 0) copy(tmp, data, new_size);
  delete[] tmp;
  size = new_size;
}

void Dyn_array::append(const datatype& x) {
  resize(size + 1);
  data[size - 1] = x;
}
