#include <iostream>
#include <string>
#include <fstream>
#include "func_1.h"
#include "Stack_1.h"

using namespace std;

void check(const char* f_main)
{
	ifstream ifs(f_main);
	char ch;
	L_Stack st;

	while (ifs)
	{
		ifs.get(ch);
		if (ifs.eof())
		{
			if (st.is_empty())
				cout << "Right C++ file" << endl;
			else
				cout << "Wrong C++ file" << endl;
			break;
		}

		if (ch == '(' || ch == '{' || ch == '[')
			st.push(ch);
		if (ch == ')' || ch == '}' || ch == ']')
		{
			if (st.is_empty())
			{
				cout << "Wrong C++ file" << endl;
				break;
			}
			char compar = st.top();
			if (ch == ')' && compar != '(' ||
				ch == '}' && compar != '{' ||
				ch == ']' && compar != '[')
			{
				cout << "Wrong C++ file" << endl;
				break;
			}
		}
	}
}
