//
// ������������ ������ �14. ����������� ���� ������. ����
// Stack.cpp
//
#include "Stack_1.h"
#include <stdexcept>

Stack::~Stack() {}

//����� d_Stack

bool d_Stack::is_empty()
{
    return data.count() == 0;
}

void d_Stack::push(const datatype & x)
{
    data.append(x);
}

void d_Stack::pop()
{
    if (is_empty()) return;
    data.resize(data.count() - 1);
}

Stack::datatype d_Stack::top()
{
    if (is_empty())
        throw out_of_range("������� �������� ������� �� ������� �����");
    return data[data.count() - 1];
}

d_Stack& d_Stack::operator=(const d_Stack& s)
{
	data = s.data;
	return *this;
}

ostream& operator<<(ostream& os, d_Stack& s)
{
	int sz = s.data.count();
	for (int i = 0; i < sz; i++)
		os << s.data[i] << " ";
	return os;
}

//����� L_Stack

L_Stack::L_Stack() : begin(nullptr)
{}

L_Stack::L_Stack(const L_Stack& s)
{
	begin = nullptr;
	Node* cur = s.begin;
	while (cur)
	{
		add(cur->data);
		cur = cur->next;
	}
}

L_Stack& L_Stack::operator=(const L_Stack& s)
{
	begin = nullptr;
	Node* cur = s.begin;
	while (cur)
	{
		add(cur->data);
		cur = cur->next;
	}
	return *this;
}

L_Stack::~L_Stack() {}

bool L_Stack::is_empty()
{
	if (!begin)
		return 1;
	return 0;
}

void L_Stack::push(const Stack::datatype& x)
{
	Node* temp = new Node;
	temp->data = x;
	temp->next = begin;
	begin = temp;
}

void L_Stack::pop()
{
	Node* temp = begin;
	begin = begin->next;
	delete temp;
}

Stack::datatype L_Stack::top()
{
	//Node* temp = begin;
	//begin = begin->next;
	//Stack::datatype x = temp->data;
	//delete temp;
	//return x;

	Stack::datatype x = begin->data;
	return x;
}

ostream& operator<<(ostream& os, L_Stack& s)
{
	L_Stack::Node* current = s.begin;
	while (current)
	{
		cout << current->data << " ";
		current = current->next;
	}
	return os;
}
