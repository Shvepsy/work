#pragma once
#include <string>
#include <iostream>
using namespace std;

class Exception {
protected:
	int code;
	string message;
public:
	Exception();
	void what();
};

class NegativeSizeException : public Exception {
public:
	NegativeSizeException();
};

class NullPointerException : public Exception {
public:
	NullPointerException();
};

class BAD_INDEX_EXP : public Exception {
public:
	BAD_INDEX_EXP();
};

class OutOfBoundException : public Exception {
public:
	OutOfBoundException();
};

class VectorSizeException : public Exception {
public:
	VectorSizeException();
};