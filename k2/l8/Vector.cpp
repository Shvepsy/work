#include <iostream>
#include "Vector.h"
#include "Exception.h"
using namespace std;

//����������� � ��������� �������
vector::vector(int _size)
{
	if (_size < 0)
		throw NegativeSizeException();
	size = _size;
	data = new datatype[_size];
}

vector::~vector()
{}

//����������� � ��������� ������� � ��������
//��� ������������� ���� ��������� �������

vector::vector(int _size, datatype mean)
{
	size = _size;
	data = new datatype[_size];
	for (int i = 0; i < _size; i++)
		data[i] = mean;
}

//����������� �����.
vector::vector(const vector &p)
{
	size = p.size;
	data = new datatype[size];
	for (int i = 0; i < size; i++)
		data[i] = p.data[i];//����������� ���������
}

//����� ���������� ����� ������� ������������� �� ��������
double vector::norma() const {
	double sum = 0;
	for (int i = 0; i < count(); i++)
		sum += data[i] * data[i];
	return sqrt(sum);
}

//
vector& vector::operator=(const vector& d)
{
	if (size < d.size)
	{
		delete[] data;
		data = new datatype[d.size];
	}
	size = d.size;
	copy(d.data, data, size);
	return *this;
}

//
vector vector::operator-(vector v2) const
{
	for (int i = 0; i < size; i++)
		data[i] -= v2.data[i];
	return *this;
}

//
vector vector::operator+(vector v2) const
{
	for (int i = 0; i < size; i++)
		data[i] += v2.data[i];
	return *this;
}

//
vector vector::operator*(vector::datatype mean)
{
	for (int i = 0; i < size; i++)
		data[i] *= mean;
	return *this;
}

//
vector::datatype vector::operator*(vector v2)
{
	if (v2.size != size)
		throw VectorSizeException();
	datatype sum = 0;
	for (int i = 0; i < size; i++) {
		sum += data[i] * v2.data[i];
	}
	return sum;
}

//
const bool vector::operator==(const vector v2)
{
	if (size == v2.size) {
		for (int i = 0; i < size; i++)
			if (data[i] != v2.data[i])
				return false;
		return true;
	}
	return false;
}

//
const bool vector::operator<(const vector v2)
{
	return norma() < v2.norma();
}

//
const bool vector::operator>(const vector v2)
{
	return norma() > v2.norma();
}
