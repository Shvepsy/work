#include "Dyn_array.h"
#include "Vector.h"
#include "Exception.h"
// #include <iostream>
// using namespace std;

int main()
{
	setlocale(LC_ALL, "Russian");

	vector v1(5);
	cout << "Создание с количеством элементов : " << endl;
	cout << v1 << endl;
	vector v2(6, 123);
	cout << "Создание с инициализацей элементов : " << endl;
	cout << v2 << endl;
	vector v3(6, 321);
	vector vectmp = v2 + v3;
	cout << "v2 + v3 = " << vectmp << endl;
	vector v4(6, 221);
	vectmp = v4 - v3 ;
	cout << "v4 - v3 = " << vectmp << endl;
	cout << "v2 * v3 : " << v2 * v3 << endl;
	vector v5(6, 200);
	vectmp = v5 * 0.1;
	cout << "v5 * 0.1: " << vectmp << endl;

	// //1. Bad agrument of constructor.
	// try
	// {
	// 	Dyn_array bad_arr(-3);
	// }
	// catch (char* err)
	// {
	// 	cout << err << endl;
	// }
  //
	// //2. Bad index.
	// Dyn_array arr1(4);
	// try
	// {
	// 	arr1[-22] = 546;
	// }
	// catch (char* err)
	// {
	// 	cout << err << endl;
	// }
	// cout << arr1;
  //
	// //3. Bad value of new size.
	// try
	// {
	// 	arr1.resize(-4);
	// }
	// catch (char* err)
	// {
	// 	cout << err << endl;
	// }
  //
	// //4. Bad index of insertion.
	// try
	// {
	// 	arr1.insert(-19, 100);
	// }
	// catch (char* err)
	// {
	// 	cout << err << endl;
	// }
  //
	// //5. Bad index of removing.
	// try
	// {
	// 	int var = arr1.count();
	// 	arr1.remove_at(var);
	// }
	// catch (char* err)
	// {
	// 	cout << err << endl;
	// }
	// //exceptions
	// try
	// {
	// 	vector vec666(-5);
	// }
	// catch (Exception &e) {
	// 	e.what();
	// }
  //
	// try
	// {
	// 	vector vec777(1);
	// 	vector vec1(5);
	// 	vector vec_s(vec777 * vec1);
	// }
	// catch (Exception &e) {
	// 	e.what();
	// }

	return 0;
}
