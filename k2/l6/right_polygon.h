#ifndef CLASS_RIGHT_POLYGON_H
#define CLASS_RIGHT_POLYGON_H
#include "polygon.h"
const double Pi = 3.141592;

class right_polygon : public polygon {
 private:
  double len_;
  point core_;

 public:
  right_polygon(int size, double len, point c);

  double len();
  point core();

  point core_of_gravity();
  double S();
  double r();
  double R();
  double P();
  bool is_convex();
};

#endif /*CLASS_RIGHT_POLYGON_H*/
