#include "point.h"

void point::set(double x, double y) {
  x_ = x;
  y_ = y;
}

void point::set_x(double x) { x_ = x; }

void point::set_y(double y) { y_ = y; }

double point::x() { return x_; }

double point::y() { return y_; }

void point::print() {
  std::cout << "point(x,y) = (" << x_ << "," << y_ << ")" << std::endl;
}

double point::distance(point &p) {
  double x1 = x_;
  double x2 = p.x_;
  double y1 = y_;
  double y2 = p.y_;

  return (sqrt(sqr(x2 - x1) + sqr(y2 - y1)));
}

bool point::operator==(point &a) {
  if ((x_ == a.x_) && (y_ == a.y_))
    return true;
  else
    return false;
}

bool point::operator!=(point &a) {
  if ((x_ != a.x_) || (y_ != a.y_))
    return true;
  else
    return false;
}

vector point::operator+(vector b) {
  return vector(x_ + b.x(), y_ + b.y());
}

vector point::operator-(vector b) {
  return vector(x_ - b.x(), y_ - b.y());
}

std::ostream &operator<<(std::ostream &s, point &a) {
  s << "(" << a.x_ << ";" << a.y_ << ")";
  return s;
}

std::istream &operator>>(std::istream &s, point &a) {
  s >> a.x_ >> a.y_;
  return s;
}

std::ostream &operator<<(std::ostream &s, vector &a) {
  s << "(" << a.x_ << ";" << a.y_ << ")";
  return s;
}

std::istream &operator>>(std::istream &s, vector &a) {
  s >> a.x_ >> a.y_;
  return s;
}

void vector::set(double x, double y) {
  x_ = x;
  y_ = y;
}

void vector::set(point &a, point &b) {
  x_ = b.x() - a.x();
  y_ = b.y() - a.y();
}

void vector::set_x(double x) { x_ = x; }

void vector::set_y(double y) { y_ = y; }

double vector::x() { return x_; }

double vector::y() { return y_; }

double vector::length() { return sqrt(sqr(x_) + sqr(y_)); }

void vector::normalize() {
  double inversion = 1 / length();
  x_ *= inversion;
  y_ *= inversion;
}

double vector::operator*(vector &b) { return (x_ * b.x_ + y_ * b.y_); }

bool vector::operator==(vector &b) {
  if ((x_ == b.x_) && (y_ == b.y_))
    return true;
  else
    return false;
}

bool vector::operator!=(vector &b) {
  if ((x_ != b.x_) || (y_ != b.y_))
    return true;
  else
    return false;
}

bool vector::operator>(vector &b) {
  if (length() > b.length())
    return true;
  else
    return false;
}

bool vector::operator<(vector &b) {
  if (length() < b.length())
    return true;
  else
    return false;
}

vector vector::operator+(vector &b) {
  return vector(x_ + b.x_, y_ + b.y_);
}

vector vector::operator-(vector &b) {
  return vector(x_ - b.x_, y_ - b.y_);
}

void vector::operator-() {
  x_ = -x_;
  y_ = -y_;
}

vector vector::operator*(double n) { return vector(x_ * n, y_ * n); }

vector vector::operator/(double n) { return vector(x_ / n, y_ / n); }

double vector::corner_with(vector &b) {
  return (x_ * b.x_ + y_ * b.y_) / (length() * b.length());
}
