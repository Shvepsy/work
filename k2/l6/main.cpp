#include "class.h"
using namespace std;

int main() {

  ifstream fin;
  fin.open("data.txt");

  int count = 0;
  fin >> count;
  char ch;
  int i = 0;

  while (i < count) {
    fin >> ch;
    if (ch == 'P') {
      cout << endl << i + 1 << ". Right polygon:" << endl;
      int n;
      double xc, yc;
      double len;
      fin >> n;
      fin >> xc;
      fin >> yc;
      fin >> len;
      point core(xc, yc);

      right_polygon poly(n, len, core);
      cout << "S = " << poly.S() << endl;
      cout << "P = " << poly.P() << endl;
    } else if (ch == 'T') {
      cout << endl << i + 1 << ". Triangle:" << endl;
      double x1, y1, x2, y2, x3, y3;
      fin >> x1 >> y1 >> x2 >> y2 >> x3 >> y3;
      point p1(x1, y1);
      point p2(x2, y2);
      point p3(x3, y3);
      point arr[3] = {p1, p2, p3};

      triangle tri(arr);
      cout << "S = " << tri.S() << endl;
      cout << "P = " << tri.P() << endl;
    } else if (ch == 'R') {
      cout << endl << i + 1 << ". Rectangle:" << endl;
      double x1, y1, x2, y2;
      fin >> x1 >> y1 >> x2 >> y2;
      point p1(x1, y1);
      point p2(x2, y2);

      rectangle rect(p1, p2);
      cout << "S = " << rect.S() << endl;
      cout << "P = " << rect.P() << endl;
    } else
      cout << "Wrong file." << endl;
    i++;
  }

  ///*Test of triangle class*/
  // point p0(3, 4);
  // point p1(0, 2);
  // point p2(-3, 8);
  // point arr[3] = { p0, p1, p2 };
  // triangle tri(arr);
  // cout << tri.get_top0() << endl;
  // cout << tri.get_top1() << endl;
  // cout << tri.get_top2() << endl;

  // tri.type(); cout << endl;
  // cout << tri.P() << endl;
  // cout << tri.S() << endl;
  // cout << tri.r() << endl;
  // cout << tri.R() << endl;

  ///*Test of right polygon class*/
  // point center(0, 0);
  // int len = 10;
  // int N = 5;
  // right_polygon poly(N, len, center);

  // cout << poly << endl;
  // cout << poly.len() << endl;
  // cout << poly.core() << endl;
  // cout << poly.core_of_gravity() << endl;
  // cout << poly.S() << endl;
  // cout << poly.P() << endl;
  // cout << poly.is_convex() << endl;
  // cout << poly.r() << endl;
  // cout << poly.R() << endl;

  ///*Test of rectangle class*/
  // point p0(2, 0);
  // point p1(0, 2);
  // rectangle rect(p0, p1);
  // cout << rect << endl;
  // cout << rect.height() << endl;
  // cout << rect.width() << endl;
  // cout << rect.core_of_gravity() << endl;
  // cout << rect.S() << endl;
  // cout << rect.P() << endl;
  // cout << rect.is_convex() << endl;
  // cout << rect.r() << endl;
  // cout << rect.R() << endl;

}
