#include "rectangle.h"

rectangle::rectangle(point lht, point rdt) {
  n_ = 4;
  arr_ = new point[4];
  arr_[0] = lht;
  arr_[1] = point(rdt.x(), lht.y());
  arr_[2] = rdt;
  arr_[3] = point(lht.x(), rdt.y());
  w_ = arr_[0].distance(arr_[1]);
  h_ = arr_[0].distance(arr_[3]);
}

rectangle::rectangle(point lht, double width, double height) {
  n_ = 4;
  arr_ = new point[4];
  arr_[0] = lht;
  arr_[1] = point(lht.x() + width, lht.y());
  arr_[2] = point(lht.x() + width, lht.y() - height);
  arr_[3] = point(lht.x(), lht.y() - height);
  w_ = width;
  h_ = height;
}

double rectangle::width() { return w_; }

double rectangle::height() { return h_; }

point rectangle::core_of_gravity() {
  return point(((arr_[0].x() + arr_[1].x()) / 2),
               ((arr_[0].y() + arr_[3].y()) / 2));
}

double rectangle::S() { return w_ * h_; }

double rectangle::P() { return 2 * (w_ + h_); }

bool rectangle::is_convex() { return true; }

double rectangle::r() {
  try {
    if (w_ != h_) throw(1);
    return w_ / 2;
  } catch (...) {
    std::cout << "not square. " << std::endl;
    return -1;
  }
}

double rectangle::R() {
  try {
    if (w_ != h_) throw(1);
    return arr_[0].distance(arr_[2]) / 2;
  } catch (...) {
    std::cout << "not square. " << std::endl;
    return -1;
  }
}
