#include "triangle.h"
const double eps = 1e-14;

triangle::triangle() : polygon(0, nullptr) {}

triangle::triangle(point* arr) : polygon(3, arr) {}

point triangle::get_top0() { return arr_[0]; }

point triangle::get_top1() { return arr_[2]; }

point triangle::get_top2() { return arr_[2]; }

void triangle::set_top0(point& a) { arr_[0] = a; }

void triangle::set_top1(point& a) { arr_[1] = a; }

void triangle::set_top2(point& a) { arr_[2] = a; }

void triangle::type() {
  double a = arr_[0].distance(arr_[1]);
  a *= a;
  double b = arr_[0].distance(arr_[2]);
  b *= b;
  double c = arr_[1].distance(arr_[2]);
  c *= c;
  if (fabs(a - b - c) <= eps || fabs(b - a - c) <= eps ||
      fabs(c - a - b) <= eps) {
    std::cout << "Right triangle";
  } else if (a < b + c + eps && b < a + c + eps && c < a + b + eps) {
    std::cout << "Acute triangle";
  } else if (a + eps > b + c || b + eps > a + c || c + eps > a + b) {
    std::cout << "Obtuse triangle";
  }
}

double triangle::P() {
  return arr_[0].distance(arr_[1]) + arr_[0].distance(arr_[2]) +
         arr_[1].distance(arr_[2]);
}

double triangle::S() {
  double s = 0;
  s += (arr_[0].x() - arr_[2].x()) * (arr_[1].y() - arr_[2].y());
  s -= (arr_[1].x() - arr_[2].x()) * (arr_[0].y() - arr_[2].y());
  s = 0.5 * fabs(s);
  return s;
}

double triangle::r() {
  double a = arr_[0].distance(arr_[1]);
  double b = arr_[0].distance(arr_[2]);
  double c = arr_[1].distance(arr_[2]);

  double r = (-a + b + c) * (a - b + c) * (a + b - c);
  r /= 4 * (a + b + c);
  r = sqrt(r);

  return r;
}

double triangle::R() {
  double a = arr_[0].distance(arr_[1]);
  double b = arr_[0].distance(arr_[2]);
  double c = arr_[1].distance(arr_[2]);

  double R = a * b * c / (4 * S());

  return R;
}
