#ifndef CLASS_POINT_H
#define CLASS_POINT_H

#include <iostream>
#include <cmath>
#include <cassert>

template <typename T>
T sqr(T x) {
  return x * x;
}

class vector;

class point {
 protected:
  double x_, y_;

 public:
  point() : x_(), y_(){};
  point(double x, double y) : x_(x), y_(y){};

  void set(double x, double y);
  void set_x(double x);
  void set_y(double y);

  double x();
  double y();

  void print();
  double distance(point &p);

  bool operator==(point &a);
  bool operator!=(point &a);

  friend std::ostream &operator<<(std::ostream &s,
                                  point &a);
  friend std::istream &operator>>(std::istream &s, point &a);
  vector operator+(vector b);
  vector operator-(vector b);
};

class vector {
 private:
  double x_;
  double y_;

 public:
  vector() : x_(0), y_(0){};
  vector(point &p1, point &p2)
      : x_(p2.x() - p1.x()),
        y_(p2.y() - p1.y()){};
  vector(double x, double y)
      : x_(x), y_(y){};

  void set(double x, double y);
  void set(point &a, point &b);
  void set_x(double x);
  void set_y(double y);
  double x();
  double y();

  double length();
  void normalize();
  double corner_with(vector &b);

  double operator*(vector &b);
  bool operator==(vector &b);
  bool operator!=(vector &b);
  bool operator>(vector &b);
  bool operator<(vector &b);
  vector operator+(vector &b);
  vector operator-(vector &b);
  void operator-();
  vector operator*(double n);
  vector operator/(double n);     

  friend std::ostream &operator<<(std::ostream &s,
                                  vector &a);
  friend std::istream &operator>>(std::istream &s,
                                  vector &a);
};

#endif /*CLASS_POINT_H*/
