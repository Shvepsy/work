#ifndef CLASS_RECTANGLE_H
#define CLASS_RECTANGLE_H
#include "polygon.h"

class rectangle : public polygon {
 private:
  double w_;
  double h_;

 public:
  rectangle(point lht, point rdt);
  rectangle(point lht, double width, double height);

  double width();
  double height();

  point core_of_gravity();
  double S();
  double P();
  bool is_convex();
  double r();
  double R();
};

#endif /*CLASS_RECTANGLE_H*/
