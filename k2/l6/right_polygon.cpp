#include "right_polygon.h"

right_polygon::right_polygon(int size, double len, point c) {
  len_ = len;
  core_ = c;
  n_ = size;
  double rad = R();
  double xc = core_.x();
  double yc = core_.y();

  arr_ = new point[n_];

  for (int i = 0; i < n_; i++) {
    arr_[i].set_x(xc + rad * cos(2 * Pi * i / n_));
    arr_[i].set_y(yc + rad * sin(2 * Pi * i / n_));
  }
}

double right_polygon::len() { return len_; }

point right_polygon::core() { return core_; }

point right_polygon::core_of_gravity() { return core_; }

double right_polygon::S() { return n_ * r() * len_; }

double right_polygon::r() { return R() * cos(Pi / n_); }

double right_polygon::P() { return len_ * n_; }

bool right_polygon::is_convex() { return true; }

double right_polygon::R() { return (len_ / (2 * sin(Pi / n_))); }
