#include "polygon.h"

void polygon::print() {
  for (int i = 0; i < n_; i++)
    std::cout << "arr[" << i << "] = " << arr_[i] << std::endl;
}

double polygon::S() {
  double S = 0;

  for (int i = 0; i < n_ - 1; i++)
    S += abs(arr_[i].x() * arr_[i + 1].y() - arr_[i + 1].x() * arr_[i].y());
  S += abs(arr_[n_].x() * arr_[0].y() - arr_[0].x() * arr_[n_].y());
  return S / 2;
}

double polygon::P() {
  double P = 0;
  for (int i = 0; i < n_ - 1; i++) P += arr_[i].distance(arr_[i + 1]);
  P += arr_[n_].distance(arr_[0]);
  return P;
}

point polygon::core_of_gravity() {
  double a = arr_[0].x(), b = arr_[0].y();
  for (int i = 1; i < n_; i++) {
    a += arr_[i].x();
    b += arr_[i].y();
  }
  return point(a / n_, b / n_);
}

bool polygon::is_convex() {
  if (n_ < 3) return true;

  bool flag = true;
  double z;
  int j, k;

  for (int i = 0; i < n_; i++) {
    j = (i) % n_;
    k = (i + 1) % n_;
    z = (arr_[j].x() - arr_[i].x()) * (arr_[k].y() - arr_[j].y());
    z -= (arr_[j].y() - arr_[i].y()) * (arr_[k].x() - arr_[j].x());
  }
  if (z < 0) flag = false;
  return flag;
}

polygon polygon::operator=(polygon& b) {
  n_ = b.n_;
  for (int i = 0; i < n_; i++) arr_[i] = b.arr_[i];
  return polygon(n_, arr_);
}

std::ostream& operator<<(std::ostream& s, polygon& a) {
  for (int i = 0; i < a.n_; i++)
    s << "top[" << i << "] = (" << a.arr_[i].x() << ";" << a.arr_[i].y() << ")"
      << std::endl;
  return s;
}
