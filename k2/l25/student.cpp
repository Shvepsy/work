#include <algorithm>
#include <iterator>
#include <iostream>
#include <fstream>
#include <string>
#include <map>
#include <set>
#include <sstream>

using namespace std;

int main()
{
	setlocale(LC_ALL, "Rus");
	ifstream fin1;
	multimap<int, string> students;
	fin1.open("snames.txt");
	string tmp;
	int man = 0, woman = 0;
	getline(fin1, tmp, ';');
	// cout << *(tmp.end() - 2) << endl;
	// cout << tmp.substr(tmp.size()-2,tmp.size()) << endl;
	while (!fin1.eof())
	{
		if (tmp.substr(tmp.size()-2,tmp.size()) == "ч")
		{
			students.insert(pair<int, string>(1, tmp));
			man++;
		}
		else
		if (tmp.substr(tmp.size()-2,tmp.size()) == "а")
		{
			students.insert(pair<int, string>(0, tmp));
			woman++;
		}
		getline(fin1, tmp, ';');
	}
	fin1.close();


	cout << "Парней: " << man << endl << "Девушек: " << woman << endl;

	set<string> *namesm = new set<string>[3];
	set<string> *namesw = new set<string>[3];

	// cout << namesm[0].empty() << endl;

	for (const auto &s : students){
		if (s.first == 1) {
			string buf;
			stringstream ss(s.second);
			int i = 0;
			// ss >> buf;
			// cout << buf << endl;
			while (ss >> buf) {
				namesm[i].insert(buf);
				i++;
				if (i > 3) i = 0 ;
				buf.clear();
			}
		}
		else {
			string buf;
			stringstream ss(s.second);
			int i = 0;
			while (ss >> buf) {
				namesw[i].insert(buf);
				i++;
				if (i > 3) i = 0 ;
				buf.clear();
			}
		}
		cout << "ПОЛ-" << s.first << " " << "ФИО" << s.second << endl;
	}

cout << namesm[1].size() << endl;

	// for (const auto &s : students)
	// 	cout << "ПОЛ-" << s.first << " " << "ФИО" << s.second << endl;

	/*for (auto it = students.begin(); it != students.end(); it++)
		cout << "ПОЛ-" << it->first <<" "+it->second << endl;*/



	// system("pause");
	return 0;
}
