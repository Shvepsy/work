#include <algorithm>
#include <iterator>
#include <iostream>
#include <fstream>
#include <string>
#include <cctype>
#include <map>
#include <set>

using namespace std;

struct fio {
  string f,i,o,p;
};

int main()
{
    setlocale(LC_ALL, "Rus");


    // map<int, fio> students;
    multimap<string, string> students;
    ifstream fin1;
    fin1.open("snames.txt");
  	string tmp;
  	getline(fin1, tmp, ';');
    while (!fin1.eof())
    {
      if (*(tmp.end() - 1) == 'ч')
      {

        students.insert(pair<string, string>("мужской", " " + tmp));
      }
      else
      {
        if (*(tmp.end() - 1) == 'а');
        students.insert(pair<string, string>("женский", " " + tmp));
      }
      getline(fin1, tmp, ';');
    }

    fin1.close();




    ifstream fin("mapset.cpp");
    // Вывести все символы из файла
    set<char> chars;
    char c;
    while (fin >> c)
        chars.insert(c);
    fin.close();
    cout << "Все символ файла" << endl;
    for (char c : chars)
        cout << c << ", ";
    cout << endl;

    // Исключить из множества символов вспомогательные символы
    string delim = " .!?,:;-=+_#()[]{}<>";
    set<char> ex(delim.begin(), delim.end());

    set<char> result;
    set_difference(chars.begin(), chars.end(), ex.begin(), ex.end(),
        inserter(result, result.end()));

    cout << "Множество без вспомогательных символов" << endl;
    for (char c : result)
        cout << c << ", ";
    cout << endl;

    // Посчитать количество повторений слов в файле
    map<string, int> words;
    fin.open("input.txt");
    string word;
    while (fin >> word) {
        // Приведём слово к нижнему регистру
        // transform(word.begin(), word.end(), word.begin(), tolower);
        // Удалим знаки препинания из конца слова
        if (delim.find(word[word.length()-1]) != string::npos) {
            word.erase(word.length()-1);
        }
        words[word] += 1;
    }
    fin.close();

    for (pair<string, int> elem : words)
        cout << elem.first << "\t: " << elem.second << endl;

    return 0;
}
