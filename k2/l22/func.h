#pragma once
#include <vector>
#include <string>
#include <sstream>
#include <iostream>
#include <algorithm>
using namespace std;

vector<string>* split_strings(string &str);
vector<string>* gen_annagramm(string &str);
void print_strings(char *prefix, vector<string> &v);
