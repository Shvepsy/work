#include "Dyn_array.h"

int main()
{
	setlocale(LC_ALL, "Russian");

	try {
		cout << "Создаем 1 массив : " << endl;
		Dyn_array arr1(4);
		arr1[0] = 21;
		arr1[1] = 165;
		arr1[2] = 65;
		arr1[3] = -198;
		cout << arr1 << endl;

		cout << "Создаем массив 2 : " << endl;
		Dyn_array arr2(6);
		arr2[0] = 45;
		arr2[1] = -875;
		arr2[2] = -27;
		arr2[3] = 36;
		arr2[4] = -452;
		arr2[5] = -67;
		cout << arr2 << endl;

		cout << "Перегружаем оператор = и печатаем 1 массив скопированный с 2го : " << endl;
		arr1 = arr2;
		cout << arr1 << endl;

		cout << "Количество элементов 1 = " << arr1.count() << endl;
		cout << "Увеличиваем массив 1 на 2 элемента : " << endl;
		arr1.resize(arr1.count() + 2);
		cout << arr1 << endl;

		cout << "Добавление числа 123321 в конец 2 го массива : " << endl;
		arr2.append(123321);
		cout << arr2 << endl;
		cout << "Вставляем элемент 233432 на место на 3 место 2го массива : " << endl;
		arr2.insert(3, 233432);
		cout << arr2 << endl;
		cout << "Удаление 2го элемента во 2м массиве : " << endl;
		arr2.remove_at(2);
		cout << arr2 << endl;
		cout << "Удаляем элемент со значением 36 2го массива : " << endl;
		arr2.remove(36);
		cout << arr2 << endl;
	}
	// catch (const  ios_base::failure &e) {
	// 		cerr << "Некорректный ввод: " << e.what() << endl;
	// }
	// catch (const domain_error &e) {
	// 		cerr << "Неверное значение параметра: " << e.what() << endl;
	// }
	// catch (const out_of_range &e) {
	// 		cerr << "Индекс наодится вне границ: " << e.what() << endl;
	// }
	catch (char* err) {
			cerr << "Непредвиденная ошибка: " << err << endl;
	}

	return 0;
}
