#include <vector>
#include <iostream>
#include <algorithm>
#include <cmath>

const double eps = 1e-14;

using namespace std;

int main()
{
	vector<int> v;
	v.insert(v.end(),1);
	v.insert(v.end(),4);
	v.insert(v.end(),9);
	v.insert(v.end(),5);

	for_each(v.begin(), v.end(), [&](int n) { cout << n << " "; });
	cout << endl;

	for_each(v.begin(), v.end(), [&](int n) {
		if (abs(sqrt(n) - int(sqrt(n))) < eps) {
			 cout << n << " Int" << endl;
		 } else {
			 cout << n << " Double" << endl;
		 }
 	 });

	v.clear();
	v.insert(v.end(),-1);
	v.insert(v.end(),-4);
	v.insert(v.end(),-9);

	for_each(v.begin(), v.end(), [&](int n) { cout << n << " "; });
	cout << endl;

	int max = *max_element(v.begin(), v.end(), [](int &op1, int &op2) { return op1 < op2; });
	cout << "Max value " << max << endl;

	replace_if(v.begin(), v.end(), [&](int n) { return (n < 0 && n < max); }, 0);
	// for_each(v.begin(), v.end(), [&](int n) {  if (n < 0 && n < max) replace(v.begin(), v.end(), n, 0); });

	for_each(v.begin(), v.end(), [&](int n) { cout << n << " "; });
	cout << endl;

	return 0;
}
