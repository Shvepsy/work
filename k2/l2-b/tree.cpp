#include <iostream>
#include "tree.h"
using namespace std;

elem* create_bstree(int n)  //#1
{
  elem* root = NULL;
  for (int i = 0; i < n; i++) {
    cout << "Enter elem value: " << endl;
    int a;
    cin >> a;
    insert_element(root, a);
  }
  return root;
}

elem* copy_tree(elem* tree)  //#2
{
  elem* res = new elem;
  res->data = tree->data;

  if (tree->lt) {
    res->lt = copy_tree(tree->lt);
  } else {
    res->lt = NULL;
  }

  if (tree->rt) {
    res->rt = copy_tree(tree->rt);
  } else {
    res->rt = NULL;
  }
  return res;
}

int width_of_tree(elem* tree)  //#3
{
  int depth = depth_of_tree(tree);
  int m = 0;
  for (int i = 0; i <= depth; i++) {
    int w = width_level(tree, i);
    if (w > m) {
      m = w;
    }
  }
  return m;
}

int depth_of_tree(elem* tree)  //#4
{
  int depth = 0;
  int width = width_level(tree, depth);

  while (width != 0) {
    depth++;
    width = width_level(tree, depth);
  }
  return depth;
}

void print_order(elem* tree)  //#5.1
{
  if (tree->lt) {
    print_order(tree->lt);
    cout << tree->data << endl;
  } else {
    cout << tree->data << endl;
  }
  if (tree->rt) {
    print_order(tree->rt);
  }
}

void print_order_rev(elem* tree)  //#5.2
{
  if (tree->rt) {
    print_order_rev(tree->rt);
    cout << tree->data << endl;
  } else {
    cout << tree->data << endl;
  }
  if (tree->lt) {
    print_order_rev(tree->lt);
  }
}

elem* elem_with_value(elem* tree, int a)  //#6
{
  if (!tree) {
    return NULL;
  } else {
    if (tree->data == a) {
      return tree;
    } else {
      if (elem_with_value(tree->lt, a)) {
        return elem_with_value(tree->lt, a);
      } else {
        return elem_with_value(tree->rt, a);
      }
    }
  }
}

void del_all_leafs_with_value(elem*& tree, int a)  //#7
{
  if (tree->lt) {
    if (!tree->lt->lt && !tree->lt->rt) {
      if (tree->lt->data == a) {
        delete tree->lt;
        tree->lt = NULL;
      }
    } else {
      del_all_leafs_with_value(tree->lt, a);
    }
  }

  if (tree->rt) {
    if (!tree->rt->lt && !tree->rt->rt) {
      if (tree->rt->data == a) {
        delete tree->rt;
        tree->rt = NULL;
      }
    } else {
      del_all_leafs_with_value(tree->rt, a);
    }
  }
}

elem* create(int n) {
  if (n > 0) {
    elem* p = new elem;
    cout << "Enter elem: " << endl;
    cin >> p->data;
    int d = n / 2;
    p->lt = create(d);
    p->rt = create(n - d - 1);
    return p;
  } else {
    return NULL;
  }
}

elem* array_to_tree(int* arr, int n) {
  elem* tree = NULL;
  for (int i = 0; i < n; i++) {
    insert_element(tree, arr[i]);
  }
  return tree;
}

void print(elem* tree, int n) {
  if (tree) {
    print(tree->rt, n + 1);
    for (int i = 0; i <= n; ++i) {
      cout << "   ";
    }
    cout << tree->data << "\n";
    print(tree->lt, n + 1);
  }
}

void insert_element(elem*& tree, int a) {
  if (!tree) {
    tree = new elem;
    tree->data = a;
    tree->lt = NULL;
    tree->rt = NULL;
  } else {
    if (a < (tree->data)) {
      insert_element(tree->lt, a);
    } else {
      insert_element(tree->rt, a);
    }
  }
}

int width_level(elem* tree, int level) {
  if (!tree) {
    return 0;
  } else {
    if (level == 0) {
      return 1;
    } else {
      return width_level(tree->lt, level - 1) +
             width_level(tree->rt, level - 1);
    }
  }
}
