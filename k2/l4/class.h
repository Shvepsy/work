// #include <iostream>
// #include <cmath>
// #include <vector>
// #include <fstream>


const double Pi = 3.1415;

template <typename T>
T sqr(T x) {
    return x*x;
}

class vector;

class point {
private:
    int x_, y_;
public:
    point();
    point(int x, int y);
    point(const point &a);
    void set(int x, int y);
    int x();
    int y();
    void print();
    double distance(point &p);
    ~point();
    bool operator== (point &a);
    bool operator!= (point &a);
    vector operator+(vector b);
    vector operator-(vector b);
};

class vector {
private:
    int x_, y_;
public:
    vector();
    vector(int x, int y);
    vector(const vector &a);
    vector(point &p1, point &p2);
    void set(int x, int y);
    int x();
    int y();
    double length();
    vector normal();
    double angle(vector &p);

    void print();
    // double distance(point &p);
    ~vector();
    double operator*(vector &b);
    bool operator==(vector &b);
    bool operator!=(vector &b);
    bool operator>(vector &b);
    bool operator<(vector &b);
    vector operator+(vector &b);
    vector operator-(vector &b);
    void operator-();
    vector operator*(double n);
    vector operator/(double n);
};
