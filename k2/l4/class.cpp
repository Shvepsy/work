#include <iostream>
#include <cmath>
// #include <vector>
#include <fstream>
#include "class.h"
// const double Pi = 3.1415;
//
// template <typename T>
// T sqr(T x) {
//     return x*x;
// }

point::point() {
        this->x_ = 0;
        this->y_ = 0;
    }

point::point(int x, int y) {
      this->x_ = x;
      this->y_ = y;
    }

point::point(const point &a) {
        this->x_ = a.x_;
        this->y_ = a.y_;
        std::cout << "created a copy of point(" << x_ << "," << y_ << ")" << std::endl;
    }

    void point::set(int x, int y) {
        this->x_ = x;
        this->y_ = y;
    }

    int point::x() {
        return this->x_;
    }
    int point::y() {
        return this->y_;
    }

void point::print() {
        std::cout << "point(x,y) = (" << this->x_ << "," << this->y_ << ")" << std::endl;
}

double point::distance(point &p) {
        return(sqrt(sqr(this->x_ - p.x_) + sqr(this->y_ - p.y_)));
};

point::~point() {
        // std::cout << "point (" << x_ << "," << y_ << ") deleted" << std::endl;

};

bool point::operator==(point &a)
{
    if ((x_ == a.x_) && (y_ == a.y_))
        return true;
    else
        return false;
}

bool point::operator!=(point &a)
{
    if ((x_ != a.x_) || (y_ != a.y_))
        return true;
    else
        return false;
}

vector point::operator+(vector b)
{
    return vector(x_ + b.x(), y_ + b.y());
}

vector point::operator-(vector b)
{
    return vector(x_ - b.x(), y_ - b.y());
}



vector::vector() {
        this->x_ = 0;
        this->y_ = 0;
    }

vector::vector(int x, int y) {
      this->x_ = x;
      this->y_ = y;
    }

vector::vector(point &p1, point &p2) {
      this->x_ = p1.x() - p2.x();
      this->y_ = p1.y() - p2.y();
    }

vector::vector(const vector &a) {
        this->x_ = a.x_;
        this->y_ = a.y_;
        std::cout << "created a copy of point(" << x_ << "," << y_ << ")" << std::endl;
    }

    void vector::set(int x, int y) {
        this->x_ = x;
        this->y_ = y;
    }

    int vector::x() {
        return this->x_;
    }
    int vector::y() {
        return this->y_;
    }

void vector::print() {
        std::cout << "vector(x,y) = (" << this->x_ << "," << this->y_ << ")" << std::endl;
}

double vector::length() {
        return(sqrt(sqr(this->x_) + sqr(this->y_)));
}

vector vector::normal() {
        return vector((this->x_)/vector::length(), (this->y_)/vector::length());
}

double vector::angle(vector &p) {
        // double a = this->x_ ;
        // double b = this->x_ ;
        return (acos(((this->x_) *p.x_ + (this->y_) *p.y_)/(vector::length()*p.length())));
}

vector::~vector() {

}

double vector::operator*(vector &b)
{
    return (x_*b.x_ + y_*b.y_);
}

bool vector::operator==(vector &b)
{
    if ((x_ == b.x_) && (y_ == b.y_))
        return true;
    else
        return false;
}

bool vector::operator!=(vector &b)
{
    if ((x_ != b.x_) || (y_ != b.y_))
        return true;
    else
        return false;
}

bool vector::operator>(vector &b)
{
    if (length() > b.length())
        return true;
    else
        return false;
}

bool vector::operator<(vector &b)
{
    if (length() < b.length())
        return true;
    else
        return false;
}

vector vector::operator+(vector &b)
{
    return vector(x_ + b.x_, y_ + b.y_);
}

vector vector::operator-(vector &b)
{
    return vector(x_ - b.x_, y_ - b.y_);
}

void vector::operator-()
{
    x_ = -x_;
    y_ = -y_;
}

vector vector::operator*(double n)
{
    return vector(x_ * n, y_ * n);
}

vector vector::operator/(double n)
{
    return vector(x_ / n, y_ / n);
}
