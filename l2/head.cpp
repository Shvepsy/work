#include <iostream>
#include "head.h"

using namespace std;

void less_swap(link*& elem) {
  if ((elem->prev->data) > (elem->data)) {
    int temp = elem->prev->data;
    elem->prev->data = elem->data;
    elem->data = temp;
  }
}

int len_list(link* first) {
  link* current = first;
  int len = 0;
  while (current) {
    len++;
    current = current->next;
  }
  return len;
}

int count_of_elem(link* first, int d)  // Q�1
{
  int cntr = 0;
  link* current = first;
  while (current) {
    if (current->data == d) {
      cntr++;
    }
    current = current->next;
  }
  return cntr;
}

void double_elem(link*& first, int d)  // Q�2
{
  link* current = first;
  while (current->next) {
    if (current->data == d) {
      link* temp = new link;
      temp->data = current->data;
      temp->next = current->next;
      temp->prev = current;

      current->next->prev = temp;
      current->next = temp;
      current = current->next;
    }
    current = current->next;
  }
  if (current->data == d) {
    current->next = new link;
    current->next->data = current->data;
    current->next->next = NULL;
    current->next->prev = current;
  }
}

void insert_elem(link*& first, int require, int paste)  // Q�3
{
  link* current = first;
  if (current->data == require) {
    link* temp = new link;
    temp->data = paste;
    temp->next = current;
    temp->prev = NULL;

    current->prev = temp;
    first = temp;
  }
  current = current->next;
  while (current) {
    if (current->data == require) {
      link* temp = new link;
      temp->data = paste;
      temp->next = current;
      temp->prev = current->prev;

      current->prev->next = temp;
      current->prev = temp;
    }
    current = current->next;
  }
}

void del_max_min(link*& first)  // Q�4
{
  link* max_el = first;
  link* min_el = first;

  link* current = first;
  while (current) {
    if (current->data > max_el->data) {
      max_el = current;
    }
    if (current->data <= min_el->data) {
      min_el = current;
    }
    current = current->next;
  }

  while (max_el->next != min_el) {
    link* temp = max_el->next;
    max_el->next = max_el->next->next;
    max_el->next->prev = max_el;
    delete temp;
  }
}

void insertion_sort(link*& first)  // Q�5
{
  link* current = first->next;
  link* comparison = first;

  while (current) {
    comparison = current;
    while (comparison->prev) {
      less_swap(comparison);
      comparison = comparison->prev;
    }
    current = current->next;
  }
}

link* inc(link* ptr, int n) {
  link* current = ptr;
  int i = 1;
  while (i <= n && current->next) {
    current = current->next;
    i++;
  }
  return current;
}

link* dec(link* ptr, int n) {
  link* current = ptr;
  int i = 1;
  while (i <= n && current->prev) {
    current = current->prev;
    i++;
  }
  return current;
}

link* binary_search_list(link* first, int d) {
  int len = len_list(first);
  bool flag = false;

  link* current = inc(first, len / 2);

  while (!flag) {
    len /= 2;
    if (len == 0) {
      len++;
    }
    if (d == current->data) {
      return current;
    }
    // std::cout << "there";

    if (d < current->data) {
      current = dec(current, len);
    } else {
      current = inc(current, len);
    }
  }
}

int count_rec(link* first, int d)  // Q�1 rec
{
  if (first) {
    if (first->data == d) {
      return count_rec(first->next, d) + 1;
    } else {
      return count_rec(first->next, d);
    }
  } else {
    return 0;
  }
}

void double_rec(link*& first, int d)  // Q�2 rec
{
  if (first->next) {
    if (first->data == d) {
      link* temp = new link;
      temp->data = d;
      temp->next = first->next;
      temp->prev = first->next->prev;
      first->next->prev = temp;
      first->next = temp;

      double_rec(first->next->next, d);
    } else {
      double_rec(first->next, d);
    }
  } else {
    if (first->data == d) {
      link* temp = new link;
      temp->data = d;
      temp->next = NULL;
      temp->prev = first;
      first->next = temp;
    } else {
      return;
    }
  }
}

void additem_right(link*& first, int d) {
  if (first == NULL) {
    first = new link;
    first->data = d;
    first->next = NULL;
    first->prev = NULL;
  } else {
    link* current = first;
    while (current->next) {
      current = current->next;
    }
    current->next = new link;
    current->next->data = d;
    current->next->next = NULL;
    current->next->prev = current;
  }
}

void print(link* first) {
  cout << "======\n";
  if (first == NULL) {
    cout << "Empty list\n";
  } else {
    link* current = first;
    while (current) {
      cout << current->data << "\n";
      current = current->next;
    }
  }
  cout << "======\n";
}

void print_rev(link* first) {
  cout << "======\n";
  if (first == NULL) {
    cout << "Empty list\n";
  } else {
    link* current = first;
    while (current->next) {
      current = current->next;
    }
    while (current) {
      cout << current->data << "\n";
      current = current->prev;
    }
  }
  cout << "======\n";
}
