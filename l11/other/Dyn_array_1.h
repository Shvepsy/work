//
// ������������ ������ �14. ����������� ���� ������. ����
// Dyn_array.h
//
#pragma once
#include <exception>
#include <iostream>
//#include "crtdynmem.h"

class Dyn_array
{
public:    
    // ��� ������, ���������� � �������
    typedef int datatype;
    
    Dyn_array();
    Dyn_array(size_t);
    Dyn_array(const Dyn_array&);    
    Dyn_array &operator=(const Dyn_array&);

    ~Dyn_array();

    // �������� ������� � ���������
    datatype& operator[](size_t);

    // ���������� ��������� � �������
    int count() const;

    // ��������� ������� �������
    void resize(size_t);

    // ���������� �������� � ����� �������
    // � ����������� ������� �������
    void append(const datatype&);

private:
    datatype *data;
    size_t size;
    void copy(const datatype*, datatype*, size_t);

    friend class test_Dyn_array; //TODO
};

