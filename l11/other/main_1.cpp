#include "Stack_1.h"
#include "test_d_stack_1.h"
#include "func_1.h"
#include <ctime>
#include <iostream>
#include <string>
#include <fstream>
#include <stack>

using namespace std;

int main()
{
	setlocale(LC_ALL, "Rus");
	////Test 1.
	cout << "----------------------------------------Test of d_Stack------------------------------------------------" << endl;
	d_Stack d;//объвляем объект класса стек на базе дин масс
	cout << "Создали объект d класса d_Stack, в настоящий момент он пустой" << endl;

	d.push(23);//добавляем элемент в стек
	d.push(-12);//добавляем элемент в стек
	d.push(123);//добавляем элемент в стек
	d.push(321);//добавляем элемент в стек
	cout << "Добавили 4 элемента в d. Печать текущего состояния d : " << d << endl;

	d.pop();//извлечение самого верхнего элемента
	cout << "Удалили крайний добавленный элемент в d. Печать текущего состояния d : " << d << endl;

	d_Stack d1 = d;//копи
	cout << "Создали объект d1 класса d_Stack, скопировали в него ранее созданный объект d." << endl;

	d1.push(666);
	cout << "Добавили 1 элемент в d1. Печать текущего состояния d1: " << d1 << endl;
	cout << "Печать текущего состояния d : " << d << endl;

	cout << "----------------------------------------Test of L_Stack------------------------------------------------" << endl;
	L_Stack L1;//объвляем объект класса стек на базе списка
	cout << "Создали объект L1 класса L_Stack, в настоящий момент он пустой" << endl;

	L1.push(65);//добавляем элемент в стек
	L1.push(-23);//добавляем элемент в стек
	L1.push(-83);//добавляем элемент в стек
	L1.push(-153);//добавляем элемент в стек
	cout << "Добавили 4 элемента в L1. Печать текущего состояния L1 : " << L1 << endl;

	L1.pop();//извлечение самого верхнего элемента
	cout << "Удалили крайний добавленный элемент в L1." << endl;

	cout << "Печать новой вершины стека " << L1.top() << endl;//печать новой вершины стека
	cout << "Печать текущего состояния L1 : " << L1 << endl;

	L1.push(-1001);//добавляем элемент в стек
	L1.push(-1002);//добавляем элемент в стек
	L1.push(-1003);//добавляем элемент в стек
	cout << "Добавили 3 элемента в L1." << endl;

	L_Stack L2(L1);//создаем копию L1
	cout << "Создали объект L2 класса L_Stack, скопировали в него L1" << endl;

	L_Stack L3 = L2;//
	cout << "Создали объект L3 класса L_Stack, присвоили ему L2" << endl;
	cout << "L1 = " << L1 << endl;
	cout << "L2 = " << L2 << endl;
	cout << "L3 = " << L3 << endl;

	L2.pop();//извлечение самого верхнего элемента
	L2.pop();//извлечение самого верхнего элемента
	L3.pop();//извлечение самого верхнего элемента
	L3.pop();//извлечение самого верхнего элемента
	L3.pop();//извлечение самого верхнего элемента

	cout << "Удалили 2 элемента из L2 и 3 элемента из L3." << endl;
	cout << "L1 = " << L1 << endl;
	cout << "L2 = " << L2 << endl;
	cout << "L3 = " << L3 << endl;


    //Test 2
	cout << "-------------------------------------------------------------------------------------------------------" << endl;
	test_d_stack::run();
	cout << "Сравнить время выполнения 1 000 000 операций поочередной вставки и удаления элементов для обеих реализаций." << endl;
    d_Stack s;
	const int MAX_N = 2000;
	int n = MAX_N;

    time_t start = clock() ;

    while (n > 0)//для каждой итерации внешн цикла добавляем/удаляем тысячу элементов
	{
        int m = 0;
        while (m < MAX_N)
		{
            s.push(rand());
            m++;
        }
        while (m > 0)
		{
            s.pop();
            m--;
        }
        n--;
    }

    time_t end = clock();
    double seconds = (end - start) * 1.0 / CLOCKS_PER_SEC;//результат переводим сразу с мс в с
    cout << "Затраченное время на обработку для стека на базе динамического массива : " << seconds << endl;



	//cout << "Сравнить время выполнения 1 000 000 операций поочередной вставки и удаления элементов для обеих реализаций." << endl;
	L_Stack s1;

	n = MAX_N;

	time_t start1 = clock();

	while (n > 0)//для каждой итерации внешн цикла добавляем/удаляем тысячу элементов
	{
		int m = 0;
		while (m < MAX_N)
		{
			s1.push(rand());
			m++;
		}
		while (m > 0)
		{
			s1.pop();
			m--;
		}
		n--;
	}

	time_t end1 = clock();
	double seconds1 = (end1 - start1) * 1.0 / CLOCKS_PER_SEC;//результат переводим сразу с мс в с
	cout << "Затраченное время на обработку для стека на базе списка : " << seconds1 << endl;


	//cout << "Сравнить время выполнения 1 000 000 операций поочередной вставки и удаления элементов для обеих реализаций." << endl;
	stack<int> s3;

	n = MAX_N;

	start1 = clock();

	while (n > 0)//для каждой итерации внешн цикла добавляем/удаляем тысячу элементов
	{
		int m = 0;
		while (m < MAX_N)
		{
			s3.push(rand());
			m++;
		}
		while (m > 0)
		{
			s3.pop();
			m--;
		}
		n--;
	}

	end1 = clock();
	double seconds3 = (end1 - start1) * 1.0 / CLOCKS_PER_SEC;//результат переводим сразу с мс в с
	cout << "Затраченное время на обработку для стека на базе списка : " << seconds3 << endl;


	cout << "Дан файл, содержащий код программы на C++. Проверить корректность расстановки скобок: { , [ , ( " << endl;

	if (check("right_cpp_program_1.txt"))
		cout << "Right cpp program " << endl;

	// if (check("wrong_cpp_program_2.txt"))
	// 	cout << "Right cpp program " << endl;
	// else cout << "Wrong program." << endl;


		/*check("right_cpp_program_1.txt");
	check("wrong_cpp_program_2.txt");*/

    return 0;
}
