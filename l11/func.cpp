#include <iostream>
#include <string>
#include <fstream>
#include "func.h"
#include "Stack.h"

using namespace std;

bool check(const char *filename) {
  ifstream fin(filename);
  if (!fin.is_open())
    cout << "���� �� ����� ���� ������!" << endl;
  L_Stack st;
  char c;
  while (fin >> c) {
    if (c == '{' || c == '[' || c == '(') st.push(c);
    if (c == '}' || c == ']' || c == ')') {
      char r = st.top();
      if (r != '{') return false;
      st.pop();
    }
    if (c == ']') {
      char r = st.top();
      if (r != '[') return false;
      st.pop();
    }
    if (c == ')') {
      char r = st.top();
      if (r != '(') return false;
      st.pop(); 
    }
  }
  return true;
}
