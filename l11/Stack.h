//
// ������������ ������ �14. ����������� ���� ������. ����
// Stack.h
//
#pragma once
#include <iostream>
#include "Dyn_array.h"
using namespace std;

// ����������� ����
class Stack
{
public:
    typedef int datatype;
    virtual ~Stack() = 0;

    // �������� �� �������
    virtual bool is_empty() = 0;

    // ���������� �������� �� ����
    virtual void push(const datatype &x) = 0;

    // ������� �������� �� �����
    virtual datatype pop() = 0;

    // �������� ������� �����
    virtual datatype top() = 0;
};

// ���� �� ���� �������
class d_Stack : public Stack
{
protected :
	Dyn_array data;
public:
    bool is_empty();
    void push(const datatype&);
    datatype pop();
    datatype top();

	d_Stack& operator=(const d_Stack& s);
    friend ostream& operator<<(ostream& os, d_Stack& s);
    friend class test_d_Stack;
};

// ���� �� ���� ������
class L_Stack : public Stack
{
    // ���� ������
    struct Node
    {
        datatype data;
        Node* next;
    };
    // ������ ������
    Node* begin;
	void add(const datatype& x)
	{
		if (!begin)
		{
			begin = new Node;
			begin->data = x;
			begin->next = nullptr;
			return;
		}
		Node* cur = begin;
		while (cur->next)
			cur = cur->next;
		cur->next = new Node;
		cur->next->data = x;
		cur->next->next = nullptr;
	}
public:
    L_Stack();
    L_Stack(const L_Stack&);
    L_Stack& operator=(const L_Stack&);

    ~L_Stack();

    bool is_empty();
    void push(const datatype&);
    datatype pop();
    datatype top();

    friend ostream& operator<<(ostream& os, L_Stack& s);
};
