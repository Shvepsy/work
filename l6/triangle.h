#ifndef CLASS_TRIANGLE_H
#define CLASS_TRIANGLE_H
#include "polygon.h"

class triangle : public polygon {
 public:
  triangle();
  triangle(point *arr);
  ~triangle() {}
  point get_top0();
  point get_top1();
  point get_top2();

  void set_top0(point &a);
  void set_top1(point &a);
  void set_top2(point &a);

  void type();

  double P();
  double S();
  double r();
  double R();
};
#endif /*CLASS_TRIANGLE_H*/
