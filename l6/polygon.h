#ifndef CLASS_POLYGON_H
#define CLASS_POLYGON_H

#include <iostream>
#include "point.h"

class polygon {
 protected:
  int n_;
  point *arr_;

 public:
  polygon(){};

  polygon(int n, point *a) {
    n_ = n;
    arr_ = new point[n_];

    for (int i = 0; i < n_; i++)  // ����������� � �����������
      arr_[i] = a[i];
  }

  polygon(const polygon &b) {
    n_ = b.n_;
    for (int i = 0; i < n_; i++)  // ����������� �����������
      arr_[i] = b.arr_[i];
  }

  ~polygon() {
    delete[] arr_;  // ����������
  }

  void print();  // ����� ������� ������

  double S();               // �������
  double P();               // ��������
  point core_of_gravity();  // �����
  bool is_convex();  // �������� �� ����������

  polygon operator=(polygon &b);  // �������� ������������ ��� ��������������
  friend std::ostream &operator<<(std::ostream &s,
                                  polygon &a);  // �������� ������
};
#endif /*CLASS_POLYGON_H*/
