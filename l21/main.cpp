#include "vector_func.h"
#include "matrix_func.h"
#include <iostream>

using namespace std;

int main()
{
	int N,M;
	cout << "Input N: ";
	cin >> N;
	cout << "Input M: ";
	cin >> M;

	test_vector(N, M);
	cout << "\n\n";
	test_matrix(N, M);

	return 0;
}
