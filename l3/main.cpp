// #include <vector>
#include <iostream>
#include "class.h"
using namespace std;

int main() {
      // cout << "Введите координаты 1й точки" << endl;
      // point p1 = get_point();
      // cout << "Введите координаты 2й точки" << endl;
      // point p2 = get_point();
      // cout << "Введите координаты 3й точки" << endl;
      // point p3 = get_point();
      point p1(0,0);
      point p2(5,5);
      point p3(0,5);

      triangle tr = triangle(p1,p2,p3);
      tr.print();
      cout << "P = " << tr.P() << endl;
      cout << "S = " << tr.S() << endl;
      cout << "r = " << tr.r() << endl;
      cout << "R = " << tr.R() << endl;

}
