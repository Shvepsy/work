//
// ������������ ������ �4. ������. ���� � v�����
// main.cpp
//
#include <iostream>
#include <stdexcept>
#include "point.h"
#include "test_point.h"
#include "triangle.h"

using namespace std;

int main()
{
    setlocale(LC_ALL, "Russian");

    test_point::run();

    try {
        cout << "������� ���������� �����" << endl;
        point p = get_point();
        print(p);

        // point p2(0,0);
        // std::cout << p2.distance_to(p);

    }
    catch (domain_error e) {
        cout << e.what() << endl;
    }
    // system("pause");
    return 0;
}
