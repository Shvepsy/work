#include <iostream>
#include <cmath>
#include <vector>
#include <fstream>
#include "class.h"
// const double Pi = 3.1415;
//
// template <typename T>
// T sqr(T x) {
//     return x*x;
// }

point::point() {
        this->x_ = 0;
        this->y_ = 0;
    }

point::point(int x, int y) {
      this->x_ = x;
      this->y_ = y;
    }

point::point(const point &a) {
        this->x_ = a.x_;
        this->y_ = a.y_;
        std::cout << "created a copy of point(" << x_ << "," << y_ << ")" << std::endl;
    }

    void point::set(int x, int y) {
        this->x_ = x;
        this->y_ = y;
    }

    int point::x() {
        return this->x_;
    }
    int point::y() {
        return this->y_;
    }

void point::print() {
        std::cout << "point(x,y) = (" << x_ << "," << y_ << ")" << std::endl;
}

double point::distance(point &p) {
        return(sqrt(sqr(this->x_ - p.x_) + sqr(this->y_ - p.y_)));
}

point::~point() {
        //std::cout << "point (" << x_ << "," << y_ << ") deleted" << std::endl;

};

    bool triangle::exist() {
        double a = this->p1_.distance(this->p2_);
        double b = this->p2_.distance(this->p3_);
        double c = this->p3_.distance(this->p1_);
        return ((a < b + c) && (b < a + c) && (c < a + b) ? true : false);
    }

    triangle::triangle(point &a, point &b, point &c) {
      this->p1_ = a;
      this->p2_ = b;
      this->p3_ = c;
        if (!exist())
            std::cout << "triangle doesn't exist!" << std::endl;
    }

    triangle::triangle() {
      this->p1_ = point(0,0);
      this->p2_ = point(0,1);
      this->p3_ = point(1,0);
    }

    void triangle::print() {
        std::cout << "========================  TRIANGLE  ========================" << std::endl
            << "p1 = (" << this->p1_.x() << "," << this->p1_.y() << ")" << std::endl
            << "p2 = (" << this->p2_.x() << "," << this->p2_.y() << ")" << std::endl
            << "p3 = (" << this->p3_.x() << "," << this->p3_.y() << ")" << std::endl << std::endl
            << "length a = " << this->p1_.distance(this->p2_) << std::endl
            << "length b = " << this->p2_.distance(this->p3_) << std::endl
            << "length c = " << this->p3_.distance(this->p1_) << std::endl << std::endl;

    }

    void triangle::set(point &a, point &b, point &c) {
        this->p1_ = a;
        this->p2_ = b;
        this->p3_ = c;
    }

    void triangle::set_a(int x, int y) {
        point tmp(x, y);
        this->p1_ = tmp;
    }
    void triangle::set_a(point &tmp) {
        this->p1_ = tmp;
    }
    void triangle::set_b(int x, int y) {
        point tmp(x, y);
        this->p2_ = tmp;
    }
    void triangle::set_b(point &tmp) {
        this->p2_ = tmp;
    }
    void triangle::set_c(int x, int y) {
        point tmp(x, y);
        this->p3_ = tmp;
    }
    void triangle::set_c(point &tmp) {
        this->p3_ = tmp;
    }

    point &triangle::p1() {
        return this->p1_;
    }
    point &triangle::p2() {
        return this->p2_;
    }
    point &triangle::p3() {
        return this->p3_;
    }

    double triangle::P() {
        double a = this->p1_.distance(this->p2_);
        double b = this->p2_.distance(this->p3_);
        double c = this->p3_.distance(this->p1_);
        return (a + b + c);
    }

    double triangle::S() {
      double a = this->p1_.distance(this->p2_);
      double b = this->p2_.distance(this->p3_);
      double c = this->p3_.distance(this->p1_);
        double p = triangle::P() / 2;
        return (sqrt(p*(p - a)*(p - b)*(p - c)));
    }

    double triangle::r() {
        return triangle::S() / triangle::P();
    }


    double triangle::R() {
      double a = this->p1_.distance(this->p2_);
      double b = this->p2_.distance(this->p3_);
      double c = this->p3_.distance(this->p1_);
        return (a*b*c) / (4 * triangle::S());
    }
