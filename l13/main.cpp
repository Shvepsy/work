#include "template_class.h"
#include "func.h"
#include <string>
#include <cmath>

int main()
{
	setlocale(LC_ALL, "Russian");
	////Test 1
	l_Stack<int> st;
	cout << "Empty check : " << st.is_empty() << endl;
	st.push(2);
	st.push(-3);
	st.push(4);
	st.push(6);
	cout << "After added 4 elements : " << st << endl;
	st.pop();
	cout << "Delete last element  : " << st << endl;
	cout << "Empty check : " << st.is_empty() << endl;
	cout << "Current head : " << st.get_top() << endl;
	cout << "Pop head and delete : " << st.top() << endl;
	cout << "Pop head and delete : " << st.top() << endl;
	cout << "Pop head and delete : " << st.top() << endl;

	cout << "Exception check : " << endl;

	try
	{
		cout << st.get_top() << endl;
	}
	catch (const char* &e)
	{
		std::cerr << e << endl;
	}

	////Q 1
	////char* s = "{[]{([])()}"; //Wrong
	char* s1 = "{[]([])()}"; //Right

	cout << "Check bracers on array: " << s1 << endl;

	l_Stack<char> st1;

	int i = 0;
	while (s1[i] != '\0')
	{
		if (s1[i] == '(' || s1[i] == '{' || s1[i] == '[')
			st1.push(s1[i]);
		if (s1[i] == ')' || s1[i] == '}' || s1[i] == ']')
		{
			if (st1.is_empty())
			{
				cout << "Wrong string." << endl;
				break;
			}
			char compar = st1.top();
			if (s1[i] == ')' && compar != '(' ||
				s1[i] == '}' && compar != '{' ||
				s1[i] == ']' && compar != '[')
			{
				cout << "Wrong string." << endl;
				break;
			}
		}
		i++;
		if (s1[i] == '\0')
		{
			(st1.is_empty()) ? cout << "Right string." << endl : cout << "Wrong string." << endl;
		}
	}

	////Q 2

	string s = "(a+b)*(c-d)/e";
	string res;
	i = 0;
	l_Stack<char> st0;

	while (s[i] != '\0')
	{
		if (s[i] >= 'a' && s[i] <= 'z')
			res += s[i];
		if (receive_priority(s[i]) == 1)
		{
			if (st0.is_empty())
				st0.push(s[i]);
			else
			{
				while (!st0.is_empty() && receive_priority(st0.get_top()) >= 1)// not empty and pri >=1
					res += st0.top();
				st0.push(s[i]);
			}
		}
		if (receive_priority(s[i]) == 2)
		{
			if (st0.is_empty())
				st0.push(s[i]); //push this oreration
			else
			{
				while (!st0.is_empty() && receive_priority(st0.get_top()) >= 2)
					res += st0.top();
				st0.push(s[i]);
			}
		}
		if (s[i] == '(') //if exist => to stack
			st0.push(s[i]);
		if (s[i] == ')') //if exist => (while != ( ) all opers to poliz and delete (
		{
			while (st0.get_top() != '(')
				res += st0.top();
			st0.pop();
		}
		i++;
	}
	while (!st0.is_empty())
		res += st0.top();


	////Q 3
	string s2 = "((1 + 2)*(3 - 4) ^ 2) / 5";
	string res1;
	res1 = poliz(s2);
	cout << "Poliz create : " << res1 << endl;

	cout << "Poliz calculate : ";
	calc(res1);


    return 0;
}
