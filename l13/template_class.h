#pragma once
#include <iostream>
using namespace std;

template <typename T>
class l_Stack
{
	struct Node
	{
		T data;
		Node* next;
	};
	Node* begin;
	void add(const T& x)
	{
		if (!begin)
		{
			begin = new Node;
			begin->data = x;
			begin->next = nullptr;
			return;
		}
		Node* cur = begin;
		while (cur->next)
			cur = cur->next;
		cur->next = new Node;
		cur->next->data = x;
		cur->next->next = nullptr;
	}
public:
	l_Stack() : begin(nullptr)
	{}
	l_Stack(const l_Stack& s)
	{
		begin = nullptr;
		Node* cur = s.begin;
		while (cur)
		{
			add(cur->data);
			cur = cur->next;
		}
	}
	l_Stack& operator=(const l_Stack& s)
	{
		begin = nullptr;
		Node* cur = s.begin;
		while (cur)
		{
			add(cur->data);
			cur = cur->next;
		}
		return *this;
	}

	~l_Stack() {};

	bool is_empty()
	{
		if (!begin)
			return 1;
		return 0;
	}

	void push(const T& x)
	{
		Node* temp = new Node;
		temp->data = x;
		temp->next = begin;
		begin = temp;
	}

	void pop()
	{
		if (begin)
		{
			Node* temp = begin;
			begin = begin->next;
			delete temp;
		}
	}

	T top()
	{
		if (begin)
		{
			Node* temp = begin;
			begin = begin->next;
			T x = temp->data;
			delete temp;
			return x;
		}
		else
			throw "Stack is empty!";
	}

	T get_top()
	{
		if (begin)
			return begin->data;
		else
			throw "Stack is empty!";
	}

	friend ostream& operator<<(ostream& os, l_Stack<T>& s)
	{
		l_Stack<T>::Node* current = s.begin;
		while (current)
		{
			cout << current->data << " ";
			current = current->next;
		}
		return os;
	}
};