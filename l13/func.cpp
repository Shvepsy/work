#include <string>
#include <cmath>
#include "template_class.h"

using namespace std;

int receive_priority(char ch)
{
	if (ch == '+' || ch == '-') return 1;
	if (ch == '*' || ch == '/') return 2;
	if (ch == '^') return 3;
	else return -1;
}

string poliz(string s) {
	string res;
	int i = 0;

	l_Stack<char> st; //char set

	while (s[i] != '\0') {
			if (s[i] >= '0' && s[i] <= '9') 
				res += s[i];
			if (receive_priority(s[i]) == 1)
			{
				if (st.is_empty())
					st.push(s[i]);
				else
				{
					while (!st.is_empty() && receive_priority(st.get_top()) >= 1)
						res += st.top();
					st.push(s[i]);
				}
			}
			if (receive_priority(s[i]) == 2)
			{
				if (st.is_empty())
					st.push(s[i]);
				else
				{
					while (!st.is_empty() && receive_priority(st.get_top()) >= 2)
						res += st.top();
					st.push(s[i]);
				}
			}
			if (receive_priority(s[i]) == 3)
			{
				if (st.is_empty())
					st.push(s[i]);
				else
				{
					while (!st.is_empty() && receive_priority(st.get_top()) == 3)
						res += st.top();
					st.push(s[i]);
				}
			}
			if (s[i] == '(')
				st.push(s[i]);
			if (s[i] == ')')
			{
				while (st.get_top() != '(')
				{
					res += st.top();
				}
				st.pop();
			}
			i++;
		}
		while (!st.is_empty())
		{
			res += st.top();
		}
		return res;
}

void calc(string res) {
	int i = 0;
	l_Stack<double> stack_of_digits;

	while (res[i] != '\0')
	{
		if (res[i] >= '0' && res[i] <= '9') // if digit
			stack_of_digits.push(res[i] - '0');
		else if (res[i] == '+')	{
			double b = stack_of_digits.top();
			double a = stack_of_digits.top();
			stack_of_digits.push(a + b);
		}
		else if (res[i] == '-')	{
			double b = stack_of_digits.top();
			double a = stack_of_digits.top();
			stack_of_digits.push(a - b);
		}
		else if (res[i] == '*')	{
			double b = stack_of_digits.top();
			double a = stack_of_digits.top();
			stack_of_digits.push(a * b);
		}
		else if (res[i] == '/')	{
			double b = stack_of_digits.top();
			double a = stack_of_digits.top();
			stack_of_digits.push(a / b);
		}
		else if (res[i] == '^')	{
			double b = stack_of_digits.top();
			double a = stack_of_digits.top();
			stack_of_digits.push(pow(a, b));
		}
		i++;
	}
	cout << stack_of_digits.get_top() << endl;
}
