#include "Right_polygon.h"
#include <fstream>

Right_polygon::Right_polygon(int size, double len, Point cent) {
  side_length = len;
  center = cent;
  N = size;
  double Rad = R();
  double xc = center.get_x();
  double yc = center.get_y();

  top = new Point[N];

  for (int i = 0; i < N; i++) {
    top[i].reset_x(xc + Rad * cos(2 * 3.141592 * i / N));
    top[i].reset_y(yc + Rad * sin(2 * 3.141592 * i / N));
  }
}

double Right_polygon::get_length() { return side_length; }

Point Right_polygon::get_center() { return center; }

Point Right_polygon::center_of_gravity() { return center; }

double Right_polygon::S() { return N * r() * side_length; }

double Right_polygon::p() { return side_length * N; }

int Right_polygon::is_convex() { return 1; }

double Right_polygon::r() { return R() * cos(PI / N); }

double Right_polygon::R() { return side_length / (2 * sin(PI / N)); }

void Right_polygon::print_to_file() {
  string s;
  ofstream out("result.txt", ios_base::app);

  out << "P ";
  out << N << " (" << center.get_x() << "; " << center.get_y() << ") "
      << side_length;
  out << endl;
  out.close();
}
