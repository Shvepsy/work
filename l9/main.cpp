#include "Triangle.h"
#include "Right_polygon.h"
#include "Rectangle.h"
#include <fstream>
#include <ctime>

int random_int(const int& minimum = 0, const int& range = 10) {
  return (minimum + rand() % range);
}

int main() {
  srand(time(NULL));
  ofstream out("result.txt");

  // Q1
  Point* a = new Point[3];
  a[0].reset_x(2.8);
  a[0].reset_y(9.5);
  a[1].reset_x(8.6);
  a[1].reset_y(-3.5);
  a[2].reset_x(0.2);
  a[2].reset_y(13);
  Polygon pol(a, 3);

  cout << pol.center_of_gravity() << endl;
  cout << pol.is_convex() << endl;
  cout << pol.p() << endl;
  cout << pol.S() << endl;
  cout << endl;

  Point* a2 = new Point[3];
  a2[0].reset_x(0.32);
  a2[0].reset_y(9.63);
  a2[1].reset_x(1.83);
  a2[1].reset_y(8.2);
  a2[2].reset_x(-23.23);
  a2[2].reset_y(-1.25);
  Triangle tri(a2);

  cout << tri.center_of_gravity() << endl;
  cout << tri.is_convex() << endl;
  cout << tri.p() << endl;
  cout << tri.S() << endl;
  cout << endl;

  Polygon* ptr;
  ptr = &pol;
  cout << ptr->S() << endl;
  ptr = &tri;
  cout << ptr->S() << endl;

  // Q 2
  int N = 5;
  Point* a1 = new Point[N];

  for (int i = 0; i < N; i++) {
    a1[i].reset_x(static_cast<double>(random_int(-1000, 2000)) / 100);
    a1[i].reset_y(static_cast<double>(random_int(-1000, 2000)) / 100);
  }
  Polygon pol1(a1, N);  // Random polygon

  for (int i = 0; i < 3; i++) {
    a1[i].reset_x(static_cast<double>(random_int(-1000, 2000)) / 100);
    a1[i].reset_y(static_cast<double>(random_int(-1000, 2000)) / 100);
  }
  Triangle tri1(a);  // Random triangle

  a1[0].reset_x(static_cast<double>(random_int(-1000, 2000)) / 100);
  a1[0].reset_y(static_cast<double>(random_int(-1000, 2000)) / 100);
  double c = static_cast<double>(random_int(-1000, 2000)) / 100;
  Right_polygon rigp(N, c, a1[0]);  // Random triangle right_polygon

  a1[0].reset_x(static_cast<double>(random_int(-1000, 2000)) / 100);
  a1[0].reset_y(static_cast<double>(random_int(-1000, 2000)) / 100);
  a1[1].reset_x(static_cast<double>(random_int(-1000, 2000)) / 100);
  a1[1].reset_y(static_cast<double>(random_int(-1000, 2000)) / 100);
  Rectangle rec(a[0], a[1]);  // Random triangle rectangle

  N = 4;
  Polygon** a_ptr = new Polygon*[N];
  a_ptr[0] = &pol;
  a_ptr[1] = &tri;
  a_ptr[2] = &rigp;
  a_ptr[3] = &rec;

  for (int i = 0; i < N; i++) {
    a_ptr[i]->print_to_file();
  }

  out.close();
  return 0;
}
