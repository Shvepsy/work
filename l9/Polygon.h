#pragma once
#include "Point.h"

class Polygon {
 protected:
  Point* top;
  int N;

 public:
  Polygon(Point*, int);
  Polygon();
  ~Polygon();
  Polygon(Polygon&);

  int get_size() const;
  void set_size(int);
  void set_i_top(int, Point);
  Polygon& operator=(Polygon&);
  Point& operator[](int);
  bool operator==(Polygon&);
  bool operator!=(Polygon&);

  virtual Point center_of_gravity();
  virtual int is_convex();
  virtual double p();
  virtual double S();

  friend ostream& operator<<(ostream&, const Polygon&);
  friend istream& operator>>(istream&, Polygon&);

  virtual void print_to_file();
};
