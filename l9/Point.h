#pragma once
#include <iostream>
#include <cmath>
#include <iomanip>

using namespace std;

class Point {
 private:
  double x, y;

 public:
  Point(double abscissa, double ordinate);
  Point();
  ~Point();
  Point(const Point& p);
  double get_x();
  double get_y();
  double distance(Point& p) const;

  void reset_x(double data);
  void reset_y(double data);

  const bool operator==(const Point& p1);
  const bool operator!=(const Point& p1);

  friend ostream& operator<<(ostream& s, const Point& p1);
  friend istream& operator>>(istream& s, Point& p1);
};
