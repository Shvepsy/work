#include "Point.h"

double Point::get_x() { return x; }

double Point::get_y() { return y; }

double Point::distance(Point& p) const {
  return sqrt(pow((p.x - x), 2) + pow((p.y - y), 2));
}

Point::Point(double abscissa, double ordinate) : x(abscissa), y(ordinate) {}

Point::~Point() {}

Point::Point() : x(0.0), y(0.0) {}

Point::Point(const Point& p) {
  x = p.x;
  y = p.y;
}

void Point::reset_x(double data) { x = data; }

void Point::reset_y(double data) { y = data; }

const bool Point::operator==(const Point& p1) { return x == p1.x && y == p1.y; }

const bool Point::operator!=(const Point& p1) { return x != p1.x || y != p1.y; }

ostream& operator<<(ostream& s, const Point& p1) {
  s << "(" << setw(2) << p1.x << ", " << setw(2) << p1.y << ")";
  return s;
}

istream& operator>>(istream& s, Point& p1) {
  double temp;

  cout << "Enter x: ";
  cin >> temp;
  p1.reset_x(temp);

  cout << "Enter y: ";
  cin >> temp;
  p1.reset_y(temp);

  return s;
}
