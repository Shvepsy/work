#pragma once
#include <iostream>
#include "Dyn_array.h"
using namespace std;

class vector : public Dyn_array
{
private:
	void remove(const datatype& x) {}
public:
	vector(int _size);
	~vector();
	vector(int _size, datatype mean);
	vector(const vector &p);
	double norma() const;
	vector& operator=(const vector& d);
	vector operator-(vector v2) const;
	vector operator+(vector v2) const;
	vector operator*(vector::datatype mean);
	datatype operator*(vector v2);
	const bool operator==(const vector v2);
	const bool operator<(const vector v2);
	const bool operator>(const vector v2);

	// friend ostream& operator<< (ostream &s, const Point& p1);
	// friend istream& operator>> (istream &s, Point& p1);

};
