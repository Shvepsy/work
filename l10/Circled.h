#pragma once
#include "Point_class.h"

class Circled
{
public:
	virtual ~Circled();

	virtual double radius_of_incircle() const = 0;
	virtual double radius_of_circumcircle() const = 0;
};