#pragma once
#include "Polygon_class.h"
#include "Shape.h"

class Rectangle : public Polygon, public Shape {
  double width;
  double heigth;

 public:
  Rectangle(Point, Point);
  Rectangle(Point, double, double);
  double get_width();
  double get_heigth();

  Point center_of_gravity();
  int is_convex();

  Point center() const;           // from class Shape
  double area() const;            // from class Shape
  double perimeter() const;       // from class Shape
  void print(ostream& os) const;  // from class Shape

  double radius_of_incircle() const;      // from class Circled
  double radius_of_circumcircle() const;  // from class Circled
};
