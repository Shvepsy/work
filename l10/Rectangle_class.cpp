#include "Rectangle_class.h"

Rectangle::Rectangle(Point left_upper_top, Point right_lower_top) {
  N = 4;
  top = new Point[4];
  top[0] = left_upper_top;
  top[1] = Point(right_lower_top.get_x(), left_upper_top.get_y());
  top[2] = right_lower_top;
  top[3] = Point(left_upper_top.get_x(), right_lower_top.get_y());
  width = top[0].distance(top[1]);
  heigth = top[0].distance(top[3]);
}

Rectangle::Rectangle(Point left_upper_top, double w, double h) {
  N = 4;
  top = new Point[4];
  top[0] = left_upper_top;
  top[1] = Point(left_upper_top.get_x() + w, left_upper_top.get_y());
  top[2] = Point(left_upper_top.get_x() + w, left_upper_top.get_y() - h);
  top[3] = Point(left_upper_top.get_x(), left_upper_top.get_y() - h);
  width = w;
  heigth = h;
}

double Rectangle::get_width() { return width; }

double Rectangle::get_heigth() { return heigth; }

Point Rectangle::center_of_gravity() {
  return Point(((top[0].get_x() + top[1].get_x()) / 2),
               ((top[0].get_y() + top[3].get_y()) / 2));
}

int Rectangle::is_convex() { return 1; }

/*Methods from abstract class Shape*/

Point Rectangle::center() const {
  return Point(((top[0].get_x() + top[1].get_x()) / 2),
               ((top[0].get_y() + top[3].get_y()) / 2));
}

double Rectangle::area() const { return width * heigth; }

double Rectangle::perimeter() const { return 2 * (width + heigth); }

void Rectangle::print(ostream& os) const {
  os << "Tops of rectangle: " << endl;
  for (int i = 0; i < N; i++) os << " " << top[i] << endl;
}

/*Methods from abstract class Circled*/

double Rectangle::radius_of_incircle() const {
  try {
    if (width != heigth) throw(1);
    return width / 2;
  } catch (...) {
    // cout << "Not square. ";
    return -1;
  }
}

double Rectangle::radius_of_circumcircle() const {
  try {
    if (width != heigth) throw(1);
    return top[0].distance(top[2]) / 2;
  } catch (...) {
    // cout << "Not square. ";
    return -1;
  }
}
