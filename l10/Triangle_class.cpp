#include "Triangle_class.h"

const double epsilon = 1e-14;

Triangle::Triangle(Point* arr) : Polygon(arr, 3) {}

Point Triangle::get_top0() { return top[0]; }

Point Triangle::get_top1() { return top[1]; }

Point Triangle::get_top2() { return top[2]; }

void Triangle::type_of_triangle() {
  double a = top[0].distance(top[1]);
  a *= a;
  double b = top[0].distance(top[2]);
  b *= b;
  double c = top[1].distance(top[2]);
  c *= c;
  if (fabs(a - b - c) <= epsilon || fabs(b - a - c) <= epsilon ||
      fabs(c - a - b) <= epsilon) {
    cout << "Right triangle";
  } else if (a < b + c + epsilon && b < a + c + epsilon &&
             c < a + b + epsilon) {
    cout << "Acute triangle";
  } else if (a + epsilon > b + c || b + epsilon > a + c ||
             c + epsilon > a + b) {
    cout << "Obtuse triangle";
  }
}

/*Methods from abstract class Shape*/

Point Triangle::center() const {
  double x = 0;
  double y = 0;
  for (int i = 0; i < N; i++) {
    x += top[i].get_x();
    y += top[i].get_y();
  }
  x /= N;
  y /= N;
  return Point(x, y);
}

double Triangle::area() const {
  double s = 0;
  s += (top[0].get_x() - top[2].get_x()) * (top[1].get_y() - top[2].get_y());
  s -= (top[1].get_x() - top[2].get_x()) * (top[0].get_y() - top[2].get_y());
  s = 0.5 * fabs(s);
  return s;
}

double Triangle::perimeter() const {
  return top[0].distance(top[1]) + top[0].distance(top[2]) +
         top[1].distance(top[2]);
}

void Triangle::print(ostream& os) const {
  os << "Tops of triangle: " << endl;
  for (int i = 0; i < N; i++) os << " " << top[i] << endl;
}

/*Methods from abstract class Circled*/

double Triangle::radius_of_incircle() const {
  double a = top[0].distance(top[1]);
  double b = top[0].distance(top[2]);
  double c = top[1].distance(top[2]);

  double rad = (-a + b + c) * (a - b + c) * (a + b - c);
  rad /= 4 * (a + b + c);
  rad = sqrt(rad);

  return rad;
}

double Triangle::radius_of_circumcircle() const {
  double a = top[0].distance(top[1]);
  double b = top[0].distance(top[2]);
  double c = top[1].distance(top[2]);

  double rad = a * b * c / (4 * area());

  return rad;
}
