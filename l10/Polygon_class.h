#pragma once
#include "Point_class.h"

class Polygon {
 protected:
  Point* top;
  int N;

 public:
  Polygon(Point*, int);
  Polygon();
  ~Polygon();
  Polygon(Polygon&);
  int get_size() const;
  void set_size(int);
  void set_i_top(int, Point);
  Polygon& operator=(Polygon&);
  Point& operator[](int);
  bool operator==(Polygon&);
  bool operator!=(Polygon&);
  Point center_of_gravity();
  int is_convex();

  friend ostream& operator<<(ostream&, const Polygon&);
  friend istream& operator>>(istream&, Polygon&);
};
