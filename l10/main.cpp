#include "Triangle_class.h"
#include "Right_polygon_class.h"
#include "Rectangle_class.h"
#include "Shape.h"
#include "Circle.h"
#include <fstream>

using namespace std;

int main() {
  // Test 1: ���� ���� �������, ������� ���������� Shape class � Circled class.
  try {
    const int n = 4;
    Shape* shapes[n];  //������� ������ �� 4 �����

    Point p0(3, 4);
    Point p1(0, 2);
    Point p2(-3, 8);
    Point arr[3] = {p0, p1, p2};  //����� ��� ������������

    shapes[0] = new Triangle(arr);
    shapes[1] = new Right_polygon(5, 3.7, p0);
    shapes[2] = new Rectangle(p0, p1);
    shapes[3] = new Circle(p0, 12);

    for (int i = 0; i < n; ++i)  //���������� ������ �� �������
    {
      cout << *shapes[i] << endl;  //����� �������� �� ��������� (������)
      //�������� ������������� �������� ������ <<
      //�������� ����� ������ ��� ������, � ������� �������� �� ������ ��������
      cout << "Center   : " << shapes[i]->center() << endl;
      cout << "Area     : " << shapes[i]->area() << endl;
      cout << "Perimeter: " << shapes[i]->p() << endl;
      cout << "Radius of incircle: " << shapes[i]->radius_of_incircle() << endl;
      cout << "Radius of circumcircle: " << shapes[i]->radius_of_circumcircle()
           << endl;
      cout << "----------------------------------------" << endl;
    }
    //������� �����
    int i_max_area = 0;
    int i_max_perim = 0;
    int i_min_in = 0;
    int i_min_circum = 0;
    for (int i = 1; i < n; ++i)  //���������, ��� �� ���� ������ ����� ����/���
    {
      if (shapes[i]->area() > shapes[i_max_area]->area()) i_max_area = i;
      if (shapes[i]->p() > shapes[i_max_perim]->p())
        i_max_perim = i;
      if (shapes[i]->radius_of_incircle() <
              shapes[i_min_in]->radius_of_incircle() &&
          shapes[i]->radius_of_incircle() != -1)
        i_min_in = i;
      if (shapes[i]->radius_of_circumcircle() <
              shapes[i_min_circum]->radius_of_circumcircle() &&
          shapes[i]->radius_of_circumcircle() != -1)
        i_min_circum = i;
    }

    cout << "The shape with maximum area: " << i_max_area
         << endl;                         //������ �������
    cout << *shapes[i_max_area] << endl;  //������ ������
    cout << "The shape with maximum perimeter: " << i_max_perim << endl;
    cout << *shapes[i_max_perim] << endl;
    cout << "The shape with minimum incircle radius: " << i_min_in << endl;
    cout << *shapes[i_min_in] << endl;
    cout << "The shape with minimum circumcircle radius: " << i_min_circum
         << endl;
    cout << *shapes[i_min_circum] << endl;

    for (int i = 0; i < n; ++i)  //����������� ������ ��-��� �����
      delete shapes[i];
  } catch (const exception& e) {
    cout << e.what() << endl;
  }

  system("pause");
  return 0;
}
