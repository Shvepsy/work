#include "Right_polygon_class.h"

Right_polygon::Right_polygon(int size, double len, Point cent) {
  side_length = len;
  center_of_pol = cent;
  N = size;
  double R = radius_of_circumcircle();
  double xc = center_of_pol.get_x();
  double yc = center_of_pol.get_y();

  top = new Point[N];

  for (int i = 0; i < N; i++) {
    top[i].reset_x(xc + R * cos(2 * 3.141592 * i / N));
    top[i].reset_y(yc + R * sin(2 * 3.141592 * i / N));
  }
}

double Right_polygon::get_length() { return side_length; }

Point Right_polygon::get_center() { return center_of_pol; }

Point Right_polygon::center_of_gravity() { return center_of_pol; }

int Right_polygon::is_convex() { return 1; }

/*Methods from abstract class Shape*/

Point Right_polygon::center() const { return center_of_pol; }

double Right_polygon::area() const {
  return N * radius_of_incircle() * side_length;
}

double Right_polygon::perimeter() const { return side_length * N; }

void Right_polygon::print(ostream& os) const {
  os << "Tops of right polygon: " << endl;
  for (int i = 0; i < N; i++) os << " " << top[i] << endl;
}

/*Methods from abstract class Circled*/

double Right_polygon::radius_of_incircle() const {
  return radius_of_circumcircle() * cos(PI / N);
}

double Right_polygon::radius_of_circumcircle() const {
  return side_length / (2 * sin(PI / N));
}
