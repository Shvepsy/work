#pragma once
#include <iostream>
using namespace std;

template <typename T>
class node {
 public:
  node* _left;
  node* _right;
  node* _parent;
  T _value;
  node();
  node(T val);
  friend std::ostream& operator<<(std::ostream& os, const node<T>& it) {
    os << it._value;
    return os;
  }
  ~node();
};

template <typename T>
node<T>::node() {
  _left = nullptr;
  _right = nullptr;
  _parent = nullptr;
}

template <typename T>
node<T>::node(T val) {
  _left = nullptr;
  _right = nullptr;
  _parent = nullptr;
  _value = val;
}

template <typename T>
node<T>::~node() {
  delete _right;
  delete _left;
}
