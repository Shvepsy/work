#pragma once
#include "node.h"
#include <iostream>

template<typename T> class tree {
private:
	//�������� � ����
	void push(node<T> *nd, T val);
	//����� ����
	node<T>* find(node<T> *nd, T val);
	//������� ���������� ��������� � ���������
	void count(node<T> *nd, int &c);
	//������ ���� � ������� ������ ���������� �� ���������
	void push(node<T> *source);
protected:
	//������ ������
	node<T>* _root;
	//������� ��������� � ������ � nd
	void remove_tree(node<T> *nd);
public:
	tree();
	//����������� �����������
	tree(tree<T> &tr);
	//������ ������� � ������
	virtual void push(T val);
	//������� ������� �� ������
	virtual void remove(T val);
	//���� ������� � ������
	virtual node<T>* find(T val);
	//���������� ���������� ����� � ������
	int count();
	//������ ������ ����������
	~tree();

	//����������� �����, ��� ���� ����������� ������

	//����� ���������
	virtual void out_tree(node<T> *nd, std::ostream& os);
	//��������� �������
	virtual void operator+=(T val);
	//������� �������
	virtual void operator-=(T val);
	//���������� ������� �� ���������� �����
	virtual bool operator>(tree<T> &val);
	virtual bool operator<(tree<T> &val);
	virtual tree<T>& operator=(tree<T> &val);//���������� =
	//����� ������
	friend std::ostream& operator<<(std::ostream& os, tree<T>& tr) {
		if (tr._root == nullptr)
			os << "tree is empty";
		else
			tr.out_tree(tr._root, os);
		os << std::endl;
		return os;
	}
};

template<typename T> tree<T>::tree() {
	_root = nullptr;
}

template<typename T> tree<T>::tree(tree<T> &tr) {
	_root = nullptr;
	push(tr._root);
}



template<typename T> void tree<T>::push(T val) {
	//���� ����� ��� - ������� �� ���������
	if (_root == nullptr)
		_root = new node<T>(val);
	else
		//�������� � �������� ����
		push(_root, val);
}


template<typename T> void tree<T>::push(node<T> *nd, T val) {
	//���� ��������, ������� ������ - ������ �������� ����, � ������� ������
	if (val < nd->_value) {
		//���� ������ ������� ���
		if (nd->_left == nullptr) {
			//������� ��� � ������ ��������
			nd->_left = new node<T>(val);
			//�������������� ��������� �� ��������
			nd->_left->_parent = nd;
		}
		else
			//���� ����� ������� ���� - ������ � ����
			push(nd->_left, val);
	}
	//���������� � ������ ����������
	else if (val >nd->_value) {
		if (nd->_right == nullptr) {
			nd->_right = new node<T>(val);
			nd->_right->_parent = nd;
		}
		else
			push(nd->_right, val);
	}
}

template<typename T> void tree<T>::remove_tree(node<T> *nd) {
	if (nd != nullptr) {
		//���� ����� ��������� �� �����
		if (nd->_left != nullptr) {
			//������� ��� ����� ����������
			remove_tree(nd->_left);
			delete nd->_left;
			nd->_left = nullptr;
		}
		//���������� � ������
		if (nd->_right != nullptr) {
			remove_tree(nd->_right);
			delete nd->_right;
			nd->_right = nullptr;
		}
	}
}

template<typename T> node<T>* tree<T>::find(T val) {
	if (_root == nullptr)
		return nullptr;
	//���������� ��������� ������ � ����
	return find(_root, val);
}

template<typename T> node<T>* tree<T>::find(node<T> *nd, T val) { //����������
	//���� �������� � ���� ��������� � ������� - ���������� ����
	if (val == nd->_value)
		return nd;

	//���� �������� ������ - ���� � ����� ���������
	if (val < nd->_value) {
		if (nd->_left != nullptr)
			return find(nd->_left, val);
		else
			return nullptr;
	}
	//����� - ���� � ������ ���������
	else if (val > nd->_value) {
		if (nd->_right != nullptr)
			return find(nd->_right, val);
		else
			return nullptr;
	}
	return nullptr;
}

template<typename T> void tree<T>::out_tree(node<T> *nd, std::ostream& os) {
	//����� ������ - ������� �����
	//������� ����� ���������
	if (nd->_left != nullptr)
		out_tree(nd->_left, os);
	//����� ����
	os << nd->_value << " ";
	//����� ������� ���������
	if (nd->_right != nullptr)
		out_tree(nd->_right, os);
}

template<typename T> tree<T>::~tree() {
	//���������� ������� ��� ������ ����������
	if (_root != nullptr)
		remove_tree(_root);
	delete _root;
}

template<typename T> int tree<T>::count() {
	int c = 0;
	//���������� ��������� ������ ��������� �� ���� ����� � ����� ���������� � ����������� � ���������
	count(_root, c);
	return c;
}

template<typename T> void tree<T>::count(node<T> *nd, int &c) {
	if (nd != nullptr) {
		c++; //�������� 1 �.�. ������� ������� �� ����
		//��������� � ������ ���������
		count(nd->_left, c);
		//��������� � ����� ���������
		count(nd->_right, c);
	}
}

template<typename T> void tree<T>::operator+=(T val) {
	this->push(val);
}

template<typename T> void tree<T>::operator-=(T val) {
	this->remove(val);
}

template<typename T> bool tree<T>::operator>(tree<T> &val) {
	//������� ������������ �� ���������� �����
	return this->count() > val.count();
}

template<typename T> bool tree<T>::operator<(tree<T> &val) {
	return this->count() < val.count();
}

template<typename T> tree<T>& tree<T>::operator=(tree<T> &val) {
	this->remove_tree(_root);
	push(_root);
	return *this;
}

template<typename T> void tree<T>::push(node<T> *source) {
	if (source != nullptr) {//���� ���� ����
		//������ � ������� ������ �������� ����
		push(source->_value);
		//������ � ������� ������ �������� ������ ���������
		push(source->_left);
		push(source->_right);
	}
}


/////��������� �������� ����/////
template<typename T> void tree<T>::remove(T val) {
	//���� ��������
	if (val == _root->_value) {
		node<T>* nd = nullptr;
		//���� ������������ �� ����� �����
		if (_root->_left != nullptr) {
			nd = _root->_left;
			while (nd->_right != nullptr)
				nd = nd->_right;
			//������ �������� � ������
			_root->_value = nd->_value;
			//������� ��������� ���� � ������� ������ � ��� �������
			_root->_left = _root->_left->_left;
			delete _root->_left->_parent;
			_root->_left->_parent = _root;
		}
		else {
			nd = _root->_right;
			//���� ����������� �� ������ �����
			while (nd->_left != nullptr)
				nd = nd->_left;
			//������ �������� � ������
			_root->_value = nd->_value;
			//������� ��������� � ������� ������ � ��� �������
			_root->_right = _root->_right->_right;
			delete _root->_right->_parent;
			_root->_right->_parent = _root;
		}
		return;
	}

	node<T>* nd = find(val);
	if (nd == nullptr)
		return;

	//�������� �����
	if (nd->_left == nullptr && nd->_right == nullptr) {
		node<T>* par = nd->_parent;
		if (par->_left == nd) {
			delete par->_left;
			par->_left = nullptr;
		}
		else if (par->_right == nd) {
			delete par->_right;
			par->_right = nullptr;
		}
	}

	//����� ���� �������� ���� ������
	else if (nd->_left == nullptr && nd->_right != nullptr ||
		nd->_right == nullptr && nd->_left != nullptr) {
		node<T>* par = nd->_parent;
		if (par->_left == nd) {
			if (nd->_left != nullptr) {
				par->_left = nd->_left;
				nd->_left->_parent = par;
				delete nd;
			}
			else {
				par->_left = nd->_right;
				nd->_right->_parent = par;
				delete nd;
			}
		}
		else if (par->_right == nd) {
			if (nd->_left != nullptr) {
				par->_right = nd->_left;
				nd->_left->_parent = par;
				delete nd;
			}
			else {
				par->_right = nd->_right;
				nd->_right->_parent = par;
				delete nd;
			}
		}
	}
	//����� ���� ��� ����
	else {
		node<T>* par = nd->_parent;
		node<T> *cr = nullptr;
		//���� ��� ����� �������
		if (par->_left == nd) {
			cr = nd->_right;
			//���� ����������� �� ������ �����
			while (cr->_left != nullptr)
				cr = cr->_left;
			//������ ��������
			nd->_value = cr->_value;
			//������� ��������� � ������� ������ � ��� �������
			if (cr->_parent != nd) {
				node<T>* cr_par = cr->_parent;
				delete cr_par->_left;
				cr_par->_left = nullptr;
			}
			else {
				delete nd->_right;
				nd->_right = nullptr;
			}
		}
		else {
			cr = nd->_left;
			//���� ������������ �� ����� �����
			while (cr->_right != nullptr)
				cr = cr->_right;
			//������ ��������
			nd->_value = cr->_value;
			if (cr->_parent != nd) {
				//������� ��������� � ������� ������ � ��� �������
				node<T>* cr_par = cr->_parent;
				delete cr_par->_right;
				cr_par->_right = nullptr;
			}
			else {
				delete nd->_left;
				nd->_left = nullptr;
			}
		}
	}
}
