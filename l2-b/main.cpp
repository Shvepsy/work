#include <iostream>
#include "tree.h"
using namespace std;

int main() {
  // Q 1, 2, 3, 4

  elem* myTree = create_bstree(9);
  cout << "myTree: " << endl;
  print(myTree, 0);
  cout << endl;

  elem* myTree_copy = copy_tree(myTree);
  insert_element(myTree, 5);
  insert_element(myTree, 25);
  insert_element(myTree, 14);

  cout << "myTree: " << endl;
  print(myTree, 0);
  cout << endl;
  cout << "myTree_copy: " << endl;
  print(myTree_copy, 0);
  cout << endl;

  cout << "Width of myTree: " << width_of_tree(myTree) << endl;
  cout << "Depth of myTree: " << depth_of_tree(myTree) << endl;
  cout << "Width of myTree_copy: " << width_of_tree(myTree_copy) << endl;
  cout << "Depth of myTree_copy: " << depth_of_tree(myTree_copy) << endl;

  // Q 5

  int arr5[9] = {8, 3, 10, 1, 6, 14, 4, 7, 13};
  elem* myTree5 = array_to_tree(arr5, 9);
  cout << "myTree5: " << endl;
  print(myTree5, 0);
  cout << endl;
  print_order(myTree5);
  cout << endl;
  print_order_rev(myTree5);

  // Q 6
  int arr6[9] = {8, 3, 10, 1, 6, 14, 4, 7, 13};
  elem* myTree6 = array_to_tree(arr6, 9);
  cout << "myTree6: " << endl;
  print(myTree6, 0);
  cout << endl;

  cout << "Left of 6 elem: " << elem_with_value(myTree6, 6)->lt->data << endl;

  // Q 7
  int arr7[9] = {8, 3, 10, 1, 6, 14, 4, 7, 13};
  elem* myTree7 = array_to_tree(arr7, 9);
  cout << "myTree7: " << endl;
  print(myTree7, 0);
  cout << endl;

  del_all_leafs_with_value(myTree7, 7);
  del_all_leafs_with_value(myTree7, 13);
  cout << "myTree7: " << endl;
  print(myTree7, 0);
  cout << endl;

  return 0;
}
