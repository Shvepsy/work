//
// ������������ ������ �4. ������. ���� � v�����
// point.cpp
//
#include <iostream>
#include <stdexcept>
#include <cmath>
#include "point.h"

point get_point()
{
    point p;
    std::cin >> p.x >> p.y;
    if (!std::cin)
        throw std::domain_error("������������ ���������� �����");
    return p;
}

point::point() {
  this->x = 0;
  this->y = 0;
}

point::point(double x, double y) {
  this->x = x;
  this->y = y;
}

double point::get_x() const {
  return this->x;
}
double point::get_y() const {
  return this->y;
}
double point::distance_to(point target) const {
  return sqrt(pow(abs(this->x - target.get_x()),2) + pow(abs(this->y - target.get_y()),2)) ;
}

void print(point p)
{
    std::cout << "( " << p.get_x() << ", " << p.get_y() << " )";
}
