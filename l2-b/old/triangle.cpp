//
// ������������ ������ �4. ������. ���� � v�����
// point.cpp
//
#include <iostream>
#include <stdexcept>
#include <cmath>
#include "triangle.h"

// point get_point()
// {
//     point p;
//     std::cin >> p.x >> p.y;
//     if (!std::cin)
//         throw std::domain_error("������������ ���������� �����");
//     return p;
// }

triangle::triangle() {
  this->p1 = 0;
  this->p2 = 0;
  this->p3 = 0;
}
triangle(point p1, point p2, point p3) {
  this->p1 = p1;
  this->p2 = p2;
  this->p3 = p3;
}

double triangle::get_p1() const {
  return this->p1;
}
double triangle::get_p2() const {
  return this->p2;
}
double triangle::get_p3() const {
  return this->p3;
}


string triangle::type() const {
  double a = p1.distance_to(p2);
  double b = p1.distance_to(p3);
  double c = p2.distance_to(p3);
  if (a<=b+c && b<=a+c && c<=b+a)
  {
    if (a==b+c || b==c+a || c==a+b || a==0 || b==0 || c==0)
      return "����������� ������� �������";
    else if (a==b && b==c && c==a)
      return "��������������";
    else if (a==b ^ b==c ^ c==a)
      return "��������������";
    else return "��������������";
  }
  else return "�� �����������";
};
double triangle::perimeter() const {
  return p1.distance_to(p2) + p1.distance_to(p3) + p2.distance_to(p3);
};

double triangle::square() const {
  double a = p1.distance_to(p2);
  double b = p1.distance_to(p3);
  double c = p2.distance_to(p3);
  double p = this->perimeter();
  return sqrt(p*(p-a)*(p-b)*(p-c));
};
double triangle::incircle() const {
  double s = this->square();
  double p = this->perimeter();
  return s/p;
};
double triangle::circumcircle() const{
  double a = p1.distance_to(p2);
  double b = p1.distance_to(p3);
  double c = p2.distance_to(p3);
  double s = this->square();
  return (a*b*c)/(4*s);
};



//
// double point::distance_to(point target) const {
//   return sqrt(pow(abs(this->x - target.get_x()),2) + pow(abs(this->y - target.get_y()),2)) ;
// }

void print(triangle t)
{
    std::cout << "( " << t.p1.get_x() << ", " << t.p1.get_y() << " )";
}
