#ifndef HEADER_H_INCLUDED
#define HEADER_H_INCLUDED

struct elem {
  int data;
  elem *lt, *rt;
};

elem* create(int);
elem* array_to_tree(int* arr, int n);
void print(elem*, int);
void insert_element(elem*& tree, int a);
int width_level(elem* tree, int level);

elem* create_bstree(int n);                            //#1
elem* copy_tree(elem* tree);                        //#2
int width_of_tree(elem* tree);                      //#3
int depth_of_tree(elem* tree);                      //#4
void print_order(elem* tree);                       //#5.1
void print_order_rev(elem* tree);                   //#5.2
elem* elem_with_value(elem* tree, int a);           //#6
void del_all_leafs_with_value(elem*& tree, int a);  //#7


#endif  // HEADER_H_INCLUDED
