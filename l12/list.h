﻿#pragma once
#include <iostream>

class list {
public:
	typedef int datatype;

    list();
    list(const list &l);
    list &operator=(const list &l);
    ~list();

    size_t size() const;
		bool is_empty() const;

    // Add to tail
    void push_back(const datatype &x);
    // Pop tail
    void pop_back();
    // Get tail
    datatype back() const;

    // Add to head
    void push_front(const datatype &x);
    // get from head
    void pop_front();
    // get head
    datatype front() const;

    friend class test_list;

private:
	struct node {
		datatype data;
		node *prev, *next;
	};

	node *first, *last;
	void copy_list(const node *from_first, const node *from_last);
	void delete_list();

public:
	class iterator {

		node *current;
    const list *collection;

	public:
		iterator(const list *collectio, node *curren) {
			collection = collectio;
			current = curren;
		}

		iterator &operator=(const iterator &l);

    datatype &operator*();
    iterator &operator++();
    iterator operator++(int);
    bool operator==(const iterator &it) const;
    bool operator!=(const iterator &it) const;

		friend void read_from_file(const char *filename);

        // Объявляем класс,
        // которому можно создавать итераторы
        friend class list;
	};


    iterator begin() const;
    iterator end() const;

    //fined elem or list::end()
    iterator find(const datatype &x) const;
    void insert(const iterator &it, const datatype &x);
    void remove(const iterator &it);

		friend void read_from_file(const char *filename);
};
void read_from_file(const char *filename);
std::ostream &operator<<(std::ostream &os, list &l);
