#pragma once
#include <iostream>

using namespace std;

template <class T>
struct node {
  T data;
  node<T> *left, *right;
};
template <class T>
class tree {
  node<T>* root;

 public:
  tree() { root = nullptr; };
  tree(const tree<T>& A) {
    root = nullptr;
    root = copy_tree(A.root);
  };
  tree(int n, T* a) {
    root = nullptr;
    for (int i = 0; i < n; i++) {
      node<T>* p = new node<T>;
      p->data = a[i];
      p->left = 0;
      p->right = 0;
      if (root == nullptr)
        root = p;
      else {
        node<T> *q = root, *r = nullptr;
        while (q) {
          r = q;
          if (p->data < q->data)
            q = q->left;
          else
            q = q->right;
        }
        if (p->data < r->data)
          r->left = p;
        else
          r->right = p;
      }
    }
  };
  ~tree() {
    cleanup(root);
    root = nullptr;
  };

  node<T>* getroot() { return root; }

  node<T>* CreateNode(T val) {
    node<T>* nd = new node<T>;
    nd->data = val;
    return nd;
  };
  tree& operator=(const tree<T>& A) {
    if (this != &A) {
      cleanup(root);
      copy_tree(A.root);
      return *this;
    };
  };
  bool operator==(const tree<T>& c) { return eq(root, c.root); };
  bool operator!=(const tree<T>& c) { return neeq(root, c.root); };
  friend void operator<<(ostream& out, tree<T> tree) {
    tree.print_tree(tree.getroot());
  };

  int nodelen() { return nodelen_tree(root); };
  void print_tree(node<T>* root) {
    if (root == nullptr) return;
    print_tree(root->left);
    cout << root->data << " ";
    print_tree(root->right);
  };
  int depth(node<T>* root) {
    int depthLeft, depthRight;
    if (root == nullptr)
      return -1;
    else {
      depthLeft = depth(root->left);
      depthRight = depth(root->right);
      if (depthLeft > depthRight) {
        return 1 + depthLeft;
      } else
        return 1 + depthRight;
    }
  };
  int Maxwidth(struct node<T>* root) {
    int maxWdth = 0;
    int i;
    int widthc = 0;
    int h = depth(root);
    for (i = 1; i < h; i++) {
      widthc = width(root, i);
      if (widthc > maxWdth) maxWdth = widthc;
    }
    return maxWdth;
  };
  void create_tree(int n) { root = create(n); };
  node<T>* create(int n) {
    if (n > 0) {
      node<T>* p = new node<T>;
      cout << "Data: " << endl;
      cin >> p->data;
      int d = n / 2;
      p->left = create(d);
      p->right = create(n - d - 1);
      return p;
    } else {
      return NULL;
    }
  };

 private:
  static node<T>* copy_tree(node<T>* root) {
    if (root == nullptr) return nullptr;
    node<T>* t = new node<T>;
    t->data = root->data;
    t->left = copy_tree(root->left);
    t->right = copy_tree(root->right);
    return t;
  };
  static void cleanup(node<T>* root) {
    if (root != 0) {
      cleanup(root->left);
      cleanup(root->right);
      delete root;
    }
  };
  static int nodelen_tree(node<T>* root) {
    if (root == nullptr)
      return 0;
    else
      return 1 + nodelen_tree(root->left) + nodelen_tree(root->right);
  };
  static bool eq(node<T>* root, node<T>* root1) {
    if (root == nullptr && root1 == nullptr)
      return true;
    else {
      if (root->data == root1->data) {
        return eq(root->left, root1->left) &&
               eq(root->right, root1->left);
      } else
        return false;
    }
  };
  static bool neeq(node<T>* root, node<T>* root1) {
    if (root == nullptr && root1 == nullptr)
      return true;
    else {
      if (root->data != root1->data) {
        return eq(root->left, root1->left) &&
               eq(root->right, root1->left);
      } else
        return false;
    }
  };
  static int width(node<T>* root, int level) {
    if (!root)
      return 0;
    else if (level == 1)
      return 1;
    else if (level > 1)
      return width(root->left, level - 1) + width(root->right, level - 1);
    width(root->right, level - 1);
  };
};
