//
// Лабораторная работа №22. Стандартная библиотека шаблонов. Шаблонные функции. Работа с контейнерами
// main.cpp
//
#include <typeinfo>
#include <iostream>
#include "algorithms.h"
#include <list>
#include "header.h"

using namespace std;

int main()
{
	const int n = 10;
	int arr[n] = { 1,-1,2,3,4,5,6,7,8,-9 };
	print<int[n]>(arr);
	cout << "Minimum(array)  = " << min_value<int*>(arr + 0, arr + n) << endl;
	cout << "Sum(array)  = " << sum_value<int*, int>(arr + 0, arr + n) << endl;

	list<int> l;
	for (int i = 0; i < n; ++i)
		l.push_back(arr[i]);
	cout << "List: " << '\n';
	print(l);

	cout << "Minimum = " << min_value(l.begin(), l.end()) << endl;
	sort(l.begin(), l.end());
	cout << "Sorted: " << '\n';
	print(l);
	cout << "Sum = " << sum_value(l.begin(), l.end()) << endl;
	int a = 4;
	cout << "Finded value (4) =" << *(find_value(l.begin(), l.end(), a)) << endl;
	cout << "Mediana = " << mediana(l) << endl;
	return 0;
}
