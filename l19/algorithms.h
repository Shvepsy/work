//
// Лабораторная работа №19. Стандартная библиотека шаблонов. Шаблонные функции. Работа с контейнерами
// algorithms.h
//
#pragma once
#include <iostream>
using namespace std;

// Шаблон функции вывода
template <typename Container>
void print(Container &container)
{
	// Проход по контейнеру.
	// Работает по любому типу с методами:
	// begin(), end().
	// А также для статических массивов T[N].
    for (const auto &value : container)
        std::cout << value << " ";
	std::cout << std::endl;
}

// Шаблон функции вычисления минимума в промежутке
template <typename Iterator>
auto min_value(const Iterator &begin, const Iterator &end)
{
	/*  Тип возвращвемого значения
	вычисляется автоматически на этапе компиляции.
	Это возможно, т.к. тип значений в контейнере
	однозначно определяет тип возвращаемого значения.
	*/
	auto min = *begin;
	cout << typeid(min).name() << " : ";

    auto it = begin;
    for (++it; it != end; ++it)
        if (min > *it)
            min = *it;
    return min;
}

// Шаблон функции вычисления суммы в промежутке
template <typename Iterator, typename T = double>
auto sum_value(const Iterator &begin, const Iterator &end)
{
	/*  Тип возвращвемого значения
	не может быть вычислен автоматически, т.к.
	типа значений в контейнере может быть не достаточно для
	хранения суммы значений.
	*/
	T sum = *begin;
	std::cout << typeid(sum).name() << " : ";

	auto it = begin;
	for (++it; it != end; ++it)
		sum += *it;
	return sum;
}

template <typename Iterator, typename T = double>
Iterator find_value(const Iterator &begin, const Iterator &end, T x)
{
	auto it = begin;
	for (it; it != end; ++it)
		if (x == *it)
			return it;
		return end;
}

template <typename Iterator, typename T = double>
size_t counting(const Iterator &begin, const Iterator &end, T x)
{
	size_t sum = 0;
	auto it = begin;
	for (it; it != end; ++it)
		if (x == *it)
			++sum;
		return sum;
}

template <typename Iterator,typename Func>
auto countingf(const Iterator &begin, const Iterator &end,Func func)
{
	size_t sum = 0 ;
	std::cout << typeid(sum).name() << " : ";
	auto it = begin;
	for (it; it != end; ++it)
		if (func(*it))
			++sum;
	return sum;
}

template <typename Iterator, typename T = double>
size_t size(const Iterator &begin, const Iterator &end, bool(*func)(T))
{
	size_t c = 0;
	cout << typeid(c).name() << " : ";
	auto it = begin;
	for (it; it != end; ++it)
		c;
	return c;
}

template <typename Container,typename T = double>
auto middle(Container &container){
	auto mid = sum_value(begin(container),end(container)) / distance(begin(container),end(container));
	return mid;
}

template <typename Iterator>
void sort(const Iterator &begin, const Iterator &end) {
	auto b_end = end;
	b_end--;
	bool fl = false;
	while (!fl) {
		fl = true;
		for (auto it = begin; it != b_end; ++it)
		{
			auto iter = it;
			iter++;
			if (*it > *iter){
				std::swap(*it, *iter);
				fl = false;
			}
		}
	}
}


template <typename Container, typename T = double>
auto mediana(Container &container) {
	sort(begin(container), end(container));
	int d = distance(begin(container), end(container));
	auto it = begin(container);
	double a;
	// it += (d / 2);
  for ( int i = 0; i < (d / 2); ++i ) it++ ;
	if (d % 2 != 0) a = *it;
  else a = (*it + *(++it)) / 2.0;
	return a;
}
