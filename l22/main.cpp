#include "func.h"
using namespace std;

int main()
{
	setlocale(LC_ALL, "Russian");
	string str;
	cout << "Enter string: ";
	getline(cin, str);

	cout << "Q1: " << endl;
	vector<char> chars(str.size());
	copy(str.begin(), str.end(), chars.begin());

	cout << "Chars based vector: ";
	for_each(str.begin(), str.end(), [](char c) { cout << c; });
	cout << endl;

	cout << "Q2:" << endl;
	vector<string> *strings = split_strings(str);
	print_strings("Words : ",*strings);

	cout << "Q3:" << endl;
	string upper_str = str;
	transform(str.begin(), str.end(), upper_str.begin(), [](char c) { return toupper(c); });
	cout << "toupper : " << upper_str << endl;

	cout << "Q4:" << endl;
	int i = 1;
	transform(strings->begin(), strings->end(), strings->begin(), [&i](string &s) {
		rotate(s.begin(), s.begin() + s.size() % i++, s.end());
		return s;
	});
	cout << "Coded: ";
	for_each(strings->begin(), strings->end(), [](string &s) { cout << s << " "; });
	cout << endl;

	i = 1;
	transform(strings->begin(), strings->end(), strings->begin(), [&i](string &s) {
		rotate(s.begin(), s.begin() + s.size() - s.size() % i++, s.end());
		return s;
	});
	cout << "Decoded: ";
	for_each(strings->begin(), strings->end(), [](string &s) { cout << s << " "; });
	cout << endl;

	cout << "Q5: " << endl;
	cout << "Enter char : ";
	char c;
	cin >> c;
	int count = count_if(str.begin(), str.end(), [&c](char cstr) { return cstr == c; });
	cout << "Count of \'" << c << "\': " << count << endl;

	cout << "Q6: " << endl;
	cout << "Find pattern : ";
	string find_string;
	cin >> find_string;
	count = count_if(strings->begin(), strings->end(), [&find_string](string &s) { return find_string.compare(s) == 0; });
	cout << "Count of \'" << find_string << "\': " << count << endl;

	cout << "Q7: " << endl;
	cout << "Patter string for diff : ";
	string second_str;
	cin.ignore();
	getline(cin, second_str);
	vector<string> *second_strings = split_strings(second_str);

	int min = strings->size();
	if (strings->size() > second_strings->size())
		min = second_strings->size();

	if (str.compare(second_str) == 0)
		cout << "Eqvialent string" << endl;
	else {
		auto mis_pair = mismatch(strings->begin(), strings->begin() + min, second_strings->begin());
		cout << "Diff : " << *mis_pair.first << " & " << *mis_pair.second << endl;
	}

	cout << "Q8: " << endl;
	string str1, str2;
	cout << "Enter string : ";
	cin >> str1;
	cout << "Enter example annagramm : ";
	cin >> str2;

	if (str1.size() != str2.size() || is_permutation(str1.begin(), str1.end(), str2.begin()))
		cout << "Matched : " << str1 << endl;
	else
		cout << "Mismatch : " << str1 << endl;

	cout << "Q9: " << endl;
	vector<string> *annagramms = gen_annagramm(str1);
	print_strings("Anangramm list : ", *annagramms);

  return 0;
}
