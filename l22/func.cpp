#include "func.h"
using namespace std;

vector<string>* split_strings(string &str) {
	stringstream sin(str);
	vector<string> *strings = new vector<string>();
	string s;
	while (sin.rdbuf()->in_avail()) {
		sin >> s;
		strings->push_back(s);
	}
	return strings;
}

vector<string>* gen_annagramm(string &str) {
	vector<string> *res = new vector<string>();
	do {
		res->push_back(str);
	} while (next_permutation(str.begin(), str.end()));
	return res;
}

void print_strings(char *prompt, vector<string> &v) {
	cout << prompt << endl;
	for_each(v.begin(), v.end(), [](string &s) { cout << "\t" << s << endl; });
}
