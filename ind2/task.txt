Создать базовый класс для хранения расстояние планеты от солнца и
полный оборот планеты в днях.
Создать производный класс, который наследует базовый класс и хранит
окружность орбиты (конструктор должен передавать классу расстояние, а
также подсчитывать окружность орбиты).
В описании классов предусмотреть методы, необходимые для
обеспечения функционирования этих классов, и перегрузку операций
присваивания, ввода данных, вывода данных на экран, сравнения двух объектов
(полного и/или частичного) и увеличения значений элементов-данных на
заданное число.
Написать программу, демонстрирующую работу с этими классами
