#pragma once
#include "Distance.h"

const double PI = 3.141592;

class Circle : public Distance {
  double orbit;

 public:
  Circle();
  Circle(double, int);
  ~Circle();

  double get_orbit() const;

  //устанавливаем значение полей
  void set_dist(const double);
  void set_period(const int&);
  void set_orbit(const double&);

  Circle& operator=(Circle&);  //перегрузка =
  friend ostream& operator<<(ostream& os, const Circle& c);
  friend istream& operator>>(istream& is, Circle& c);
  const bool operator<(const Circle&) const;
  const bool operator>(const Circle&) const;
};
