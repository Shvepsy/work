/*Ind. #2. Variant #3.*/

#include "Distance.h"
#include "Circle.h"

int main() {
  Distance d1;
  Distance d2(1.4, 320);

  Circle c1;
  Circle c2(1.4, 320);

  cout << "Default constructor : " << endl;
  cout << "Distance = " << c1.get_dist() << endl;
  cout << "Period   = " << c1.get_period() << endl;
  cout << "Orbit    = " << c1.get_orbit() << endl;

  cout << "Initial with data constructor : " << endl;
  cout << "Distance = " << c2.get_dist() << endl;
  cout << "Period   = " << c2.get_period() << endl;
  cout << "Orbit    = " << c2.get_orbit() << endl;

  // Test 2.
  Circle cinp;
  cout << "Replace object by input : " << endl;
  cin >> cinp;

  Circle c22(14.68, 260);
  cout << "Saved object data : " << endl << cinp << endl;

  cout << "Addition values to data : " << endl;

  cout << "Old : " << endl;
  cout << "Distance = " << cinp.get_dist() << endl;
  cout << "Period   = " << cinp.get_period() << endl;
  cout << "Orbit    = " << cinp.get_orbit() << endl;
  cinp.set_dist(9.85);
  cinp.set_period(90);
  cinp.set_orbit(2.9);
  cout << "New : " << endl;
  cout << "Distance = " << cinp.get_dist() << endl;
  cout << "Period   = " << cinp.get_period() << endl;
  cout << "Orbit    = " << cinp.get_orbit() << endl;

  if (c1 < c2)
    cout << "c1 < c22" << endl;
  else if (c1 > c2)
    cout << "c1 > c22" << endl;

  return 0;
}
