#include "Distance.h"

Distance::Distance() : distance(0), period(0) {}

Distance::Distance(double d, int p) : distance(d), period(p) {}

Distance::~Distance() {}

double Distance::get_dist() const { return distance; }

int Distance::get_period() const { return period; }
